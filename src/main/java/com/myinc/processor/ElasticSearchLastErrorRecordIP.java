package com.myinc.processor;

import java.util.HashMap;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class ElasticSearchLastErrorRecordIP implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		
		
/*		{"size": 1,
			   "sort": { "Date": "desc"},
			   "query": {
			      "match_all": {}
			   }
			}*/



		
		Map<String,Object> sort=new HashMap<>();
		
		sort.put("timestamp", "desc");
		
		
		Map<String,Object> map=new HashMap<>();
		
		map.put("size", 1);
		map.put("sort", sort);
		System.out.println("ElasticSearchLastErrorRecordIP-------map--------"+map);
		
		
		exchange.getOut().setBody(map);
		
	}

}
