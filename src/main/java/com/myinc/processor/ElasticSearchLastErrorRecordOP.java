package com.myinc.processor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class ElasticSearchLastErrorRecordOP implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		

		Map<String, Object> mqMap = exchange.getIn().getBody(Map.class);
		Map<String,Object> finalResult=new HashMap<>();
		try {
			System.out.println("ElasticSearchLastErrorRecordOP--------mqMap------" +mqMap);

			Map<String,Object> hits=(Map<String, Object>) mqMap.get("hits");
			List<Map<String,Object>> listhits=(List<Map<String, Object>>) hits.get("hits");
			Map<String,Object> map=listhits.get(0);

			Map<String,Object> result=(Map<String, Object>) map.get("_source");

			System.out.println("date------------------------------------------"+result.get("timestamp"));	
			
			
			finalResult.put("timestamp", result.get("timestamp"));
		} catch (Exception e) {
			e.printStackTrace();
		}


	
		exchange.getOut().setBody(finalResult);

	}

}
