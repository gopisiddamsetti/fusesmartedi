package com.myinc.processor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import generated_1603090170802.Invoice;
import generated_1603090170802.Invoice.BaselineItemData;
import generated_1603090170802.Invoice.BeginningSegmentForInvoice;
import generated_1603090170802.Invoice.DateTimeReference;
import generated_1603090170802.Invoice.MonetaryAmount;
import generated_1603090170802.Invoice.Name;
import generated_1603090170802.Invoice.ReferenceId;
import generated_1603090170802.Invoice.TotalMonetaryValueSummary;
import generated_1603090170802.Invoice.TransactionSetHeader;
import generated_1603090170802.Invoice.BaselineItemData.BaselineItemDataIn;
import generated_1603090170802.Invoice.BaselineItemData.TaxInfo;
import generated_1603090170802.Invoice.Name.AddrInfo;
import generated_1603090170802.Invoice.Name.GeographicLocation;
import generated_1603090170802.Invoice.Name.NameIn;

public class Invoice810dbIP  implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		//-----------------------invoice DB-----------------------
		
		Map<String,String> invoiceHeader=(Map<String, String>) exchange.getProperty("InvoiceHeader");
		List<Map<String, String>> shipToAddress=(List<Map<String, String>>) exchange.getProperty("ShipToAddress");
	    List<Map<String, String>> invoiceLineItems=(List<Map<String, String>>) exchange.getProperty("InvoiceLineItems");
		
	    System.out.println("InvoiceNum--------------" + exchange.getProperty("InvoiceNum"));
		System.out.println("InvoiceHeader--------------" + invoiceHeader);
		System.out.println(shipToAddress.size()+"-------ShipToAddress--------------" + shipToAddress);
		System.out.println(invoiceLineItems.size()+"-------InvoiceLineItems--------------" + invoiceLineItems);

		
		
		String receiverID=invoiceHeader.get("SupplierNum");
		String invoiceNumber=invoiceHeader.get("InvoiceNum");
		String purchaseOrderNumber=invoiceHeader.get("PONum");
		short  transactionSetIdCode=Short.parseShort("810");
		byte transactionSetControlNumber=Byte.parseByte("000001");
		
		
		Invoice invoiceDB=new Invoice();
		
		
		TransactionSetHeader transactionSetHeaderDb=new TransactionSetHeader();
		transactionSetHeaderDb.setTransactionSetIdCode(transactionSetIdCode);
		transactionSetHeaderDb.setTransactionSetControlNumber(transactionSetControlNumber);
		invoiceDB.setTransactionSetHeader(transactionSetHeaderDb);
		
		
		for (Map<String, String> map : invoiceLineItems) {
			BaselineItemData baselineItemData=new BaselineItemData();
			BaselineItemDataIn baselineItemDataIn=new BaselineItemDataIn();
			List<JAXBElement<? extends Serializable>> assignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode=new ArrayList<>();
			assignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode.add(new JAXBElement(new QName("AssignedId"), String.class, map.get("InvoiceLineNum")));
			assignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode.add(new JAXBElement(new QName("QuantityInvoiced"), String.class, map.get("Quantity")));
			assignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode.add(new JAXBElement(new QName("UnitOrBasisForMeasurementCode"), String.class, map.get("UOM")));
			assignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode.add(new JAXBElement(new QName("UnitPrice"), String.class, map.get("UnitPrice")));
			assignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode.add(new JAXBElement(new QName("ProductServiceIdQual"), String.class, "PO"));
			assignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode.add(new JAXBElement(new QName("ProductServiceId"), String.class, map.get("POLineNum")));
			baselineItemDataIn.setAssignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode(assignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode);
			
			baselineItemData.setBaselineItemDataIn(baselineItemDataIn);
			TaxInfo taxInfo=new TaxInfo();
			taxInfo.setTaxTypeCode("SU");
			taxInfo.setMonetaryAmount(Float.parseFloat(map.get("LineAmount")));
			taxInfo.setPercent(map.get("SalesTaxPercent"));
			baselineItemData.setTaxInfo(taxInfo);
			
			invoiceDB.getBaselineItemData().add(baselineItemData);
			
		/*	map.get("InvoiceNum")
			map.get("SupplierPartNum")
			map.get("ShortDescription")
			map.get("LongDescription")
			map.get("DeliveryChargeCode")*/
			
		}
		
		

		List<JAXBElement<? extends Serializable>> dateOrInvoiceNumberOrPurchaseOrderNumberDb=new ArrayList<>();
		dateOrInvoiceNumberOrPurchaseOrderNumberDb.add(new JAXBElement(new QName("Date"), String.class, invoiceHeader.get("InvoiceDate")));
		dateOrInvoiceNumberOrPurchaseOrderNumberDb.add(new JAXBElement(new QName("InvoiceNumber"), String.class, invoiceNumber));
		dateOrInvoiceNumberOrPurchaseOrderNumberDb.add(new JAXBElement(new QName("Date"), String.class, invoiceHeader.get("TransmitDate")));
		dateOrInvoiceNumberOrPurchaseOrderNumberDb.add(new JAXBElement(new QName("PurchaseOrderNumber"), String.class, purchaseOrderNumber));
	 	BeginningSegmentForInvoice beginningSegmentForInvoiceDb=new BeginningSegmentForInvoice();
		beginningSegmentForInvoiceDb.setDateOrInvoiceNumberOrPurchaseOrderNumber(dateOrInvoiceNumberOrPurchaseOrderNumberDb);
		invoiceDB.setBeginningSegmentForInvoice(beginningSegmentForInvoiceDb);
	   	
		
		
		ReferenceId elementDB=new ReferenceId();
		elementDB.setReferenceIdQual("ZA");
		elementDB.setReferenceId(receiverID);
		
		ReferenceId elementDB1=new ReferenceId();
		elementDB1.setReferenceIdQual("PO");
		elementDB1.setReferenceId(invoiceHeader.get("PONum"));
		
		invoiceDB.getReferenceId().add(elementDB);
		invoiceDB.getReferenceId().add(elementDB1);
	
		List<String> addrInfoListDB=new ArrayList<>();
		Name nameDB =new Name();
		AddrInfo addrInfoDB=new AddrInfo();
		for (Map<String,String> shipToAddressMap: shipToAddress) {
			NameIn nameInDB=new NameIn();
			
			List<JAXBElement<String>> entityIdCodeOrNameOrIdCodeQualDB=new ArrayList<>();
			entityIdCodeOrNameOrIdCodeQualDB.add(new JAXBElement(new QName("EntityIdCode"), String.class, "ST"));
			entityIdCodeOrNameOrIdCodeQualDB.add(new JAXBElement(new QName("Name"), String.class, shipToAddressMap.get("Name")));
			
			nameInDB.setEntityIdCodeOrNameOrIdCodeQual(entityIdCodeOrNameOrIdCodeQualDB);
			
			nameDB.setNameIn(nameInDB);
			
					
			addrInfoListDB.add(shipToAddressMap.get("AddressLine1"));
			addrInfoDB.setAddrInfo(addrInfoListDB);
			//invoice.getName().setAddrInfo(addrInfo);
			
			
			GeographicLocation geographicLocationDB=new GeographicLocation();
			geographicLocationDB.setCityName(shipToAddressMap.get("City"));
			geographicLocationDB.setStateOrProvinceCode(shipToAddressMap.get("State"));
			geographicLocationDB.setPostalCode(Integer.parseInt(shipToAddressMap.get("Zip")));
			geographicLocationDB.setCountryCode(shipToAddressMap.get("Country"));
			//invoice.getName().setGeographicLocation(geographicLocation);
			nameDB.setGeographicLocation(geographicLocationDB);
		
			
			
		}
		
		nameDB.setAddrInfo(addrInfoDB);
		
		invoiceDB.setName(nameDB);
		
		DateTimeReference DateTimeReferenceDB=new DateTimeReference();
		short DateTimeQualDB=205;
		DateTimeReferenceDB.setDate(Integer.parseInt(invoiceHeader.get("InvoiceDate")));
		DateTimeReferenceDB.setDateTimeQual(DateTimeQualDB);
		DateTimeReference DateTimeReference1DB=new DateTimeReference();
		short DateTimeQual1DB=012;
		DateTimeReference1DB.setDate(Integer.parseInt(invoiceHeader.get("InvoiceDate")));
		DateTimeReference1DB.setDateTimeQual(DateTimeQual1DB);
		invoiceDB.getDateTimeReference().add(DateTimeReferenceDB);
		invoiceDB.getDateTimeReference().add(DateTimeReference1DB);
		
	
		
		
		
	
		List<String> amountDB=new ArrayList<>();
		amountDB.add(invoiceHeader.get("TotalInvoiceAmount"));
		
		TotalMonetaryValueSummary totalMonetaryValueSummaryDB=new TotalMonetaryValueSummary();
		totalMonetaryValueSummaryDB.setAmount(amountDB);
		invoiceDB.setTotalMonetaryValueSummary(totalMonetaryValueSummaryDB);
		
		generated_1603090170802.Invoice.TaxInfo taxInfomainDB=new generated_1603090170802.Invoice.TaxInfo();
		taxInfomainDB.setTaxTypeCode("SU");
		taxInfomainDB.setMonetaryAmount(Float.parseFloat(invoiceHeader.get("TotalSalesTaxAmount")));
	    invoiceDB.setTaxInfo(taxInfomainDB);
		
		
	    
		
		MonetaryAmount monetaryAmountDB=new MonetaryAmount();
		monetaryAmountDB.setAmountQualCode("D8");
		monetaryAmountDB.setMonetaryAmount(Float.parseFloat(invoiceHeader.get("DiscountPercent")));
		
		invoiceDB.setMonetaryAmount(monetaryAmountDB);
		
		//-----------------------invoice DB-----------------------
		
		
			

		
		
		exchange.setProperty("URL", invoiceHeader.get("Partner")+"/"+invoiceHeader.get("Partner")+"_X124010810"+"_Webhook_Outbound/webhook.rsb");
			
		System.out.println("URL--------URL------" + exchange.getProperty("URL"));
		

		
		
		Invoice invoice=new Invoice();
		
		
	
	
		
		TransactionSetHeader transactionSetHeader=new TransactionSetHeader();
		transactionSetHeader.setTransactionSetIdCode(transactionSetIdCode);
		transactionSetHeader.setTransactionSetControlNumber(transactionSetControlNumber);
		invoice.setTransactionSetHeader(transactionSetHeader);
		
		
		List<JAXBElement<? extends Serializable>> dateOrInvoiceNumberOrPurchaseOrderNumber=new ArrayList<>();
		dateOrInvoiceNumberOrPurchaseOrderNumber.add(new JAXBElement(new QName("Date"), String.class, "20090721"));
		dateOrInvoiceNumberOrPurchaseOrderNumber.add(new JAXBElement(new QName("InvoiceNumber"), String.class, invoiceNumber));
		dateOrInvoiceNumberOrPurchaseOrderNumber.add(new JAXBElement(new QName("Date"), String.class, "20090901"));
		dateOrInvoiceNumberOrPurchaseOrderNumber.add(new JAXBElement(new QName("PurchaseOrderNumber"), String.class, purchaseOrderNumber));
	 	BeginningSegmentForInvoice beginningSegmentForInvoice=new BeginningSegmentForInvoice();
		beginningSegmentForInvoice.setDateOrInvoiceNumberOrPurchaseOrderNumber(dateOrInvoiceNumberOrPurchaseOrderNumber);
		invoice.setBeginningSegmentForInvoice(beginningSegmentForInvoice);
	   	
		
		
		ReferenceId element=new ReferenceId();
		element.setReferenceIdQual("ZA");
		element.setReferenceId(receiverID);
		
		ReferenceId element1=new ReferenceId();
		element1.setReferenceIdQual("PO");
		element1.setReferenceId("4100ABC12300");
		
		invoice.getReferenceId().add(element);
		invoice.getReferenceId().add(element1);
	
		  
		NameIn nameIn=new NameIn();
		List<JAXBElement<String>> entityIdCodeOrNameOrIdCodeQual=new ArrayList<>();
		entityIdCodeOrNameOrIdCodeQual.add(new JAXBElement(new QName("EntityIdCode"), String.class, "ST"));
		entityIdCodeOrNameOrIdCodeQual.add(new JAXBElement(new QName("Name"), String.class, "Joe"));
		
		nameIn.setEntityIdCodeOrNameOrIdCodeQual(entityIdCodeOrNameOrIdCodeQual);
		Name name =new Name();
		name.setNameIn(nameIn);
		
	
		
		
		
		AddrInfo addrInfo=new AddrInfo();
		List<String> addrInfoList=new ArrayList<>();
		addrInfoList.add("100 Westwood");
		addrInfo.setAddrInfo(addrInfoList);
		//invoice.getName().setAddrInfo(addrInfo);
		name.setAddrInfo(addrInfo);
		
		GeographicLocation geographicLocation=new GeographicLocation();
		geographicLocation.setCityName("Los Angeles");
		geographicLocation.setStateOrProvinceCode("CA");
		geographicLocation.setPostalCode(90000);
		geographicLocation.setCountryCode("US");
		//invoice.getName().setGeographicLocation(geographicLocation);
		name.setGeographicLocation(geographicLocation);
	
		
		invoice.setName(name);
		
		DateTimeReference DateTimeReference=new DateTimeReference();
		short DateTimeQual=205;
		DateTimeReference.setDate(20090721);
		DateTimeReference.setDateTimeQual(DateTimeQual);
		DateTimeReference DateTimeReference1=new DateTimeReference();
		short DateTimeQual1=012;
		DateTimeReference1.setDate(20090901);
		DateTimeReference1.setDateTimeQual(DateTimeQual1);
		invoice.getDateTimeReference().add(DateTimeReference);
		invoice.getDateTimeReference().add(DateTimeReference1);
		
	
		BaselineItemData baselineItemData=new BaselineItemData();
		BaselineItemDataIn baselineItemDataIn=new BaselineItemDataIn();
		List<JAXBElement<? extends Serializable>> assignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode=new ArrayList<>();
		assignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode.add(new JAXBElement(new QName("AssignedId"), String.class, "1"));
		assignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode.add(new JAXBElement(new QName("QuantityInvoiced"), String.class, "2"));
		assignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode.add(new JAXBElement(new QName("UnitOrBasisForMeasurementCode"), String.class, "EA"));
		assignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode.add(new JAXBElement(new QName("UnitPrice"), String.class, "50.00"));
		assignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode.add(new JAXBElement(new QName("ProductServiceIdQual"), String.class, "PO"));
		assignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode.add(new JAXBElement(new QName("ProductServiceId"), String.class, "1"));
		baselineItemDataIn.setAssignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode(assignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode);
		
		baselineItemData.setBaselineItemDataIn(baselineItemDataIn);
		TaxInfo taxInfo=new TaxInfo();
		taxInfo.setTaxTypeCode("SU");
		taxInfo.setMonetaryAmount(100.00f);
		taxInfo.setPercent("9.75");
		baselineItemData.setTaxInfo(taxInfo);
		
		BaselineItemData baselineItemData1=new BaselineItemData();
		BaselineItemDataIn baselineItemDataIn1=new BaselineItemDataIn();
		List<JAXBElement<? extends Serializable>> assignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode1=new ArrayList<>();
		assignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode1.add(new JAXBElement(new QName("AssignedId"), String.class, "2"));
		assignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode1.add(new JAXBElement(new QName("ProductServiceIdQual"), String.class, "PO"));
		assignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode1.add(new JAXBElement(new QName("ProductServiceId"), String.class, "2"));
		baselineItemDataIn1.setAssignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode(assignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode1);
		
	
		 
		baselineItemData1.setBaselineItemDataIn(baselineItemDataIn1);
		TaxInfo taxInfo1=new TaxInfo();
		taxInfo1.setTaxTypeCode("SU");
		taxInfo1.setMonetaryAmount(10.00f);
	
		baselineItemData1.setTaxInfo(taxInfo1);
		
		
		
		
		
		
		invoice.getBaselineItemData().add(baselineItemData);
		invoice.getBaselineItemData().add(baselineItemData1);
		
		List<String> amount=new ArrayList<>();
		amount.add("99.75");
		
		TotalMonetaryValueSummary totalMonetaryValueSummary=new TotalMonetaryValueSummary();
		totalMonetaryValueSummary.setAmount(amount);
		invoice.setTotalMonetaryValueSummary(totalMonetaryValueSummary);
		
		generated_1603090170802.Invoice.TaxInfo taxInfomain=new generated_1603090170802.Invoice.TaxInfo();
		taxInfomain.setTaxTypeCode("SU");
		taxInfomain.setMonetaryAmount(9.75f);
	    invoice.setTaxInfo(taxInfomain);
		
		
	    
		
		MonetaryAmount monetaryAmount=new MonetaryAmount();
		monetaryAmount.setAmountQualCode("D8");
		monetaryAmount.setMonetaryAmount(1.5f);
		
		invoice.setMonetaryAmount(monetaryAmount);
		  System.out.println("invoice-------810 out---" +invoice.toString());
		 // exchange.getOut().setBody(invoice);
		  exchange.getOut().setBody(invoiceDB);
		  
		  
		 
	}}
