package com.myinc.processor;

import java.util.List;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class InvoiceHeaderIP  implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		List<Map<String, String>> list = exchange.getIn().getBody(List.class);


			
		Map<String,String> map=list.get(0);
		//System.out.println("InvoiceHeaderIP--------------" + map);
		
		exchange.setProperty("InvoiceNum", map.get("InvoiceNum"));
		exchange.setProperty("InvoiceHeader",map);
		
	}
	
}
