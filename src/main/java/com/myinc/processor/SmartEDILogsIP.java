package com.myinc.processor;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class SmartEDILogsIP implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		 String stringdata = exchange.getIn().getBody(String.class);
		Map<String, String> mqMap = exchange.getIn().getBody(Map.class);
		System.out.println("SmartEDILogsIP------stringdata---"+stringdata+"   mqMap-----------------" + mqMap);
		
		if(mqMap==null) {
		//System.out.println("SmartEDILogsIP------stringdata--------------------" + stringdata);
		

		Map<String, String> reconstructedUtilMap = Arrays.stream(stringdata.split(","))
	            .map(s -> s.split(":"))
	            .collect(Collectors.toMap(s -> s[0], s -> s[1]));
		//System.out.println("SmartEDILogsIP--------------------------" + reconstructedUtilMap);
		exchange.getOut().setBody(reconstructedUtilMap);}else {
			exchange.getOut().setBody(mqMap);
		}
		
		

	}
	
	


}