package com.myinc.processor;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import com.myinc.xmltojson.Interchange;
import com.myinc.xmltojson.output.Application;
import com.myinc.xmltojson.output.BusinessKeyPerformanceValues;
import com.myinc.xmltojson.output.Element;
import com.myinc.xmltojson.output.Element_;
import com.myinc.xmltojson.output.Element__;
import com.myinc.xmltojson.output.Element___;
import com.myinc.xmltojson.output.Element____;
import com.myinc.xmltojson.output.ErrorDetails;
import com.myinc.xmltojson.output.ErrorInfo;
import com.myinc.xmltojson.output.InterchangeOutPut;
import com.myinc.xmltojson.output.InterfaceDetails;
import com.myinc.xmltojson.output.Payload;
import com.myinc.xmltojson.output.RelationTransactionID;
import com.myinc.xmltojson.output.TechnicalKeyValues;
import com.myinc.xmltojson.output.TransactionInfo;
import com.myinc.xmltojson.output.TransationDetails;
import com.myinc.xmltojson.output.TransationDetails_;





public class XmlInputIP implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		
		try {
			System.out.println("Header data----------"+exchange.getIn().getHeaders());
			Interchange Interchange = exchange.getIn().getBody(Interchange.class);

			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			Date date = new Date();
			System.out.println(formatter.format(date));
			
			
			Random rand = new Random();
			int num = rand.nextInt(9000000) + 1000000;
			
			System.out.println("XmlInputIP--------Interchange 6------" + Interchange.getMeta().getISA06());
			System.out.println("XmlInputIP--------Interchange 8------" + Interchange.getMeta().getISA08());

			InterchangeOutPut interchangeOutPut = new InterchangeOutPut();

			
			//-----------------------Application Start-------------------------------
			Application application = new Application();
			application.setCorrelationID("");
			application.setDocumentType("X12 4010 850");
			application.setEnvironmentType("DEV");
			application.setLogType("B2B");
			application.setOrganisation("CG");
			application.setProjectID("");
			application.setSystem("ArcESB");
			application.setTransactionDateTimeStamp(formatter.format(date));
			application.setTransactionID(num+"-2934-4fdf-a106-d1dc75d777e3");
			application.setTransactionStatus("Success");
			application.setTransactionType("");
			
			interchangeOutPut.setApplication(application);
			//-----------------------Application End-------------------------------
			
			//-----------------------ErrorInfo Start-------------------------------
			ErrorInfo ErrorInfo=new ErrorInfo();
			ErrorDetails ErrorDetails=new ErrorDetails();
						
			Element elementErr=new Element();
			elementErr.setErronNotification("");
			elementErr.setErrorComment("");
			elementErr.setErrorDump("");
			elementErr.setErrorID("");
			elementErr.setErrorLocation("");
			elementErr.setErrorMessage("");
			elementErr.setErrorStatus("");
			elementErr.setErrorType("");
			ErrorDetails.setElement(elementErr);
			ErrorInfo.setErrorDetails(ErrorDetails);
			interchangeOutPut.setErrorInfo(ErrorInfo);
			
			//-----------------------ErrorInfo End-------------------------------
			
			//-----------------------InterfaceDetails Start-------------------------------
			InterfaceDetails interfaceDetails = new InterfaceDetails();
			BusinessKeyPerformanceValues BusinessKeyPerformanceValues=new BusinessKeyPerformanceValues();
			Element_ elementBusinessKeyPerformanceValues=new Element_();
			elementBusinessKeyPerformanceValues.setKeyPerformanceIndicator1("");
			elementBusinessKeyPerformanceValues.setKeyPerformanceIndicator2("");
			elementBusinessKeyPerformanceValues.setKeyPerformanceIndicator3("");
			elementBusinessKeyPerformanceValues.setKeyPerformanceIndicator4("");
			elementBusinessKeyPerformanceValues.setKeyPerformanceIndicator5("");
			elementBusinessKeyPerformanceValues.setKeyPerformanceKeyName1("");
			elementBusinessKeyPerformanceValues.setKeyPerformanceKeyName2("");
			elementBusinessKeyPerformanceValues.setKeyPerformanceKeyName3("");
			elementBusinessKeyPerformanceValues.setKeyPerformanceKeyName4("");
			elementBusinessKeyPerformanceValues.setKeyPerformanceKeyName5("");
			BusinessKeyPerformanceValues.setElement(elementBusinessKeyPerformanceValues);
			interfaceDetails.setBusinessKeyPerformanceValues(BusinessKeyPerformanceValues);
			
			interfaceDetails.setInterfaceID("PO_001");
			interfaceDetails.setInterfaceModule("PurchaseOrder");
			interfaceDetails.setInterfaceType("Inbound");
			interfaceDetails.setRegion("");
			interfaceDetails.setSourceSystemID(Interchange.getMeta().getISA06());
			interfaceDetails.setSourceSystemTranID("");
			interfaceDetails.setTargetSystemID(Interchange.getMeta().getISA08());
			
			
			TechnicalKeyValues TechnicalKeyValues=new TechnicalKeyValues();
			Element__ elementTechnicalKeyValues=new Element__();
			elementTechnicalKeyValues.setTransactionKeyName("");
			elementTechnicalKeyValues.setTransactionKeyReference("");
			elementTechnicalKeyValues.setTransactionKeyValue("");
			TechnicalKeyValues.setElement(elementTechnicalKeyValues);
			interfaceDetails.setTechnicalKeyValues(TechnicalKeyValues);
	
			interchangeOutPut.setInterfaceDetails(interfaceDetails);
			
			//-----------------------InterfaceDetails End-------------------------------
			
			//-----------------------TransactionInfo Start-------------------------------
			
			TransactionInfo TransactionInfo = new TransactionInfo();
			TransationDetails TransationDetails=new TransationDetails();
			TransationDetails_ TransationDetailsin=new TransationDetails_();
			Element___ element=new Element___();
			Payload Payload = new Payload();
			Payload.setPayload(Interchange.toString());
			Payload.setSourceType("");
			Payload.setSourceTypeID("");
			element.setPayload(Payload);
	
			RelationTransactionID RelationTransactionID=new RelationTransactionID();
			Element____ elementRelationTransactionID=new Element____();
			elementRelationTransactionID.setChildID("");
			elementRelationTransactionID.setParentTransactionID("");
			
			RelationTransactionID.setElement(elementRelationTransactionID);
			
			element.setRelationTransactionID(RelationTransactionID);
			TransationDetailsin.setElement(element);
			
			
			TransationDetails.setTransationDetails(TransationDetailsin);
			TransactionInfo.setTransationDetails(TransationDetails);

			interchangeOutPut.setTransactionInfo(TransactionInfo);
			
			//-----------------------TransactionInfo End-------------------------------
			
			
			
			

			exchange.getOut().setBody(interchangeOutPut);
		} catch (Exception e) {

			System.out.println("XmlInputIP---Exception-----Interchange------" + e.getMessage());

		}

	}

}
