package com.myinc.routebuilder;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary; 

public class ElasticSearchRoute extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		from("activemq:queue:ElasticSearchRoute")/*.unmarshal()
		.json(JsonLibrary.Jackson)*/.process(new Processor() {
			public void process(Exchange exchange) throws Exception {

				List<Map<String, Object>> result = new ArrayList<>();
				System.out.println("---------ElasticSearchRoute-----------" + exchange.getIn().getBody(String.class));
				
			/*	List<Map<String, Object>> list = exchange.getIn().getBody(List.class);

				System.out.println("ElasticSearchRoute--------list------" + list);*/
				String ip = exchange.getIn().getBody(String.class);

				result = getStringToListOfMap(ip);
				for (Map<String, Object> map : result) {
					try {

						ProducerTemplate template = exchange.getContext().createProducerTemplate();

						String indexId = template.requestBody("direct:index", map, String.class);
						exchange.getOut().setBody(indexId);

					} catch (Exception e) {
						e.printStackTrace();
					}

				}

			}
		});

		from("direct:index").to("activemq:queue:InsertESError");

	}

	private static List<Map<String, Object>> getStringToListOfMap(String ip) {

		ip = ip.substring(1, ip.length() - 1);

		ip = ip.replace("},{", "}@{");

		String[] array = ip.split("@");

		List<Map<String, Object>> finalList = new ArrayList<Map<String, Object>>();
		for (String string : array) {
			string = string.substring(1, string.length() - 1);
			string = string.replace(",\"", "\"@\"");
			String[] map = string.split("@");

			Map<String, Object> finalMap = new HashMap<String, Object>();

			for (String string2 : map) {
				string2 = string2.replace("\"\"", "\"");
				string2 = string2.replace("\":", "\"@\"");

				string2 = string2.replace("\"", "");

				String[] mapkeyvalue = string2.split("@");

				if (mapkeyvalue[0].equals("timestamp")) {

					String timeStampDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
							.format(new Date(Long.parseLong(mapkeyvalue[1])));
					finalMap.put(mapkeyvalue[0], timeStampDate);

				} else {

					finalMap.put(mapkeyvalue[0], mapkeyvalue[1]);
				}

			}

			System.out.println(finalMap);
			finalList.add(finalMap);

		}

		return finalList;

	}
	
	


}
