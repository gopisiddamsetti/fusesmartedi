package com.myinc.routebuilder;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;

import com.myinc.xmltojson.Interchange;
import com.myinc.xmltojson.Interchange.FunctionalGroup.TransactionSet.TX00401850.PO1Loop1; 

public class InsertMultiRoute extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		from("activemq:topic:xmlActivemqArcesb").from("activemq:topic:purchaseOrder850Test").process(new Processor() {
			public void process(Exchange exchange) throws Exception {

				Interchange Interchange = exchange.getIn().getBody(Interchange.class);
				
				
				System.out.println("Interchange-----------------"+Interchange.toString());
				
				List<Map<String, String>> purchaseOrderHeader=new ArrayList<>();
			    List<Map<String, String>> purchaseOrderAddress=new ArrayList<>();
				List<Map<String, String>> purchaseOrderItems=new ArrayList<>();
				
				Map<String, String> purchaseOrderHeaderMap=new HashMap<>();
				purchaseOrderHeaderMap.put("PurchaseOrderNumber", String.valueOf(Interchange.getFunctionalGroup().getTransactionSet().getTX00401850().getBEG().getBEG03()));
				purchaseOrderHeaderMap.put("OrderDate", String.valueOf(Interchange.getFunctionalGroup().getTransactionSet().getTX00401850().getBEG().getBEG05()));
				purchaseOrderHeader.add(purchaseOrderHeaderMap);
				
				
	
				
				
				Map<String, String> purchaseOrderAddressMap=new HashMap<>();
			
				purchaseOrderAddressMap.put("PurchaseOrderNumber", String.valueOf(Interchange.getFunctionalGroup().getTransactionSet().getTX00401850().getBEG().getBEG03()));
				purchaseOrderAddressMap.put("Name", Interchange.getFunctionalGroup().getTransactionSet().getTX00401850().getN1Loop1().getN1().getN102());
				purchaseOrderAddressMap.put("Street", Interchange.getFunctionalGroup().getTransactionSet().getTX00401850().getN1Loop1().getN3().getN301());
				purchaseOrderAddressMap.put("City", Interchange.getFunctionalGroup().getTransactionSet().getTX00401850().getN1Loop1().getN4().getN401());
				purchaseOrderAddressMap.put("State", Interchange.getFunctionalGroup().getTransactionSet().getTX00401850().getN1Loop1().getN4().getN402());
				purchaseOrderAddressMap.put("Zip",  String.valueOf(Interchange.getFunctionalGroup().getTransactionSet().getTX00401850().getN1Loop1().getN4().getN403()));
				purchaseOrderAddress.add(purchaseOrderAddressMap);
				
				
				
				System.out.println("PurchaseOrder"+"------"+Interchange.getFunctionalGroup().getTransactionSet().getTX00401850().getBEG().getBEG03());
				System.out.println("OrderDate"+"------"+Interchange.getFunctionalGroup().getTransactionSet().getTX00401850().getBEG().getBEG05());
				System.out.println("Name"+"------"+Interchange.getFunctionalGroup().getTransactionSet().getTX00401850().getN1Loop1().getN1().getN102());
				System.out.println("Street"+"------"+Interchange.getFunctionalGroup().getTransactionSet().getTX00401850().getN1Loop1().getN3().getN301());
				System.out.println("City"+"------"+Interchange.getFunctionalGroup().getTransactionSet().getTX00401850().getN1Loop1().getN4().getN401());
				System.out.println("State"+"------"+Interchange.getFunctionalGroup().getTransactionSet().getTX00401850().getN1Loop1().getN4().getN402());
				System.out.println("Zip"+"------"+Interchange.getFunctionalGroup().getTransactionSet().getTX00401850().getN1Loop1().getN4().getN403());
				
				//System.out.println("Zip"+"------"+);
		
				//PO1Loop1 obj = Interchange.getFunctionalGroup().getTransactionSet().getTX00401850().getPO1Loop1().get(0);
				
				for (PO1Loop1 obj : Interchange.getFunctionalGroup().getTransactionSet().getTX00401850().getPO1Loop1()) {
					System.out.println("PartNumber"+"------"+obj.getPO1().getPO107());
					System.out.println("ProductName"+"------"+obj.getPIDLoop1().getPID().getPID05());
					System.out.println("Quantity"+"------"+obj.getPO1().getPO102());
					System.out.println("USPrice"+"------"+obj.getPO1().getPO104());
					
					Map<String, String> map=new HashMap<>();
					
					 
					map.put("PurchaseOrderNumber",String.valueOf(Interchange.getFunctionalGroup().getTransactionSet().getTX00401850().getBEG().getBEG03()));
					map.put("PartNumber", obj.getPO1().getPO107());
					map.put("ProductName", obj.getPIDLoop1().getPID().getPID05());
					map.put("Quantity", String.valueOf(obj.getPO1().getPO102()));
					map.put("USPrice", String.valueOf(obj.getPO1().getPO104()));
					purchaseOrderItems.add(map);
					
					
					
				}
				
				
				//exchange.getOut().setBody(listData);
				
			
				
				
				
				for (Map<String, String> map : purchaseOrderHeader) {
					try {

						ProducerTemplate template = exchange.getContext().createProducerTemplate();

						String indexId = template.requestBody("direct:PurchaseOrderHeader", map, String.class);
						exchange.getOut().setBody(indexId);

					} catch (Exception e) {
						e.printStackTrace();
					}

				}
				
				for (Map<String, String> map : purchaseOrderAddress) {
					try {

						ProducerTemplate template = exchange.getContext().createProducerTemplate();

						String indexId = template.requestBody("direct:PurchaseOrderAddress", map, String.class);
						exchange.getOut().setBody(indexId);

					} catch (Exception e) {
						e.printStackTrace();
					}

				}
				
				for (Map<String, String> map : purchaseOrderItems) {
					try {

						ProducerTemplate template = exchange.getContext().createProducerTemplate();

						String indexId = template.requestBody("direct:PurchaseOrderItems", map, String.class);
						exchange.getOut().setBody(indexId);

					} catch (Exception e) {
						e.printStackTrace();
					}

				}
				
				

			}
		});

		//from("direct:index").to("activemq:queue:InsertESError");

	}

	}
