package com.myinc.routebuilder;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

public class InvoiceStatusRoute extends RouteBuilder {

	@Override
	public void configure() throws Exception {

		PropertiesConfiguration properties = new PropertiesConfiguration("Timer.properties");
		
	       from("scheduler://InvioceStatus?delay="+properties.getProperty("SchedulerTime"))
	       .to("sqlComponent:select * from InvoiceStatus where Status='Pending'?dataSource=dataSource")
	       .to("direct:InvoiceStatusRoute");
	       
		from("direct:InvoiceStatusRoute").process(new Processor() {
			public void process(Exchange exchange) throws Exception {
				try {
				List<Map<String, String>> result = exchange.getIn().getBody(List.class);
			if(result.size()>0) {
				
System.out.println("SchedulerTime----------------"+properties.getProperty("SchedulerTime"));
			
				properties.setProperty("SchedulerTime", "900000");

				//properties.save();
				
				System.out.println("config.properties updated Successfully!! 900000");
				for (Map<String, String> map : result) {
					try {

						ProducerTemplate template = exchange.getContext().createProducerTemplate();

						String indexId = null;

						int InvioceUpdateCount = 0;
						int Invioce810xmlCount = 0;

						for (int a = 0; a < 5; a = a + 1) {
							try {
								System.out.println("*****************---------");
								indexId = template.requestBody("direct:InvioceUpdateStatus", map, String.class);
								InvioceUpdateCount = 1;
								if (InvioceUpdateCount == 1) {
									break;
								}
								System.out.println("indexId---------" + indexId);
							} catch (Exception e) {

								System.out.println(indexId + "----Exception-----" + e.getMessage());
							}
							TimeUnit.SECONDS.sleep(10);

						}
						String indexId1 = null;
						
						if(InvioceUpdateCount == 1) {
						try {

							indexId1 = template.requestBody("direct:Invioce810xml", map, String.class);
							Invioce810xmlCount = 1;
						} catch (Exception e) {

							System.out.println(indexId1 + "----Exception-----" + e.getMessage());
						}}

						try {
							if (Invioce810xmlCount == 1 && InvioceUpdateCount == 1) {
								properties.setProperty("SchedulerTime", "60000");

								//properties.save();
							}
							System.out.println("config.properties updated Successfully!! 60000");
						} catch (Exception e) {
							System.out.println(e.getMessage());
						}

						exchange.getOut().setBody(indexId1);

					} catch (Exception e) {
						e.printStackTrace();
					}

				}
				
				
				
				}else {
					System.out.println("list+----- size ---------------"+result.size());
				}
				}catch (Exception e) {
					System.out.println("--file prop-----------------------"+e.getMessage());
				}

			}
		});

	}
}
