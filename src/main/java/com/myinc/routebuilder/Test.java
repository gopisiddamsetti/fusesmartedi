package com.myinc.routebuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

public class Test {
	public static void main(String[] args) {
		
		/*try {
		 * PropertiesConfiguration properties;
			properties = new PropertiesConfiguration("src//main//resources//config//FuseTms.properties");
			properties.setProperty("SchedulerTime", "900000");

			properties.save();
		} catch (ConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/

		/*String timeStampDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
				.format(new Date(1596120047000L));
		
		System.out.println(timeStampDate);*/

		
	//	2020-07-29T20:10:47.1890+05:30
		//getStringToListOfMap("[{\"id\":3,\"timestamp\":1596120047000,\"type\":\"Error\",\"script\":\"~/api.rsc\",\"message\":\"[X121] Unable to execute work queue.: : xJN: [SendFile] [sample/X121]Invalid input: Document contains an open transaction set.[id: X121-20200728-201045188-BEK_] Stack Trace: at RSSBus.PortOps.SendFileOpBase.ExF(xRk ) at ArcESB200a.xhx.WaQ.b() Inner Exception: [Translate] [sample/X121]Invalid input: Document contains an open transaction set.[id: X121-20200728-201045188-BEK_] Inner Stack Trace: at RSSBus.EdiOps.EDISendFile.EkN(xRk , String[] ) at RSSBus.PortOps.SendFileOpBase.ExF(xRk ) Inner Exception: Invalid input: Document contains an open transaction set. Inner Stack Trace: at ArcESB200a.DJA.Ba() at RSSBus.EdiOps.EDISendFile.EkN(xRk , String[] ) Inner Exception: Invalid input: Document contains an open transaction set. Inner Stack Trace: at ArcESB200a.sDD.bH() at ArcESB200a.sDD.br() at ArcESB200a.DJA.Ba()\"}]");
	}

	private static List<Map<String, Object>> getStringToListOfMap(String ip) {

		ip = ip.substring(1, ip.length() - 1);

		ip = ip.replace("},{", "}@{");

		String[] array = ip.split("@");

		List<Map<String, Object>> finalList = new ArrayList<Map<String, Object>>();
		for (String string : array) {
			string = string.substring(1, string.length() - 1);
			string = string.replace(",\"", "\"@\"");
			String[] map = string.split("@");

			Map<String, Object> finalMap = new HashMap<String, Object>();

			for (String string2 : map) {
				string2 = string2.replace("\"\"", "\"");
				string2 = string2.replace("\":", "\"@\"");

				string2 = string2.replace("\"", "");

				String[] mapkeyvalue = string2.split("@");

				if (mapkeyvalue[0].equals("timestamp")) {

					String timeStampDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
							.format(new Date(Long.parseLong(mapkeyvalue[1])));
					finalMap.put(mapkeyvalue[0], timeStampDate);

				} else {

					finalMap.put(mapkeyvalue[0], mapkeyvalue[1]);
				}

			}

			System.out.println(finalMap);
			finalList.add(finalMap);

		}

		return finalList;

	}
}
