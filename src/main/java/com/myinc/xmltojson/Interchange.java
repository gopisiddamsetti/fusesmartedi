
package com.myinc.xmltojson;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Meta">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ISA01" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *                   &lt;element name="ISA02" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ISA03" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *                   &lt;element name="ISA04" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ISA05" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *                   &lt;element name="ISA06" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ISA07" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *                   &lt;element name="ISA08" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ISA09" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="ISA10" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *                   &lt;element name="ISA11" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ISA12" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *                   &lt;element name="ISA13" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *                   &lt;element name="ISA14" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *                   &lt;element name="ISA15" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ISA16" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="FunctionalGroup">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Meta">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="GS01" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="GS02" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="GS03" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="GS04" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                             &lt;element name="GS05" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *                             &lt;element name="GS06" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *                             &lt;element name="GS07" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="GS08" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="TransactionSet">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="TX-00401-850">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="Meta">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="ST01" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *                                                 &lt;element name="ST02" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="BEG">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="BEG01" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *                                                 &lt;element name="BEG02" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="BEG03" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                                                 &lt;element name="BEG04" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="BEG05" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                                 &lt;element name="BEG06" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                               &lt;/sequence>
 *                                               &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="REF" maxOccurs="unbounded" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="REF01">
 *                                                   &lt;simpleType>
 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                       &lt;enumeration value="DP"/>
 *                                                       &lt;enumeration value="PS"/>
 *                                                     &lt;/restriction>
 *                                                   &lt;/simpleType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="REF02">
 *                                                   &lt;simpleType>
 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                       &lt;enumeration value="038"/>
 *                                                       &lt;enumeration value="R"/>
 *                                                     &lt;/restriction>
 *                                                   &lt;/simpleType>
 *                                                 &lt;/element>
 *                                               &lt;/sequence>
 *                                               &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="ITD">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="ITD01" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *                                                 &lt;element name="ITD02" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *                                                 &lt;element name="ITD03" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *                                                 &lt;element name="ITD04" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="ITD05" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *                                                 &lt;element name="ITD06" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="ITD07" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *                                               &lt;/sequence>
 *                                               &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="DTM">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="DTM01" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *                                                 &lt;element name="DTM02" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                               &lt;/sequence>
 *                                               &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="PKG" maxOccurs="unbounded" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="PKG01" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="PKG02">
 *                                                   &lt;simpleType>
 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
 *                                                       &lt;enumeration value="68"/>
 *                                                       &lt;enumeration value="66"/>
 *                                                     &lt;/restriction>
 *                                                   &lt;/simpleType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="PKG03" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="PKG04" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="PKG05">
 *                                                   &lt;simpleType>
 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                       &lt;enumeration value="PALLETIZE SHIPMENT"/>
 *                                                       &lt;enumeration value="REGULAR"/>
 *                                                     &lt;/restriction>
 *                                                   &lt;/simpleType>
 *                                                 &lt;/element>
 *                                               &lt;/sequence>
 *                                               &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="TD5">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="TD501" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="TD502" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *                                                 &lt;element name="TD503" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="TD504" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="TD505" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                               &lt;/sequence>
 *                                               &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="N1Loop1">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="N1">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="N101" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="N102" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="N103" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *                                                           &lt;element name="N104" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                                                         &lt;/sequence>
 *                                                         &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="N3">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="N301" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                         &lt;/sequence>
 *                                                         &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="N4">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="N401" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="N402" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="N403" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                                         &lt;/sequence>
 *                                                         &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                               &lt;/sequence>
 *                                               &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="PO1Loop1" maxOccurs="unbounded" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="PO1">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="PO101">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
 *                                                                 &lt;enumeration value="1"/>
 *                                                                 &lt;enumeration value="2"/>
 *                                                                 &lt;enumeration value="3"/>
 *                                                                 &lt;enumeration value="4"/>
 *                                                                 &lt;enumeration value="5"/>
 *                                                                 &lt;enumeration value="6"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="PO102">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}short">
 *                                                                 &lt;enumeration value="120"/>
 *                                                                 &lt;enumeration value="220"/>
 *                                                                 &lt;enumeration value="126"/>
 *                                                                 &lt;enumeration value="76"/>
 *                                                                 &lt;enumeration value="72"/>
 *                                                                 &lt;enumeration value="696"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="PO103" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="PO104">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}float">
 *                                                                 &lt;enumeration value="9.25"/>
 *                                                                 &lt;enumeration value="13.79"/>
 *                                                                 &lt;enumeration value="10.99"/>
 *                                                                 &lt;enumeration value="4.35"/>
 *                                                                 &lt;enumeration value="7.5"/>
 *                                                                 &lt;enumeration value="9.55"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="PO105" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="PO106" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="PO107">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                 &lt;enumeration value="065322-117"/>
 *                                                                 &lt;enumeration value="066850-116"/>
 *                                                                 &lt;enumeration value="060733-110"/>
 *                                                                 &lt;enumeration value="065308-116"/>
 *                                                                 &lt;enumeration value="065374-118"/>
 *                                                                 &lt;enumeration value="067504-118"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="PO108" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="PO109" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="PO110" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="PO111">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                 &lt;enumeration value="AB3542"/>
 *                                                                 &lt;enumeration value="RD5322"/>
 *                                                                 &lt;enumeration value="XY5266"/>
 *                                                                 &lt;enumeration value="VX2332"/>
 *                                                                 &lt;enumeration value="RV0524"/>
 *                                                                 &lt;enumeration value="DX1875"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                         &lt;/sequence>
 *                                                         &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="PIDLoop1">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="PID">
 *                                                             &lt;complexType>
 *                                                               &lt;complexContent>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                   &lt;sequence>
 *                                                                     &lt;element name="PID01" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                                     &lt;element name="PID02" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                                     &lt;element name="PID03" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                                     &lt;element name="PID04" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                                     &lt;element name="PID05">
 *                                                                       &lt;simpleType>
 *                                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                                                           &lt;enumeration value="SMALL WIDGET"/>
 *                                                                           &lt;enumeration value="MEDIUM WIDGET"/>
 *                                                                           &lt;enumeration value="LARGE WIDGET"/>
 *                                                                           &lt;enumeration value="NANO WIDGET"/>
 *                                                                           &lt;enumeration value="BLUE WIDGET"/>
 *                                                                           &lt;enumeration value="ORANGE WIDGET"/>
 *                                                                         &lt;/restriction>
 *                                                                       &lt;/simpleType>
 *                                                                     &lt;/element>
 *                                                                   &lt;/sequence>
 *                                                                   &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                 &lt;/restriction>
 *                                                               &lt;/complexContent>
 *                                                             &lt;/complexType>
 *                                                           &lt;/element>
 *                                                         &lt;/sequence>
 *                                                         &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="PO4">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="PO401">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
 *                                                                 &lt;enumeration value="4"/>
 *                                                                 &lt;enumeration value="2"/>
 *                                                                 &lt;enumeration value="6"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="PO402">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
 *                                                                 &lt;enumeration value="4"/>
 *                                                                 &lt;enumeration value="2"/>
 *                                                                 &lt;enumeration value="1"/>
 *                                                                 &lt;enumeration value="6"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="PO403" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="PO404" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                                           &lt;element name="PO405" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                                           &lt;element name="PO406" minOccurs="0">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
 *                                                                 &lt;enumeration value="3"/>
 *                                                                 &lt;enumeration value="6"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="PO407" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                                           &lt;element name="PO408" minOccurs="0">
 *                                                             &lt;simpleType>
 *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
 *                                                                 &lt;enumeration value="15"/>
 *                                                                 &lt;enumeration value="12"/>
 *                                                                 &lt;enumeration value="19"/>
 *                                                                 &lt;enumeration value="10"/>
 *                                                               &lt;/restriction>
 *                                                             &lt;/simpleType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="PO409" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                                         &lt;/sequence>
 *                                                         &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                               &lt;/sequence>
 *                                               &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="CTTLoop1">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="CTT">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="CTT01" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *                                                         &lt;/sequence>
 *                                                         &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="AMT">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="AMT01" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *                                                           &lt;element name="AMT02" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *                                                         &lt;/sequence>
 *                                                         &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                               &lt;/sequence>
 *                                               &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                     &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "meta",
    "functionalGroup"
})
@XmlRootElement(name = "Interchange")
public class Interchange {

    @Override
	public String toString() {
		return "Interchange [meta=" + meta + ", functionalGroup=" + functionalGroup + "]";
	}


	@XmlElement(name = "Meta", required = true)
    protected Interchange.Meta meta;
    @XmlElement(name = "FunctionalGroup", required = true)
    protected Interchange.FunctionalGroup functionalGroup;

    /**
     * Gets the value of the meta property.
     * 
     * @return
     *     possible object is
     *     {@link Interchange.Meta }
     *     
     */
    public Interchange.Meta getMeta() {
        return meta;
    }

    /**
     * Sets the value of the meta property.
     * 
     * @param value
     *     allowed object is
     *     {@link Interchange.Meta }
     *     
     */
    public void setMeta(Interchange.Meta value) {
        this.meta = value;
    }

    /**
     * Gets the value of the functionalGroup property.
     * 
     * @return
     *     possible object is
     *     {@link Interchange.FunctionalGroup }
     *     
     */
    public Interchange.FunctionalGroup getFunctionalGroup() {
        return functionalGroup;
    }

    /**
     * Sets the value of the functionalGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link Interchange.FunctionalGroup }
     *     
     */
    public void setFunctionalGroup(Interchange.FunctionalGroup value) {
        this.functionalGroup = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Meta">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="GS01" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="GS02" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="GS03" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="GS04" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="GS05" type="{http://www.w3.org/2001/XMLSchema}short"/>
     *                   &lt;element name="GS06" type="{http://www.w3.org/2001/XMLSchema}short"/>
     *                   &lt;element name="GS07" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="GS08" type="{http://www.w3.org/2001/XMLSchema}short"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="TransactionSet">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="TX-00401-850">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="Meta">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="ST01" type="{http://www.w3.org/2001/XMLSchema}short"/>
     *                                       &lt;element name="ST02" type="{http://www.w3.org/2001/XMLSchema}byte"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="BEG">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="BEG01" type="{http://www.w3.org/2001/XMLSchema}byte"/>
     *                                       &lt;element name="BEG02" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="BEG03" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                                       &lt;element name="BEG04" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="BEG05" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                                       &lt;element name="BEG06" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                                     &lt;/sequence>
     *                                     &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="REF" maxOccurs="unbounded" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="REF01">
     *                                         &lt;simpleType>
     *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                             &lt;enumeration value="DP"/>
     *                                             &lt;enumeration value="PS"/>
     *                                           &lt;/restriction>
     *                                         &lt;/simpleType>
     *                                       &lt;/element>
     *                                       &lt;element name="REF02">
     *                                         &lt;simpleType>
     *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                             &lt;enumeration value="038"/>
     *                                             &lt;enumeration value="R"/>
     *                                           &lt;/restriction>
     *                                         &lt;/simpleType>
     *                                       &lt;/element>
     *                                     &lt;/sequence>
     *                                     &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="ITD">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="ITD01" type="{http://www.w3.org/2001/XMLSchema}byte"/>
     *                                       &lt;element name="ITD02" type="{http://www.w3.org/2001/XMLSchema}byte"/>
     *                                       &lt;element name="ITD03" type="{http://www.w3.org/2001/XMLSchema}byte"/>
     *                                       &lt;element name="ITD04" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="ITD05" type="{http://www.w3.org/2001/XMLSchema}byte"/>
     *                                       &lt;element name="ITD06" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="ITD07" type="{http://www.w3.org/2001/XMLSchema}byte"/>
     *                                     &lt;/sequence>
     *                                     &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="DTM">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="DTM01" type="{http://www.w3.org/2001/XMLSchema}byte"/>
     *                                       &lt;element name="DTM02" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                                     &lt;/sequence>
     *                                     &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="PKG" maxOccurs="unbounded" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="PKG01" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="PKG02">
     *                                         &lt;simpleType>
     *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
     *                                             &lt;enumeration value="68"/>
     *                                             &lt;enumeration value="66"/>
     *                                           &lt;/restriction>
     *                                         &lt;/simpleType>
     *                                       &lt;/element>
     *                                       &lt;element name="PKG03" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="PKG04" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="PKG05">
     *                                         &lt;simpleType>
     *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                             &lt;enumeration value="PALLETIZE SHIPMENT"/>
     *                                             &lt;enumeration value="REGULAR"/>
     *                                           &lt;/restriction>
     *                                         &lt;/simpleType>
     *                                       &lt;/element>
     *                                     &lt;/sequence>
     *                                     &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="TD5">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="TD501" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="TD502" type="{http://www.w3.org/2001/XMLSchema}byte"/>
     *                                       &lt;element name="TD503" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="TD504" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="TD505" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                     &lt;/sequence>
     *                                     &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="N1Loop1">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="N1">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="N101" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="N102" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="N103" type="{http://www.w3.org/2001/XMLSchema}byte"/>
     *                                                 &lt;element name="N104" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                                               &lt;/sequence>
     *                                               &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                       &lt;element name="N3">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="N301" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                               &lt;/sequence>
     *                                               &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                       &lt;element name="N4">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="N401" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="N402" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="N403" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                                               &lt;/sequence>
     *                                               &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                     &lt;/sequence>
     *                                     &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="PO1Loop1" maxOccurs="unbounded" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="PO1">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="PO101">
     *                                                   &lt;simpleType>
     *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
     *                                                       &lt;enumeration value="1"/>
     *                                                       &lt;enumeration value="2"/>
     *                                                       &lt;enumeration value="3"/>
     *                                                       &lt;enumeration value="4"/>
     *                                                       &lt;enumeration value="5"/>
     *                                                       &lt;enumeration value="6"/>
     *                                                     &lt;/restriction>
     *                                                   &lt;/simpleType>
     *                                                 &lt;/element>
     *                                                 &lt;element name="PO102">
     *                                                   &lt;simpleType>
     *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}short">
     *                                                       &lt;enumeration value="120"/>
     *                                                       &lt;enumeration value="220"/>
     *                                                       &lt;enumeration value="126"/>
     *                                                       &lt;enumeration value="76"/>
     *                                                       &lt;enumeration value="72"/>
     *                                                       &lt;enumeration value="696"/>
     *                                                     &lt;/restriction>
     *                                                   &lt;/simpleType>
     *                                                 &lt;/element>
     *                                                 &lt;element name="PO103" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="PO104">
     *                                                   &lt;simpleType>
     *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}float">
     *                                                       &lt;enumeration value="9.25"/>
     *                                                       &lt;enumeration value="13.79"/>
     *                                                       &lt;enumeration value="10.99"/>
     *                                                       &lt;enumeration value="4.35"/>
     *                                                       &lt;enumeration value="7.5"/>
     *                                                       &lt;enumeration value="9.55"/>
     *                                                     &lt;/restriction>
     *                                                   &lt;/simpleType>
     *                                                 &lt;/element>
     *                                                 &lt;element name="PO105" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="PO106" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="PO107">
     *                                                   &lt;simpleType>
     *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                                       &lt;enumeration value="065322-117"/>
     *                                                       &lt;enumeration value="066850-116"/>
     *                                                       &lt;enumeration value="060733-110"/>
     *                                                       &lt;enumeration value="065308-116"/>
     *                                                       &lt;enumeration value="065374-118"/>
     *                                                       &lt;enumeration value="067504-118"/>
     *                                                     &lt;/restriction>
     *                                                   &lt;/simpleType>
     *                                                 &lt;/element>
     *                                                 &lt;element name="PO108" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="PO109" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="PO110" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="PO111">
     *                                                   &lt;simpleType>
     *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                                       &lt;enumeration value="AB3542"/>
     *                                                       &lt;enumeration value="RD5322"/>
     *                                                       &lt;enumeration value="XY5266"/>
     *                                                       &lt;enumeration value="VX2332"/>
     *                                                       &lt;enumeration value="RV0524"/>
     *                                                       &lt;enumeration value="DX1875"/>
     *                                                     &lt;/restriction>
     *                                                   &lt;/simpleType>
     *                                                 &lt;/element>
     *                                               &lt;/sequence>
     *                                               &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                       &lt;element name="PIDLoop1">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="PID">
     *                                                   &lt;complexType>
     *                                                     &lt;complexContent>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                         &lt;sequence>
     *                                                           &lt;element name="PID01" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                           &lt;element name="PID02" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                           &lt;element name="PID03" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                           &lt;element name="PID04" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                           &lt;element name="PID05">
     *                                                             &lt;simpleType>
     *                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                                                 &lt;enumeration value="SMALL WIDGET"/>
     *                                                                 &lt;enumeration value="MEDIUM WIDGET"/>
     *                                                                 &lt;enumeration value="LARGE WIDGET"/>
     *                                                                 &lt;enumeration value="NANO WIDGET"/>
     *                                                                 &lt;enumeration value="BLUE WIDGET"/>
     *                                                                 &lt;enumeration value="ORANGE WIDGET"/>
     *                                                               &lt;/restriction>
     *                                                             &lt;/simpleType>
     *                                                           &lt;/element>
     *                                                         &lt;/sequence>
     *                                                         &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                       &lt;/restriction>
     *                                                     &lt;/complexContent>
     *                                                   &lt;/complexType>
     *                                                 &lt;/element>
     *                                               &lt;/sequence>
     *                                               &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                       &lt;element name="PO4">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="PO401">
     *                                                   &lt;simpleType>
     *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
     *                                                       &lt;enumeration value="4"/>
     *                                                       &lt;enumeration value="2"/>
     *                                                       &lt;enumeration value="6"/>
     *                                                     &lt;/restriction>
     *                                                   &lt;/simpleType>
     *                                                 &lt;/element>
     *                                                 &lt;element name="PO402">
     *                                                   &lt;simpleType>
     *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
     *                                                       &lt;enumeration value="4"/>
     *                                                       &lt;enumeration value="2"/>
     *                                                       &lt;enumeration value="1"/>
     *                                                       &lt;enumeration value="6"/>
     *                                                     &lt;/restriction>
     *                                                   &lt;/simpleType>
     *                                                 &lt;/element>
     *                                                 &lt;element name="PO403" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="PO404" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                                                 &lt;element name="PO405" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                                                 &lt;element name="PO406" minOccurs="0">
     *                                                   &lt;simpleType>
     *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
     *                                                       &lt;enumeration value="3"/>
     *                                                       &lt;enumeration value="6"/>
     *                                                     &lt;/restriction>
     *                                                   &lt;/simpleType>
     *                                                 &lt;/element>
     *                                                 &lt;element name="PO407" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                                                 &lt;element name="PO408" minOccurs="0">
     *                                                   &lt;simpleType>
     *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
     *                                                       &lt;enumeration value="15"/>
     *                                                       &lt;enumeration value="12"/>
     *                                                       &lt;enumeration value="19"/>
     *                                                       &lt;enumeration value="10"/>
     *                                                     &lt;/restriction>
     *                                                   &lt;/simpleType>
     *                                                 &lt;/element>
     *                                                 &lt;element name="PO409" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                                               &lt;/sequence>
     *                                               &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                     &lt;/sequence>
     *                                     &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="CTTLoop1">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="CTT">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="CTT01" type="{http://www.w3.org/2001/XMLSchema}byte"/>
     *                                               &lt;/sequence>
     *                                               &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                       &lt;element name="AMT">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="AMT01" type="{http://www.w3.org/2001/XMLSchema}byte"/>
     *                                                 &lt;element name="AMT02" type="{http://www.w3.org/2001/XMLSchema}float"/>
     *                                               &lt;/sequence>
     *                                               &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                     &lt;/sequence>
     *                                     &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                           &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "meta",
        "transactionSet"
    })
    public static class FunctionalGroup {

        @Override
		public String toString() {
			return "FunctionalGroup [meta=" + meta + ", transactionSet=" + transactionSet + "]";
		}


		@XmlElement(name = "Meta", required = true)
        protected Interchange.FunctionalGroup.Meta meta;
        @XmlElement(name = "TransactionSet", required = true)
        protected Interchange.FunctionalGroup.TransactionSet transactionSet;

        /**
         * Gets the value of the meta property.
         * 
         * @return
         *     possible object is
         *     {@link Interchange.FunctionalGroup.Meta }
         *     
         */
        public Interchange.FunctionalGroup.Meta getMeta() {
            return meta;
        }

        /**
         * Sets the value of the meta property.
         * 
         * @param value
         *     allowed object is
         *     {@link Interchange.FunctionalGroup.Meta }
         *     
         */
        public void setMeta(Interchange.FunctionalGroup.Meta value) {
            this.meta = value;
        }

        /**
         * Gets the value of the transactionSet property.
         * 
         * @return
         *     possible object is
         *     {@link Interchange.FunctionalGroup.TransactionSet }
         *     
         */
        public Interchange.FunctionalGroup.TransactionSet getTransactionSet() {
            return transactionSet;
        }

        /**
         * Sets the value of the transactionSet property.
         * 
         * @param value
         *     allowed object is
         *     {@link Interchange.FunctionalGroup.TransactionSet }
         *     
         */
        public void setTransactionSet(Interchange.FunctionalGroup.TransactionSet value) {
            this.transactionSet = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="GS01" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="GS02" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="GS03" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="GS04" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="GS05" type="{http://www.w3.org/2001/XMLSchema}short"/>
         *         &lt;element name="GS06" type="{http://www.w3.org/2001/XMLSchema}short"/>
         *         &lt;element name="GS07" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="GS08" type="{http://www.w3.org/2001/XMLSchema}short"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "gs01",
            "gs02",
            "gs03",
            "gs04",
            "gs05",
            "gs06",
            "gs07",
            "gs08"
        })
        public static class Meta {

            @Override
			public String toString() {
				return "Meta [gs01=" + gs01 + ", gs02=" + gs02 + ", gs03=" + gs03 + ", gs04=" + gs04 + ", gs05=" + gs05
						+ ", gs06=" + gs06 + ", gs07=" + gs07 + ", gs08=" + gs08 + "]";
			}

			@XmlElement(name = "GS01", required = true)
            protected String gs01;
            @XmlElement(name = "GS02", required = true)
            protected String gs02;
            @XmlElement(name = "GS03", required = true)
            protected String gs03;
            @XmlElement(name = "GS04")
            protected int gs04;
            @XmlElement(name = "GS05")
            protected short gs05;
            @XmlElement(name = "GS06")
            protected short gs06;
            @XmlElement(name = "GS07", required = true)
            protected String gs07;
            @XmlElement(name = "GS08")
            protected short gs08;

            /**
             * Gets the value of the gs01 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGS01() {
                return gs01;
            }

            /**
             * Sets the value of the gs01 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGS01(String value) {
                this.gs01 = value;
            }

            /**
             * Gets the value of the gs02 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGS02() {
                return gs02;
            }

            /**
             * Sets the value of the gs02 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGS02(String value) {
                this.gs02 = value;
            }

            /**
             * Gets the value of the gs03 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGS03() {
                return gs03;
            }

            /**
             * Sets the value of the gs03 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGS03(String value) {
                this.gs03 = value;
            }

            /**
             * Gets the value of the gs04 property.
             * 
             */
            public int getGS04() {
                return gs04;
            }

            /**
             * Sets the value of the gs04 property.
             * 
             */
            public void setGS04(int value) {
                this.gs04 = value;
            }

            /**
             * Gets the value of the gs05 property.
             * 
             */
            public short getGS05() {
                return gs05;
            }

            /**
             * Sets the value of the gs05 property.
             * 
             */
            public void setGS05(short value) {
                this.gs05 = value;
            }

            /**
             * Gets the value of the gs06 property.
             * 
             */
            public short getGS06() {
                return gs06;
            }

            /**
             * Sets the value of the gs06 property.
             * 
             */
            public void setGS06(short value) {
                this.gs06 = value;
            }

            /**
             * Gets the value of the gs07 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGS07() {
                return gs07;
            }

            /**
             * Sets the value of the gs07 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGS07(String value) {
                this.gs07 = value;
            }

            /**
             * Gets the value of the gs08 property.
             * 
             */
            public short getGS08() {
                return gs08;
            }

            /**
             * Sets the value of the gs08 property.
             * 
             */
            public void setGS08(short value) {
                this.gs08 = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="TX-00401-850">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="Meta">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="ST01" type="{http://www.w3.org/2001/XMLSchema}short"/>
         *                             &lt;element name="ST02" type="{http://www.w3.org/2001/XMLSchema}byte"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="BEG">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="BEG01" type="{http://www.w3.org/2001/XMLSchema}byte"/>
         *                             &lt;element name="BEG02" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="BEG03" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *                             &lt;element name="BEG04" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="BEG05" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                             &lt;element name="BEG06" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                           &lt;/sequence>
         *                           &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="REF" maxOccurs="unbounded" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="REF01">
         *                               &lt;simpleType>
         *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                   &lt;enumeration value="DP"/>
         *                                   &lt;enumeration value="PS"/>
         *                                 &lt;/restriction>
         *                               &lt;/simpleType>
         *                             &lt;/element>
         *                             &lt;element name="REF02">
         *                               &lt;simpleType>
         *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                   &lt;enumeration value="038"/>
         *                                   &lt;enumeration value="R"/>
         *                                 &lt;/restriction>
         *                               &lt;/simpleType>
         *                             &lt;/element>
         *                           &lt;/sequence>
         *                           &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="ITD">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="ITD01" type="{http://www.w3.org/2001/XMLSchema}byte"/>
         *                             &lt;element name="ITD02" type="{http://www.w3.org/2001/XMLSchema}byte"/>
         *                             &lt;element name="ITD03" type="{http://www.w3.org/2001/XMLSchema}byte"/>
         *                             &lt;element name="ITD04" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="ITD05" type="{http://www.w3.org/2001/XMLSchema}byte"/>
         *                             &lt;element name="ITD06" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="ITD07" type="{http://www.w3.org/2001/XMLSchema}byte"/>
         *                           &lt;/sequence>
         *                           &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="DTM">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="DTM01" type="{http://www.w3.org/2001/XMLSchema}byte"/>
         *                             &lt;element name="DTM02" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                           &lt;/sequence>
         *                           &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="PKG" maxOccurs="unbounded" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="PKG01" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="PKG02">
         *                               &lt;simpleType>
         *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
         *                                   &lt;enumeration value="68"/>
         *                                   &lt;enumeration value="66"/>
         *                                 &lt;/restriction>
         *                               &lt;/simpleType>
         *                             &lt;/element>
         *                             &lt;element name="PKG03" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="PKG04" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="PKG05">
         *                               &lt;simpleType>
         *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                   &lt;enumeration value="PALLETIZE SHIPMENT"/>
         *                                   &lt;enumeration value="REGULAR"/>
         *                                 &lt;/restriction>
         *                               &lt;/simpleType>
         *                             &lt;/element>
         *                           &lt;/sequence>
         *                           &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="TD5">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="TD501" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="TD502" type="{http://www.w3.org/2001/XMLSchema}byte"/>
         *                             &lt;element name="TD503" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="TD504" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="TD505" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                           &lt;/sequence>
         *                           &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="N1Loop1">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="N1">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="N101" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="N102" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="N103" type="{http://www.w3.org/2001/XMLSchema}byte"/>
         *                                       &lt;element name="N104" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *                                     &lt;/sequence>
         *                                     &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                             &lt;element name="N3">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="N301" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                     &lt;/sequence>
         *                                     &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                             &lt;element name="N4">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="N401" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="N402" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="N403" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                                     &lt;/sequence>
         *                                     &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                           &lt;/sequence>
         *                           &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="PO1Loop1" maxOccurs="unbounded" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="PO1">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="PO101">
         *                                         &lt;simpleType>
         *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
         *                                             &lt;enumeration value="1"/>
         *                                             &lt;enumeration value="2"/>
         *                                             &lt;enumeration value="3"/>
         *                                             &lt;enumeration value="4"/>
         *                                             &lt;enumeration value="5"/>
         *                                             &lt;enumeration value="6"/>
         *                                           &lt;/restriction>
         *                                         &lt;/simpleType>
         *                                       &lt;/element>
         *                                       &lt;element name="PO102">
         *                                         &lt;simpleType>
         *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}short">
         *                                             &lt;enumeration value="120"/>
         *                                             &lt;enumeration value="220"/>
         *                                             &lt;enumeration value="126"/>
         *                                             &lt;enumeration value="76"/>
         *                                             &lt;enumeration value="72"/>
         *                                             &lt;enumeration value="696"/>
         *                                           &lt;/restriction>
         *                                         &lt;/simpleType>
         *                                       &lt;/element>
         *                                       &lt;element name="PO103" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="PO104">
         *                                         &lt;simpleType>
         *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}float">
         *                                             &lt;enumeration value="9.25"/>
         *                                             &lt;enumeration value="13.79"/>
         *                                             &lt;enumeration value="10.99"/>
         *                                             &lt;enumeration value="4.35"/>
         *                                             &lt;enumeration value="7.5"/>
         *                                             &lt;enumeration value="9.55"/>
         *                                           &lt;/restriction>
         *                                         &lt;/simpleType>
         *                                       &lt;/element>
         *                                       &lt;element name="PO105" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="PO106" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="PO107">
         *                                         &lt;simpleType>
         *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                             &lt;enumeration value="065322-117"/>
         *                                             &lt;enumeration value="066850-116"/>
         *                                             &lt;enumeration value="060733-110"/>
         *                                             &lt;enumeration value="065308-116"/>
         *                                             &lt;enumeration value="065374-118"/>
         *                                             &lt;enumeration value="067504-118"/>
         *                                           &lt;/restriction>
         *                                         &lt;/simpleType>
         *                                       &lt;/element>
         *                                       &lt;element name="PO108" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="PO109" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="PO110" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="PO111">
         *                                         &lt;simpleType>
         *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                             &lt;enumeration value="AB3542"/>
         *                                             &lt;enumeration value="RD5322"/>
         *                                             &lt;enumeration value="XY5266"/>
         *                                             &lt;enumeration value="VX2332"/>
         *                                             &lt;enumeration value="RV0524"/>
         *                                             &lt;enumeration value="DX1875"/>
         *                                           &lt;/restriction>
         *                                         &lt;/simpleType>
         *                                       &lt;/element>
         *                                     &lt;/sequence>
         *                                     &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                             &lt;element name="PIDLoop1">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="PID">
         *                                         &lt;complexType>
         *                                           &lt;complexContent>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                               &lt;sequence>
         *                                                 &lt;element name="PID01" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                                 &lt;element name="PID02" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                                 &lt;element name="PID03" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                                 &lt;element name="PID04" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                                 &lt;element name="PID05">
         *                                                   &lt;simpleType>
         *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                                                       &lt;enumeration value="SMALL WIDGET"/>
         *                                                       &lt;enumeration value="MEDIUM WIDGET"/>
         *                                                       &lt;enumeration value="LARGE WIDGET"/>
         *                                                       &lt;enumeration value="NANO WIDGET"/>
         *                                                       &lt;enumeration value="BLUE WIDGET"/>
         *                                                       &lt;enumeration value="ORANGE WIDGET"/>
         *                                                     &lt;/restriction>
         *                                                   &lt;/simpleType>
         *                                                 &lt;/element>
         *                                               &lt;/sequence>
         *                                               &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                             &lt;/restriction>
         *                                           &lt;/complexContent>
         *                                         &lt;/complexType>
         *                                       &lt;/element>
         *                                     &lt;/sequence>
         *                                     &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                             &lt;element name="PO4">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="PO401">
         *                                         &lt;simpleType>
         *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
         *                                             &lt;enumeration value="4"/>
         *                                             &lt;enumeration value="2"/>
         *                                             &lt;enumeration value="6"/>
         *                                           &lt;/restriction>
         *                                         &lt;/simpleType>
         *                                       &lt;/element>
         *                                       &lt;element name="PO402">
         *                                         &lt;simpleType>
         *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
         *                                             &lt;enumeration value="4"/>
         *                                             &lt;enumeration value="2"/>
         *                                             &lt;enumeration value="1"/>
         *                                             &lt;enumeration value="6"/>
         *                                           &lt;/restriction>
         *                                         &lt;/simpleType>
         *                                       &lt;/element>
         *                                       &lt;element name="PO403" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="PO404" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                                       &lt;element name="PO405" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                                       &lt;element name="PO406" minOccurs="0">
         *                                         &lt;simpleType>
         *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
         *                                             &lt;enumeration value="3"/>
         *                                             &lt;enumeration value="6"/>
         *                                           &lt;/restriction>
         *                                         &lt;/simpleType>
         *                                       &lt;/element>
         *                                       &lt;element name="PO407" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                                       &lt;element name="PO408" minOccurs="0">
         *                                         &lt;simpleType>
         *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
         *                                             &lt;enumeration value="15"/>
         *                                             &lt;enumeration value="12"/>
         *                                             &lt;enumeration value="19"/>
         *                                             &lt;enumeration value="10"/>
         *                                           &lt;/restriction>
         *                                         &lt;/simpleType>
         *                                       &lt;/element>
         *                                       &lt;element name="PO409" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                                     &lt;/sequence>
         *                                     &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                           &lt;/sequence>
         *                           &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="CTTLoop1">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="CTT">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="CTT01" type="{http://www.w3.org/2001/XMLSchema}byte"/>
         *                                     &lt;/sequence>
         *                                     &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                             &lt;element name="AMT">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="AMT01" type="{http://www.w3.org/2001/XMLSchema}byte"/>
         *                                       &lt;element name="AMT02" type="{http://www.w3.org/2001/XMLSchema}float"/>
         *                                     &lt;/sequence>
         *                                     &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                           &lt;/sequence>
         *                           &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *                 &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "tx00401850"
        })
        public static class TransactionSet {

            @Override
			public String toString() {
				return "TransactionSet [tx00401850=" + tx00401850 + "]";
			}


			@XmlElement(name = "TX-00401-850", required = true)
            protected Interchange.FunctionalGroup.TransactionSet.TX00401850 tx00401850;

            /**
             * Gets the value of the tx00401850 property.
             * 
             * @return
             *     possible object is
             *     {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 }
             *     
             */
            public Interchange.FunctionalGroup.TransactionSet.TX00401850 getTX00401850() {
                return tx00401850;
            }

            /**
             * Sets the value of the tx00401850 property.
             * 
             * @param value
             *     allowed object is
             *     {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 }
             *     
             */
            public void setTX00401850(Interchange.FunctionalGroup.TransactionSet.TX00401850 value) {
                this.tx00401850 = value;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="Meta">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="ST01" type="{http://www.w3.org/2001/XMLSchema}short"/>
             *                   &lt;element name="ST02" type="{http://www.w3.org/2001/XMLSchema}byte"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="BEG">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="BEG01" type="{http://www.w3.org/2001/XMLSchema}byte"/>
             *                   &lt;element name="BEG02" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="BEG03" type="{http://www.w3.org/2001/XMLSchema}long"/>
             *                   &lt;element name="BEG04" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="BEG05" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *                   &lt;element name="BEG06" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *                 &lt;/sequence>
             *                 &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="REF" maxOccurs="unbounded" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="REF01">
             *                     &lt;simpleType>
             *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                         &lt;enumeration value="DP"/>
             *                         &lt;enumeration value="PS"/>
             *                       &lt;/restriction>
             *                     &lt;/simpleType>
             *                   &lt;/element>
             *                   &lt;element name="REF02">
             *                     &lt;simpleType>
             *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                         &lt;enumeration value="038"/>
             *                         &lt;enumeration value="R"/>
             *                       &lt;/restriction>
             *                     &lt;/simpleType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *                 &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="ITD">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="ITD01" type="{http://www.w3.org/2001/XMLSchema}byte"/>
             *                   &lt;element name="ITD02" type="{http://www.w3.org/2001/XMLSchema}byte"/>
             *                   &lt;element name="ITD03" type="{http://www.w3.org/2001/XMLSchema}byte"/>
             *                   &lt;element name="ITD04" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="ITD05" type="{http://www.w3.org/2001/XMLSchema}byte"/>
             *                   &lt;element name="ITD06" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="ITD07" type="{http://www.w3.org/2001/XMLSchema}byte"/>
             *                 &lt;/sequence>
             *                 &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="DTM">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="DTM01" type="{http://www.w3.org/2001/XMLSchema}byte"/>
             *                   &lt;element name="DTM02" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *                 &lt;/sequence>
             *                 &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="PKG" maxOccurs="unbounded" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="PKG01" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="PKG02">
             *                     &lt;simpleType>
             *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
             *                         &lt;enumeration value="68"/>
             *                         &lt;enumeration value="66"/>
             *                       &lt;/restriction>
             *                     &lt;/simpleType>
             *                   &lt;/element>
             *                   &lt;element name="PKG03" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="PKG04" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="PKG05">
             *                     &lt;simpleType>
             *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                         &lt;enumeration value="PALLETIZE SHIPMENT"/>
             *                         &lt;enumeration value="REGULAR"/>
             *                       &lt;/restriction>
             *                     &lt;/simpleType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *                 &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="TD5">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="TD501" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="TD502" type="{http://www.w3.org/2001/XMLSchema}byte"/>
             *                   &lt;element name="TD503" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="TD504" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="TD505" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                 &lt;/sequence>
             *                 &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="N1Loop1">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="N1">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="N101" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="N102" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="N103" type="{http://www.w3.org/2001/XMLSchema}byte"/>
             *                             &lt;element name="N104" type="{http://www.w3.org/2001/XMLSchema}long"/>
             *                           &lt;/sequence>
             *                           &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                   &lt;element name="N3">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="N301" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                           &lt;/sequence>
             *                           &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                   &lt;element name="N4">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="N401" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="N402" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="N403" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *                           &lt;/sequence>
             *                           &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *                 &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="PO1Loop1" maxOccurs="unbounded" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="PO1">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="PO101">
             *                               &lt;simpleType>
             *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
             *                                   &lt;enumeration value="1"/>
             *                                   &lt;enumeration value="2"/>
             *                                   &lt;enumeration value="3"/>
             *                                   &lt;enumeration value="4"/>
             *                                   &lt;enumeration value="5"/>
             *                                   &lt;enumeration value="6"/>
             *                                 &lt;/restriction>
             *                               &lt;/simpleType>
             *                             &lt;/element>
             *                             &lt;element name="PO102">
             *                               &lt;simpleType>
             *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}short">
             *                                   &lt;enumeration value="120"/>
             *                                   &lt;enumeration value="220"/>
             *                                   &lt;enumeration value="126"/>
             *                                   &lt;enumeration value="76"/>
             *                                   &lt;enumeration value="72"/>
             *                                   &lt;enumeration value="696"/>
             *                                 &lt;/restriction>
             *                               &lt;/simpleType>
             *                             &lt;/element>
             *                             &lt;element name="PO103" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="PO104">
             *                               &lt;simpleType>
             *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}float">
             *                                   &lt;enumeration value="9.25"/>
             *                                   &lt;enumeration value="13.79"/>
             *                                   &lt;enumeration value="10.99"/>
             *                                   &lt;enumeration value="4.35"/>
             *                                   &lt;enumeration value="7.5"/>
             *                                   &lt;enumeration value="9.55"/>
             *                                 &lt;/restriction>
             *                               &lt;/simpleType>
             *                             &lt;/element>
             *                             &lt;element name="PO105" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="PO106" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="PO107">
             *                               &lt;simpleType>
             *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                                   &lt;enumeration value="065322-117"/>
             *                                   &lt;enumeration value="066850-116"/>
             *                                   &lt;enumeration value="060733-110"/>
             *                                   &lt;enumeration value="065308-116"/>
             *                                   &lt;enumeration value="065374-118"/>
             *                                   &lt;enumeration value="067504-118"/>
             *                                 &lt;/restriction>
             *                               &lt;/simpleType>
             *                             &lt;/element>
             *                             &lt;element name="PO108" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="PO109" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="PO110" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="PO111">
             *                               &lt;simpleType>
             *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                                   &lt;enumeration value="AB3542"/>
             *                                   &lt;enumeration value="RD5322"/>
             *                                   &lt;enumeration value="XY5266"/>
             *                                   &lt;enumeration value="VX2332"/>
             *                                   &lt;enumeration value="RV0524"/>
             *                                   &lt;enumeration value="DX1875"/>
             *                                 &lt;/restriction>
             *                               &lt;/simpleType>
             *                             &lt;/element>
             *                           &lt;/sequence>
             *                           &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                   &lt;element name="PIDLoop1">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="PID">
             *                               &lt;complexType>
             *                                 &lt;complexContent>
             *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                     &lt;sequence>
             *                                       &lt;element name="PID01" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                                       &lt;element name="PID02" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                                       &lt;element name="PID03" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                                       &lt;element name="PID04" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                                       &lt;element name="PID05">
             *                                         &lt;simpleType>
             *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *                                             &lt;enumeration value="SMALL WIDGET"/>
             *                                             &lt;enumeration value="MEDIUM WIDGET"/>
             *                                             &lt;enumeration value="LARGE WIDGET"/>
             *                                             &lt;enumeration value="NANO WIDGET"/>
             *                                             &lt;enumeration value="BLUE WIDGET"/>
             *                                             &lt;enumeration value="ORANGE WIDGET"/>
             *                                           &lt;/restriction>
             *                                         &lt;/simpleType>
             *                                       &lt;/element>
             *                                     &lt;/sequence>
             *                                     &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                   &lt;/restriction>
             *                                 &lt;/complexContent>
             *                               &lt;/complexType>
             *                             &lt;/element>
             *                           &lt;/sequence>
             *                           &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                   &lt;element name="PO4">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="PO401">
             *                               &lt;simpleType>
             *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
             *                                   &lt;enumeration value="4"/>
             *                                   &lt;enumeration value="2"/>
             *                                   &lt;enumeration value="6"/>
             *                                 &lt;/restriction>
             *                               &lt;/simpleType>
             *                             &lt;/element>
             *                             &lt;element name="PO402">
             *                               &lt;simpleType>
             *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
             *                                   &lt;enumeration value="4"/>
             *                                   &lt;enumeration value="2"/>
             *                                   &lt;enumeration value="1"/>
             *                                   &lt;enumeration value="6"/>
             *                                 &lt;/restriction>
             *                               &lt;/simpleType>
             *                             &lt;/element>
             *                             &lt;element name="PO403" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="PO404" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *                             &lt;element name="PO405" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *                             &lt;element name="PO406" minOccurs="0">
             *                               &lt;simpleType>
             *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
             *                                   &lt;enumeration value="3"/>
             *                                   &lt;enumeration value="6"/>
             *                                 &lt;/restriction>
             *                               &lt;/simpleType>
             *                             &lt;/element>
             *                             &lt;element name="PO407" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *                             &lt;element name="PO408" minOccurs="0">
             *                               &lt;simpleType>
             *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
             *                                   &lt;enumeration value="15"/>
             *                                   &lt;enumeration value="12"/>
             *                                   &lt;enumeration value="19"/>
             *                                   &lt;enumeration value="10"/>
             *                                 &lt;/restriction>
             *                               &lt;/simpleType>
             *                             &lt;/element>
             *                             &lt;element name="PO409" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *                           &lt;/sequence>
             *                           &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *                 &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="CTTLoop1">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="CTT">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="CTT01" type="{http://www.w3.org/2001/XMLSchema}byte"/>
             *                           &lt;/sequence>
             *                           &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                   &lt;element name="AMT">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="AMT01" type="{http://www.w3.org/2001/XMLSchema}byte"/>
             *                             &lt;element name="AMT02" type="{http://www.w3.org/2001/XMLSchema}float"/>
             *                           &lt;/sequence>
             *                           &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *                 &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "meta",
                "beg",
                "ref",
                "itd",
                "dtm",
                "pkg",
                "td5",
                "n1Loop1",
                "po1Loop1",
                "cttLoop1"
            })
            public static class TX00401850 {

                @Override
				public String toString() {
					return "TX00401850 [meta=" + meta + ", beg=" + beg + ", ref=" + ref + ", itd=" + itd + ", dtm="
							+ dtm + ", pkg=" + pkg + ", td5=" + td5 + ", n1Loop1=" + n1Loop1 + ", po1Loop1=" + po1Loop1
							+ ", cttLoop1=" + cttLoop1 + ", type=" + type + "]";
				}


				@XmlElement(name = "Meta", required = true)
                protected Interchange.FunctionalGroup.TransactionSet.TX00401850 .Meta meta;
                @XmlElement(name = "BEG", required = true)
                protected Interchange.FunctionalGroup.TransactionSet.TX00401850 .BEG beg;
                @XmlElement(name = "REF")
                protected List<Interchange.FunctionalGroup.TransactionSet.TX00401850 .REF> ref;
                @XmlElement(name = "ITD", required = true)
                protected Interchange.FunctionalGroup.TransactionSet.TX00401850 .ITD itd;
                @XmlElement(name = "DTM", required = true)
                protected Interchange.FunctionalGroup.TransactionSet.TX00401850 .DTM dtm;
                @XmlElement(name = "PKG")
                protected List<Interchange.FunctionalGroup.TransactionSet.TX00401850 .PKG> pkg;
                @XmlElement(name = "TD5", required = true)
                protected Interchange.FunctionalGroup.TransactionSet.TX00401850 .TD5 td5;
                @XmlElement(name = "N1Loop1", required = true)
                protected Interchange.FunctionalGroup.TransactionSet.TX00401850 .N1Loop1 n1Loop1;
                @XmlElement(name = "PO1Loop1")
                protected List<Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1> po1Loop1;
                @XmlElement(name = "CTTLoop1", required = true)
                protected Interchange.FunctionalGroup.TransactionSet.TX00401850 .CTTLoop1 cttLoop1;
                @XmlAttribute(name = "type")
                protected String type;

                /**
                 * Gets the value of the meta property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .Meta }
                 *     
                 */
                public Interchange.FunctionalGroup.TransactionSet.TX00401850 .Meta getMeta() {
                    return meta;
                }

                /**
                 * Sets the value of the meta property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .Meta }
                 *     
                 */
                public void setMeta(Interchange.FunctionalGroup.TransactionSet.TX00401850 .Meta value) {
                    this.meta = value;
                }

                /**
                 * Gets the value of the beg property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .BEG }
                 *     
                 */
                public Interchange.FunctionalGroup.TransactionSet.TX00401850 .BEG getBEG() {
                    return beg;
                }

                /**
                 * Sets the value of the beg property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .BEG }
                 *     
                 */
                public void setBEG(Interchange.FunctionalGroup.TransactionSet.TX00401850 .BEG value) {
                    this.beg = value;
                }

                /**
                 * Gets the value of the ref property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the ref property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getREF().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .REF }
                 * 
                 * 
                 */
                public List<Interchange.FunctionalGroup.TransactionSet.TX00401850 .REF> getREF() {
                    if (ref == null) {
                        ref = new ArrayList<Interchange.FunctionalGroup.TransactionSet.TX00401850 .REF>();
                    }
                    return this.ref;
                }

                /**
                 * Gets the value of the itd property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .ITD }
                 *     
                 */
                public Interchange.FunctionalGroup.TransactionSet.TX00401850 .ITD getITD() {
                    return itd;
                }

                /**
                 * Sets the value of the itd property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .ITD }
                 *     
                 */
                public void setITD(Interchange.FunctionalGroup.TransactionSet.TX00401850 .ITD value) {
                    this.itd = value;
                }

                /**
                 * Gets the value of the dtm property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .DTM }
                 *     
                 */
                public Interchange.FunctionalGroup.TransactionSet.TX00401850 .DTM getDTM() {
                    return dtm;
                }

                /**
                 * Sets the value of the dtm property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .DTM }
                 *     
                 */
                public void setDTM(Interchange.FunctionalGroup.TransactionSet.TX00401850 .DTM value) {
                    this.dtm = value;
                }

                /**
                 * Gets the value of the pkg property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the pkg property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getPKG().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .PKG }
                 * 
                 * 
                 */
                public List<Interchange.FunctionalGroup.TransactionSet.TX00401850 .PKG> getPKG() {
                    if (pkg == null) {
                        pkg = new ArrayList<Interchange.FunctionalGroup.TransactionSet.TX00401850 .PKG>();
                    }
                    return this.pkg;
                }

                /**
                 * Gets the value of the td5 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .TD5 }
                 *     
                 */
                public Interchange.FunctionalGroup.TransactionSet.TX00401850 .TD5 getTD5() {
                    return td5;
                }

                /**
                 * Sets the value of the td5 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .TD5 }
                 *     
                 */
                public void setTD5(Interchange.FunctionalGroup.TransactionSet.TX00401850 .TD5 value) {
                    this.td5 = value;
                }

                /**
                 * Gets the value of the n1Loop1 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .N1Loop1 }
                 *     
                 */
                public Interchange.FunctionalGroup.TransactionSet.TX00401850 .N1Loop1 getN1Loop1() {
                    return n1Loop1;
                }

                /**
                 * Sets the value of the n1Loop1 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .N1Loop1 }
                 *     
                 */
                public void setN1Loop1(Interchange.FunctionalGroup.TransactionSet.TX00401850 .N1Loop1 value) {
                    this.n1Loop1 = value;
                }

                /**
                 * Gets the value of the po1Loop1 property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the po1Loop1 property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getPO1Loop1().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1 }
                 * 
                 * 
                 */
                public List<Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1> getPO1Loop1() {
                    if (po1Loop1 == null) {
                        po1Loop1 = new ArrayList<Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1>();
                    }
                    return this.po1Loop1;
                }

                /**
                 * Gets the value of the cttLoop1 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .CTTLoop1 }
                 *     
                 */
                public Interchange.FunctionalGroup.TransactionSet.TX00401850 .CTTLoop1 getCTTLoop1() {
                    return cttLoop1;
                }

                /**
                 * Sets the value of the cttLoop1 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .CTTLoop1 }
                 *     
                 */
                public void setCTTLoop1(Interchange.FunctionalGroup.TransactionSet.TX00401850 .CTTLoop1 value) {
                    this.cttLoop1 = value;
                }

                /**
                 * Gets the value of the type property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getType() {
                    return type;
                }

                /**
                 * Sets the value of the type property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setType(String value) {
                    this.type = value;
                }

                public void setREF(List<Interchange.FunctionalGroup.TransactionSet.TX00401850 .REF> ref) {
                    this.ref = ref;
                }

                public void setPO1Loop1(List<Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1> po1Loop1) {
                    this.po1Loop1 = po1Loop1;
                }

                public void setPKG(List<Interchange.FunctionalGroup.TransactionSet.TX00401850 .PKG> pkg) {
                    this.pkg = pkg;
                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="BEG01" type="{http://www.w3.org/2001/XMLSchema}byte"/>
                 *         &lt;element name="BEG02" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="BEG03" type="{http://www.w3.org/2001/XMLSchema}long"/>
                 *         &lt;element name="BEG04" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="BEG05" type="{http://www.w3.org/2001/XMLSchema}int"/>
                 *         &lt;element name="BEG06" type="{http://www.w3.org/2001/XMLSchema}int"/>
                 *       &lt;/sequence>
                 *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "beg01",
                    "beg02",
                    "beg03",
                    "beg04",
                    "beg05",
                    "beg06"
                })
                public static class BEG {

                    @Override
					public String toString() {
						return "BEG [beg01=" + beg01 + ", beg02=" + beg02 + ", beg03=" + beg03 + ", beg04=" + beg04
								+ ", beg05=" + beg05 + ", beg06=" + beg06 + ", type=" + type + "]";
					}

					@XmlElement(name = "BEG01")
                    protected byte beg01;
                    @XmlElement(name = "BEG02", required = true)
                    protected String beg02;
                    @XmlElement(name = "BEG03")
                    protected long beg03;
                    @XmlElement(name = "BEG04", required = true, nillable = true)
                    protected String beg04;
                    @XmlElement(name = "BEG05")
                    protected int beg05;
                    @XmlElement(name = "BEG06")
                    protected int beg06;
                    @XmlAttribute(name = "type")
                    protected String type;

                    /**
                     * Gets the value of the beg01 property.
                     * 
                     */
                    public byte getBEG01() {
                        return beg01;
                    }

                    /**
                     * Sets the value of the beg01 property.
                     * 
                     */
                    public void setBEG01(byte value) {
                        this.beg01 = value;
                    }

                    /**
                     * Gets the value of the beg02 property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getBEG02() {
                        return beg02;
                    }

                    /**
                     * Sets the value of the beg02 property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setBEG02(String value) {
                        this.beg02 = value;
                    }

                    /**
                     * Gets the value of the beg03 property.
                     * 
                     */
                    public long getBEG03() {
                        return beg03;
                    }

                    /**
                     * Sets the value of the beg03 property.
                     * 
                     */
                    public void setBEG03(long value) {
                        this.beg03 = value;
                    }

                    /**
                     * Gets the value of the beg04 property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getBEG04() {
                        return beg04;
                    }

                    /**
                     * Sets the value of the beg04 property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setBEG04(String value) {
                        this.beg04 = value;
                    }

                    /**
                     * Gets the value of the beg05 property.
                     * 
                     */
                    public int getBEG05() {
                        return beg05;
                    }

                    /**
                     * Sets the value of the beg05 property.
                     * 
                     */
                    public void setBEG05(int value) {
                        this.beg05 = value;
                    }

                    /**
                     * Gets the value of the beg06 property.
                     * 
                     */
                    public int getBEG06() {
                        return beg06;
                    }

                    /**
                     * Sets the value of the beg06 property.
                     * 
                     */
                    public void setBEG06(int value) {
                        this.beg06 = value;
                    }

                    /**
                     * Gets the value of the type property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getType() {
                        return type;
                    }

                    /**
                     * Sets the value of the type property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setType(String value) {
                        this.type = value;
                    }

                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="CTT">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="CTT01" type="{http://www.w3.org/2001/XMLSchema}byte"/>
                 *                 &lt;/sequence>
                 *                 &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *         &lt;element name="AMT">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="AMT01" type="{http://www.w3.org/2001/XMLSchema}byte"/>
                 *                   &lt;element name="AMT02" type="{http://www.w3.org/2001/XMLSchema}float"/>
                 *                 &lt;/sequence>
                 *                 &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "ctt",
                    "amt"
                })
                public static class CTTLoop1 {

                    @Override
					public String toString() {
						return "CTTLoop1 [ctt=" + ctt + ", amt=" + amt + ", type=" + type + "]";
					}


					@XmlElement(name = "CTT", required = true)
                    protected Interchange.FunctionalGroup.TransactionSet.TX00401850 .CTTLoop1 .CTT ctt;
                    @XmlElement(name = "AMT", required = true)
                    protected Interchange.FunctionalGroup.TransactionSet.TX00401850 .CTTLoop1 .AMT amt;
                    @XmlAttribute(name = "type")
                    protected String type;

                    /**
                     * Gets the value of the ctt property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .CTTLoop1 .CTT }
                     *     
                     */
                    public Interchange.FunctionalGroup.TransactionSet.TX00401850 .CTTLoop1 .CTT getCTT() {
                        return ctt;
                    }

                    /**
                     * Sets the value of the ctt property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .CTTLoop1 .CTT }
                     *     
                     */
                    public void setCTT(Interchange.FunctionalGroup.TransactionSet.TX00401850 .CTTLoop1 .CTT value) {
                        this.ctt = value;
                    }

                    /**
                     * Gets the value of the amt property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .CTTLoop1 .AMT }
                     *     
                     */
                    public Interchange.FunctionalGroup.TransactionSet.TX00401850 .CTTLoop1 .AMT getAMT() {
                        return amt;
                    }

                    /**
                     * Sets the value of the amt property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .CTTLoop1 .AMT }
                     *     
                     */
                    public void setAMT(Interchange.FunctionalGroup.TransactionSet.TX00401850 .CTTLoop1 .AMT value) {
                        this.amt = value;
                    }

                    /**
                     * Gets the value of the type property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getType() {
                        return type;
                    }

                    /**
                     * Sets the value of the type property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setType(String value) {
                        this.type = value;
                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="AMT01" type="{http://www.w3.org/2001/XMLSchema}byte"/>
                     *         &lt;element name="AMT02" type="{http://www.w3.org/2001/XMLSchema}float"/>
                     *       &lt;/sequence>
                     *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "amt01",
                        "amt02"
                    })
                    public static class AMT {

                        @Override
						public String toString() {
							return "AMT [amt01=" + amt01 + ", amt02=" + amt02 + ", type=" + type + "]";
						}

						@XmlElement(name = "AMT01")
                        protected byte amt01;
                        @XmlElement(name = "AMT02")
                        protected float amt02;
                        @XmlAttribute(name = "type")
                        protected String type;

                        /**
                         * Gets the value of the amt01 property.
                         * 
                         */
                        public byte getAMT01() {
                            return amt01;
                        }

                        /**
                         * Sets the value of the amt01 property.
                         * 
                         */
                        public void setAMT01(byte value) {
                            this.amt01 = value;
                        }

                        /**
                         * Gets the value of the amt02 property.
                         * 
                         */
                        public float getAMT02() {
                            return amt02;
                        }

                        /**
                         * Sets the value of the amt02 property.
                         * 
                         */
                        public void setAMT02(float value) {
                            this.amt02 = value;
                        }

                        /**
                         * Gets the value of the type property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getType() {
                            return type;
                        }

                        /**
                         * Sets the value of the type property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setType(String value) {
                            this.type = value;
                        }

                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="CTT01" type="{http://www.w3.org/2001/XMLSchema}byte"/>
                     *       &lt;/sequence>
                     *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "ctt01"
                    })
                    public static class CTT {

                        @Override
						public String toString() {
							return "CTT [ctt01=" + ctt01 + ", type=" + type + "]";
						}

						@XmlElement(name = "CTT01")
                        protected byte ctt01;
                        @XmlAttribute(name = "type")
                        protected String type;

                        /**
                         * Gets the value of the ctt01 property.
                         * 
                         */
                        public byte getCTT01() {
                            return ctt01;
                        }

                        /**
                         * Sets the value of the ctt01 property.
                         * 
                         */
                        public void setCTT01(byte value) {
                            this.ctt01 = value;
                        }

                        /**
                         * Gets the value of the type property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getType() {
                            return type;
                        }

                        /**
                         * Sets the value of the type property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setType(String value) {
                            this.type = value;
                        }

                    }

                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="DTM01" type="{http://www.w3.org/2001/XMLSchema}byte"/>
                 *         &lt;element name="DTM02" type="{http://www.w3.org/2001/XMLSchema}int"/>
                 *       &lt;/sequence>
                 *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "dtm01",
                    "dtm02"
                })
                public static class DTM {

                    @Override
					public String toString() {
						return "DTM [dtm01=" + dtm01 + ", dtm02=" + dtm02 + ", type=" + type + "]";
					}

					@XmlElement(name = "DTM01")
                    protected byte dtm01;
                    @XmlElement(name = "DTM02")
                    protected int dtm02;
                    @XmlAttribute(name = "type")
                    protected String type;

                    /**
                     * Gets the value of the dtm01 property.
                     * 
                     */
                    public byte getDTM01() {
                        return dtm01;
                    }

                    /**
                     * Sets the value of the dtm01 property.
                     * 
                     */
                    public void setDTM01(byte value) {
                        this.dtm01 = value;
                    }

                    /**
                     * Gets the value of the dtm02 property.
                     * 
                     */
                    public int getDTM02() {
                        return dtm02;
                    }

                    /**
                     * Sets the value of the dtm02 property.
                     * 
                     */
                    public void setDTM02(int value) {
                        this.dtm02 = value;
                    }

                    /**
                     * Gets the value of the type property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getType() {
                        return type;
                    }

                    /**
                     * Sets the value of the type property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setType(String value) {
                        this.type = value;
                    }

                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="ITD01" type="{http://www.w3.org/2001/XMLSchema}byte"/>
                 *         &lt;element name="ITD02" type="{http://www.w3.org/2001/XMLSchema}byte"/>
                 *         &lt;element name="ITD03" type="{http://www.w3.org/2001/XMLSchema}byte"/>
                 *         &lt;element name="ITD04" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="ITD05" type="{http://www.w3.org/2001/XMLSchema}byte"/>
                 *         &lt;element name="ITD06" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="ITD07" type="{http://www.w3.org/2001/XMLSchema}byte"/>
                 *       &lt;/sequence>
                 *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "itd01",
                    "itd02",
                    "itd03",
                    "itd04",
                    "itd05",
                    "itd06",
                    "itd07"
                })
                public static class ITD {

                    @Override
					public String toString() {
						return "ITD [itd01=" + itd01 + ", itd02=" + itd02 + ", itd03=" + itd03 + ", itd04=" + itd04
								+ ", itd05=" + itd05 + ", itd06=" + itd06 + ", itd07=" + itd07 + ", type=" + type + "]";
					}

					@XmlElement(name = "ITD01")
                    protected byte itd01;
                    @XmlElement(name = "ITD02")
                    protected byte itd02;
                    @XmlElement(name = "ITD03")
                    protected byte itd03;
                    @XmlElement(name = "ITD04", required = true, nillable = true)
                    protected String itd04;
                    @XmlElement(name = "ITD05")
                    protected byte itd05;
                    @XmlElement(name = "ITD06", required = true, nillable = true)
                    protected String itd06;
                    @XmlElement(name = "ITD07")
                    protected byte itd07;
                    @XmlAttribute(name = "type")
                    protected String type;

                    /**
                     * Gets the value of the itd01 property.
                     * 
                     */
                    public byte getITD01() {
                        return itd01;
                    }

                    /**
                     * Sets the value of the itd01 property.
                     * 
                     */
                    public void setITD01(byte value) {
                        this.itd01 = value;
                    }

                    /**
                     * Gets the value of the itd02 property.
                     * 
                     */
                    public byte getITD02() {
                        return itd02;
                    }

                    /**
                     * Sets the value of the itd02 property.
                     * 
                     */
                    public void setITD02(byte value) {
                        this.itd02 = value;
                    }

                    /**
                     * Gets the value of the itd03 property.
                     * 
                     */
                    public byte getITD03() {
                        return itd03;
                    }

                    /**
                     * Sets the value of the itd03 property.
                     * 
                     */
                    public void setITD03(byte value) {
                        this.itd03 = value;
                    }

                    /**
                     * Gets the value of the itd04 property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getITD04() {
                        return itd04;
                    }

                    /**
                     * Sets the value of the itd04 property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setITD04(String value) {
                        this.itd04 = value;
                    }

                    /**
                     * Gets the value of the itd05 property.
                     * 
                     */
                    public byte getITD05() {
                        return itd05;
                    }

                    /**
                     * Sets the value of the itd05 property.
                     * 
                     */
                    public void setITD05(byte value) {
                        this.itd05 = value;
                    }

                    /**
                     * Gets the value of the itd06 property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getITD06() {
                        return itd06;
                    }

                    /**
                     * Sets the value of the itd06 property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setITD06(String value) {
                        this.itd06 = value;
                    }

                    /**
                     * Gets the value of the itd07 property.
                     * 
                     */
                    public byte getITD07() {
                        return itd07;
                    }

                    /**
                     * Sets the value of the itd07 property.
                     * 
                     */
                    public void setITD07(byte value) {
                        this.itd07 = value;
                    }

                    /**
                     * Gets the value of the type property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getType() {
                        return type;
                    }

                    /**
                     * Sets the value of the type property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setType(String value) {
                        this.type = value;
                    }

                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="ST01" type="{http://www.w3.org/2001/XMLSchema}short"/>
                 *         &lt;element name="ST02" type="{http://www.w3.org/2001/XMLSchema}byte"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "st01",
                    "st02"
                })
                public static class Meta {

                    @Override
					public String toString() {
						return "Meta [st01=" + st01 + ", st02=" + st02 + "]";
					}

					@XmlElement(name = "ST01")
                    protected short st01;
                    @XmlElement(name = "ST02")
                    protected byte st02;

                    /**
                     * Gets the value of the st01 property.
                     * 
                     */
                    public short getST01() {
                        return st01;
                    }

                    /**
                     * Sets the value of the st01 property.
                     * 
                     */
                    public void setST01(short value) {
                        this.st01 = value;
                    }

                    /**
                     * Gets the value of the st02 property.
                     * 
                     */
                    public byte getST02() {
                        return st02;
                    }

                    /**
                     * Sets the value of the st02 property.
                     * 
                     */
                    public void setST02(byte value) {
                        this.st02 = value;
                    }

                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="N1">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="N101" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="N102" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="N103" type="{http://www.w3.org/2001/XMLSchema}byte"/>
                 *                   &lt;element name="N104" type="{http://www.w3.org/2001/XMLSchema}long"/>
                 *                 &lt;/sequence>
                 *                 &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *         &lt;element name="N3">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="N301" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                 &lt;/sequence>
                 *                 &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *         &lt;element name="N4">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="N401" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="N402" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="N403" type="{http://www.w3.org/2001/XMLSchema}int"/>
                 *                 &lt;/sequence>
                 *                 &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "n1",
                    "n3",
                    "n4"
                })
                public static class N1Loop1 {

                    @Override
					public String toString() {
						return "N1Loop1 [n1=" + n1 + ", n3=" + n3 + ", n4=" + n4 + ", type=" + type + "]";
					}


					@XmlElement(name = "N1", required = true)
                    protected Interchange.FunctionalGroup.TransactionSet.TX00401850 .N1Loop1 .N1 n1;
                    @XmlElement(name = "N3", required = true)
                    protected Interchange.FunctionalGroup.TransactionSet.TX00401850 .N1Loop1 .N3 n3;
                    @XmlElement(name = "N4", required = true)
                    protected Interchange.FunctionalGroup.TransactionSet.TX00401850 .N1Loop1 .N4 n4;
                    @XmlAttribute(name = "type")
                    protected String type;

                    /**
                     * Gets the value of the n1 property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .N1Loop1 .N1 }
                     *     
                     */
                    public Interchange.FunctionalGroup.TransactionSet.TX00401850 .N1Loop1 .N1 getN1() {
                        return n1;
                    }

                    /**
                     * Sets the value of the n1 property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .N1Loop1 .N1 }
                     *     
                     */
                    public void setN1(Interchange.FunctionalGroup.TransactionSet.TX00401850 .N1Loop1 .N1 value) {
                        this.n1 = value;
                    }

                    /**
                     * Gets the value of the n3 property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .N1Loop1 .N3 }
                     *     
                     */
                    public Interchange.FunctionalGroup.TransactionSet.TX00401850 .N1Loop1 .N3 getN3() {
                        return n3;
                    }

                    /**
                     * Sets the value of the n3 property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .N1Loop1 .N3 }
                     *     
                     */
                    public void setN3(Interchange.FunctionalGroup.TransactionSet.TX00401850 .N1Loop1 .N3 value) {
                        this.n3 = value;
                    }

                    /**
                     * Gets the value of the n4 property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .N1Loop1 .N4 }
                     *     
                     */
                    public Interchange.FunctionalGroup.TransactionSet.TX00401850 .N1Loop1 .N4 getN4() {
                        return n4;
                    }

                    /**
                     * Sets the value of the n4 property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .N1Loop1 .N4 }
                     *     
                     */
                    public void setN4(Interchange.FunctionalGroup.TransactionSet.TX00401850 .N1Loop1 .N4 value) {
                        this.n4 = value;
                    }

                    /**
                     * Gets the value of the type property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getType() {
                        return type;
                    }

                    /**
                     * Sets the value of the type property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setType(String value) {
                        this.type = value;
                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="N101" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="N102" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="N103" type="{http://www.w3.org/2001/XMLSchema}byte"/>
                     *         &lt;element name="N104" type="{http://www.w3.org/2001/XMLSchema}long"/>
                     *       &lt;/sequence>
                     *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "n101",
                        "n102",
                        "n103",
                        "n104"
                    })
                    public static class N1 {

                        @Override
						public String toString() {
							return "N1 [n101=" + n101 + ", n102=" + n102 + ", n103=" + n103 + ", n104=" + n104
									+ ", type=" + type + "]";
						}

						@XmlElement(name = "N101", required = true)
                        protected String n101;
                        @XmlElement(name = "N102", required = true)
                        protected String n102;
                        @XmlElement(name = "N103")
                        protected byte n103;
                        @XmlElement(name = "N104")
                        protected long n104;
                        @XmlAttribute(name = "type")
                        protected String type;

                        /**
                         * Gets the value of the n101 property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getN101() {
                            return n101;
                        }

                        /**
                         * Sets the value of the n101 property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setN101(String value) {
                            this.n101 = value;
                        }

                        /**
                         * Gets the value of the n102 property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getN102() {
                            return n102;
                        }

                        /**
                         * Sets the value of the n102 property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setN102(String value) {
                            this.n102 = value;
                        }

                        /**
                         * Gets the value of the n103 property.
                         * 
                         */
                        public byte getN103() {
                            return n103;
                        }

                        /**
                         * Sets the value of the n103 property.
                         * 
                         */
                        public void setN103(byte value) {
                            this.n103 = value;
                        }

                        /**
                         * Gets the value of the n104 property.
                         * 
                         */
                        public long getN104() {
                            return n104;
                        }

                        /**
                         * Sets the value of the n104 property.
                         * 
                         */
                        public void setN104(long value) {
                            this.n104 = value;
                        }

                        /**
                         * Gets the value of the type property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getType() {
                            return type;
                        }

                        /**
                         * Sets the value of the type property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setType(String value) {
                            this.type = value;
                        }

                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="N301" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *       &lt;/sequence>
                     *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "n301"
                    })
                    public static class N3 {

                        @Override
						public String toString() {
							return "N3 [n301=" + n301 + ", type=" + type + "]";
						}

						@XmlElement(name = "N301", required = true)
                        protected String n301;
                        @XmlAttribute(name = "type")
                        protected String type;

                        /**
                         * Gets the value of the n301 property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getN301() {
                            return n301;
                        }

                        /**
                         * Sets the value of the n301 property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setN301(String value) {
                            this.n301 = value;
                        }

                        /**
                         * Gets the value of the type property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getType() {
                            return type;
                        }

                        /**
                         * Sets the value of the type property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setType(String value) {
                            this.type = value;
                        }

                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="N401" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="N402" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="N403" type="{http://www.w3.org/2001/XMLSchema}int"/>
                     *       &lt;/sequence>
                     *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "n401",
                        "n402",
                        "n403"
                    })
                    public static class N4 {

                        @Override
						public String toString() {
							return "N4 [n401=" + n401 + ", n402=" + n402 + ", n403=" + n403 + ", type=" + type + "]";
						}

						@XmlElement(name = "N401", required = true)
                        protected String n401;
                        @XmlElement(name = "N402", required = true)
                        protected String n402;
                        @XmlElement(name = "N403")
                        protected int n403;
                        @XmlAttribute(name = "type")
                        protected String type;

                        /**
                         * Gets the value of the n401 property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getN401() {
                            return n401;
                        }

                        /**
                         * Sets the value of the n401 property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setN401(String value) {
                            this.n401 = value;
                        }

                        /**
                         * Gets the value of the n402 property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getN402() {
                            return n402;
                        }

                        /**
                         * Sets the value of the n402 property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setN402(String value) {
                            this.n402 = value;
                        }

                        /**
                         * Gets the value of the n403 property.
                         * 
                         */
                        public int getN403() {
                            return n403;
                        }

                        /**
                         * Sets the value of the n403 property.
                         * 
                         */
                        public void setN403(int value) {
                            this.n403 = value;
                        }

                        /**
                         * Gets the value of the type property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getType() {
                            return type;
                        }

                        /**
                         * Sets the value of the type property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setType(String value) {
                            this.type = value;
                        }

                    }

                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="PKG01" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="PKG02">
                 *           &lt;simpleType>
                 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
                 *               &lt;enumeration value="68"/>
                 *               &lt;enumeration value="66"/>
                 *             &lt;/restriction>
                 *           &lt;/simpleType>
                 *         &lt;/element>
                 *         &lt;element name="PKG03" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="PKG04" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="PKG05">
                 *           &lt;simpleType>
                 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *               &lt;enumeration value="PALLETIZE SHIPMENT"/>
                 *               &lt;enumeration value="REGULAR"/>
                 *             &lt;/restriction>
                 *           &lt;/simpleType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "pkg01",
                    "pkg02",
                    "pkg03",
                    "pkg04",
                    "pkg05"
                })
                public static class PKG {

                    @Override
					public String toString() {
						return "PKG [pkg01=" + pkg01 + ", pkg02=" + pkg02 + ", pkg03=" + pkg03 + ", pkg04=" + pkg04
								+ ", pkg05=" + pkg05 + ", type=" + type + "]";
					}

					@XmlElement(name = "PKG01", required = true)
                    protected String pkg01;
                    @XmlElement(name = "PKG02")
                    protected byte pkg02;
                    @XmlElement(name = "PKG03", required = true)
                    protected String pkg03;
                    @XmlElement(name = "PKG04", required = true, nillable = true)
                    protected String pkg04;
                    @XmlElement(name = "PKG05", required = true)
                    protected String pkg05;
                    @XmlAttribute(name = "type")
                    protected String type;

                    /**
                     * Gets the value of the pkg01 property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getPKG01() {
                        return pkg01;
                    }

                    /**
                     * Sets the value of the pkg01 property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setPKG01(String value) {
                        this.pkg01 = value;
                    }

                    /**
                     * Gets the value of the pkg02 property.
                     * 
                     */
                    public byte getPKG02() {
                        return pkg02;
                    }

                    /**
                     * Sets the value of the pkg02 property.
                     * 
                     */
                    public void setPKG02(byte value) {
                        this.pkg02 = value;
                    }

                    /**
                     * Gets the value of the pkg03 property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getPKG03() {
                        return pkg03;
                    }

                    /**
                     * Sets the value of the pkg03 property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setPKG03(String value) {
                        this.pkg03 = value;
                    }

                    /**
                     * Gets the value of the pkg04 property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getPKG04() {
                        return pkg04;
                    }

                    /**
                     * Sets the value of the pkg04 property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setPKG04(String value) {
                        this.pkg04 = value;
                    }

                    /**
                     * Gets the value of the pkg05 property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getPKG05() {
                        return pkg05;
                    }

                    /**
                     * Sets the value of the pkg05 property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setPKG05(String value) {
                        this.pkg05 = value;
                    }

                    /**
                     * Gets the value of the type property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getType() {
                        return type;
                    }

                    /**
                     * Sets the value of the type property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setType(String value) {
                        this.type = value;
                    }

                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="PO1">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="PO101">
                 *                     &lt;simpleType>
                 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
                 *                         &lt;enumeration value="1"/>
                 *                         &lt;enumeration value="2"/>
                 *                         &lt;enumeration value="3"/>
                 *                         &lt;enumeration value="4"/>
                 *                         &lt;enumeration value="5"/>
                 *                         &lt;enumeration value="6"/>
                 *                       &lt;/restriction>
                 *                     &lt;/simpleType>
                 *                   &lt;/element>
                 *                   &lt;element name="PO102">
                 *                     &lt;simpleType>
                 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}short">
                 *                         &lt;enumeration value="120"/>
                 *                         &lt;enumeration value="220"/>
                 *                         &lt;enumeration value="126"/>
                 *                         &lt;enumeration value="76"/>
                 *                         &lt;enumeration value="72"/>
                 *                         &lt;enumeration value="696"/>
                 *                       &lt;/restriction>
                 *                     &lt;/simpleType>
                 *                   &lt;/element>
                 *                   &lt;element name="PO103" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="PO104">
                 *                     &lt;simpleType>
                 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}float">
                 *                         &lt;enumeration value="9.25"/>
                 *                         &lt;enumeration value="13.79"/>
                 *                         &lt;enumeration value="10.99"/>
                 *                         &lt;enumeration value="4.35"/>
                 *                         &lt;enumeration value="7.5"/>
                 *                         &lt;enumeration value="9.55"/>
                 *                       &lt;/restriction>
                 *                     &lt;/simpleType>
                 *                   &lt;/element>
                 *                   &lt;element name="PO105" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="PO106" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="PO107">
                 *                     &lt;simpleType>
                 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *                         &lt;enumeration value="065322-117"/>
                 *                         &lt;enumeration value="066850-116"/>
                 *                         &lt;enumeration value="060733-110"/>
                 *                         &lt;enumeration value="065308-116"/>
                 *                         &lt;enumeration value="065374-118"/>
                 *                         &lt;enumeration value="067504-118"/>
                 *                       &lt;/restriction>
                 *                     &lt;/simpleType>
                 *                   &lt;/element>
                 *                   &lt;element name="PO108" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="PO109" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="PO110" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="PO111">
                 *                     &lt;simpleType>
                 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *                         &lt;enumeration value="AB3542"/>
                 *                         &lt;enumeration value="RD5322"/>
                 *                         &lt;enumeration value="XY5266"/>
                 *                         &lt;enumeration value="VX2332"/>
                 *                         &lt;enumeration value="RV0524"/>
                 *                         &lt;enumeration value="DX1875"/>
                 *                       &lt;/restriction>
                 *                     &lt;/simpleType>
                 *                   &lt;/element>
                 *                 &lt;/sequence>
                 *                 &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *         &lt;element name="PIDLoop1">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="PID">
                 *                     &lt;complexType>
                 *                       &lt;complexContent>
                 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                           &lt;sequence>
                 *                             &lt;element name="PID01" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                             &lt;element name="PID02" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                             &lt;element name="PID03" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                             &lt;element name="PID04" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                             &lt;element name="PID05">
                 *                               &lt;simpleType>
                 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *                                   &lt;enumeration value="SMALL WIDGET"/>
                 *                                   &lt;enumeration value="MEDIUM WIDGET"/>
                 *                                   &lt;enumeration value="LARGE WIDGET"/>
                 *                                   &lt;enumeration value="NANO WIDGET"/>
                 *                                   &lt;enumeration value="BLUE WIDGET"/>
                 *                                   &lt;enumeration value="ORANGE WIDGET"/>
                 *                                 &lt;/restriction>
                 *                               &lt;/simpleType>
                 *                             &lt;/element>
                 *                           &lt;/sequence>
                 *                           &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                         &lt;/restriction>
                 *                       &lt;/complexContent>
                 *                     &lt;/complexType>
                 *                   &lt;/element>
                 *                 &lt;/sequence>
                 *                 &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *         &lt;element name="PO4">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="PO401">
                 *                     &lt;simpleType>
                 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
                 *                         &lt;enumeration value="4"/>
                 *                         &lt;enumeration value="2"/>
                 *                         &lt;enumeration value="6"/>
                 *                       &lt;/restriction>
                 *                     &lt;/simpleType>
                 *                   &lt;/element>
                 *                   &lt;element name="PO402">
                 *                     &lt;simpleType>
                 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
                 *                         &lt;enumeration value="4"/>
                 *                         &lt;enumeration value="2"/>
                 *                         &lt;enumeration value="1"/>
                 *                         &lt;enumeration value="6"/>
                 *                       &lt;/restriction>
                 *                     &lt;/simpleType>
                 *                   &lt;/element>
                 *                   &lt;element name="PO403" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="PO404" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                 *                   &lt;element name="PO405" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                 *                   &lt;element name="PO406" minOccurs="0">
                 *                     &lt;simpleType>
                 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
                 *                         &lt;enumeration value="3"/>
                 *                         &lt;enumeration value="6"/>
                 *                       &lt;/restriction>
                 *                     &lt;/simpleType>
                 *                   &lt;/element>
                 *                   &lt;element name="PO407" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                 *                   &lt;element name="PO408" minOccurs="0">
                 *                     &lt;simpleType>
                 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
                 *                         &lt;enumeration value="15"/>
                 *                         &lt;enumeration value="12"/>
                 *                         &lt;enumeration value="19"/>
                 *                         &lt;enumeration value="10"/>
                 *                       &lt;/restriction>
                 *                     &lt;/simpleType>
                 *                   &lt;/element>
                 *                   &lt;element name="PO409" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                 *                 &lt;/sequence>
                 *                 &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "po1",
                    "pidLoop1",
                    "po4"
                })
                public static class PO1Loop1 {

                    @Override
					public String toString() {
						return "PO1Loop1 [po1=" + po1 + ", pidLoop1=" + pidLoop1 + ", po4=" + po4 + ", type=" + type
								+ "]";
					}


					@XmlElement(name = "PO1", required = true)
                    protected Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1 .PO1 po1;
                    @XmlElement(name = "PIDLoop1", required = true)
                    protected Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1 .PIDLoop1 pidLoop1;
                    @XmlElement(name = "PO4", required = true)
                    protected Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1 .PO4 po4;
                    @XmlAttribute(name = "type")
                    protected String type;

                    /**
                     * Gets the value of the po1 property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1 .PO1 }
                     *     
                     */
                    public Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1 .PO1 getPO1() {
                        return po1;
                    }

                    /**
                     * Sets the value of the po1 property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1 .PO1 }
                     *     
                     */
                    public void setPO1(Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1 .PO1 value) {
                        this.po1 = value;
                    }

                    /**
                     * Gets the value of the pidLoop1 property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1 .PIDLoop1 }
                     *     
                     */
                    public Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1 .PIDLoop1 getPIDLoop1() {
                        return pidLoop1;
                    }

                    /**
                     * Sets the value of the pidLoop1 property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1 .PIDLoop1 }
                     *     
                     */
                    public void setPIDLoop1(Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1 .PIDLoop1 value) {
                        this.pidLoop1 = value;
                    }

                    /**
                     * Gets the value of the po4 property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1 .PO4 }
                     *     
                     */
                    public Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1 .PO4 getPO4() {
                        return po4;
                    }

                    /**
                     * Sets the value of the po4 property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1 .PO4 }
                     *     
                     */
                    public void setPO4(Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1 .PO4 value) {
                        this.po4 = value;
                    }

                    /**
                     * Gets the value of the type property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getType() {
                        return type;
                    }

                    /**
                     * Sets the value of the type property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setType(String value) {
                        this.type = value;
                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="PID">
                     *           &lt;complexType>
                     *             &lt;complexContent>
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                 &lt;sequence>
                     *                   &lt;element name="PID01" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *                   &lt;element name="PID02" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *                   &lt;element name="PID03" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *                   &lt;element name="PID04" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *                   &lt;element name="PID05">
                     *                     &lt;simpleType>
                     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                     *                         &lt;enumeration value="SMALL WIDGET"/>
                     *                         &lt;enumeration value="MEDIUM WIDGET"/>
                     *                         &lt;enumeration value="LARGE WIDGET"/>
                     *                         &lt;enumeration value="NANO WIDGET"/>
                     *                         &lt;enumeration value="BLUE WIDGET"/>
                     *                         &lt;enumeration value="ORANGE WIDGET"/>
                     *                       &lt;/restriction>
                     *                     &lt;/simpleType>
                     *                   &lt;/element>
                     *                 &lt;/sequence>
                     *                 &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *               &lt;/restriction>
                     *             &lt;/complexContent>
                     *           &lt;/complexType>
                     *         &lt;/element>
                     *       &lt;/sequence>
                     *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "pid"
                    })
                    public static class PIDLoop1 {

                        @Override
						public String toString() {
							return "PIDLoop1 [pid=" + pid + ", type=" + type + "]";
						}


						@XmlElement(name = "PID", required = true)
                        protected Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1 .PIDLoop1 .PID pid;
                        @XmlAttribute(name = "type")
                        protected String type;

                        /**
                         * Gets the value of the pid property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1 .PIDLoop1 .PID }
                         *     
                         */
                        public Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1 .PIDLoop1 .PID getPID() {
                            return pid;
                        }

                        /**
                         * Sets the value of the pid property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1 .PIDLoop1 .PID }
                         *     
                         */
                        public void setPID(Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1 .PIDLoop1 .PID value) {
                            this.pid = value;
                        }

                        /**
                         * Gets the value of the type property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getType() {
                            return type;
                        }

                        /**
                         * Sets the value of the type property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setType(String value) {
                            this.type = value;
                        }


                        /**
                         * <p>Java class for anonymous complex type.
                         * 
                         * <p>The following schema fragment specifies the expected content contained within this class.
                         * 
                         * <pre>
                         * &lt;complexType>
                         *   &lt;complexContent>
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *       &lt;sequence>
                         *         &lt;element name="PID01" type="{http://www.w3.org/2001/XMLSchema}string"/>
                         *         &lt;element name="PID02" type="{http://www.w3.org/2001/XMLSchema}string"/>
                         *         &lt;element name="PID03" type="{http://www.w3.org/2001/XMLSchema}string"/>
                         *         &lt;element name="PID04" type="{http://www.w3.org/2001/XMLSchema}string"/>
                         *         &lt;element name="PID05">
                         *           &lt;simpleType>
                         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                         *               &lt;enumeration value="SMALL WIDGET"/>
                         *               &lt;enumeration value="MEDIUM WIDGET"/>
                         *               &lt;enumeration value="LARGE WIDGET"/>
                         *               &lt;enumeration value="NANO WIDGET"/>
                         *               &lt;enumeration value="BLUE WIDGET"/>
                         *               &lt;enumeration value="ORANGE WIDGET"/>
                         *             &lt;/restriction>
                         *           &lt;/simpleType>
                         *         &lt;/element>
                         *       &lt;/sequence>
                         *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *     &lt;/restriction>
                         *   &lt;/complexContent>
                         * &lt;/complexType>
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "pid01",
                            "pid02",
                            "pid03",
                            "pid04",
                            "pid05"
                        })
                        public static class PID {

                            @Override
							public String toString() {
								return "PID [pid01=" + pid01 + ", pid02=" + pid02 + ", pid03=" + pid03 + ", pid04="
										+ pid04 + ", pid05=" + pid05 + ", type=" + type + "]";
							}

							@XmlElement(name = "PID01", required = true)
                            protected String pid01;
                            @XmlElement(name = "PID02", required = true)
                            protected String pid02;
                            @XmlElement(name = "PID03", required = true)
                            protected String pid03;
                            @XmlElement(name = "PID04", required = true, nillable = true)
                            protected String pid04;
                            @XmlElement(name = "PID05", required = true)
                            protected String pid05;
                            @XmlAttribute(name = "type")
                            protected String type;

                            /**
                             * Gets the value of the pid01 property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getPID01() {
                                return pid01;
                            }

                            /**
                             * Sets the value of the pid01 property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setPID01(String value) {
                                this.pid01 = value;
                            }

                            /**
                             * Gets the value of the pid02 property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getPID02() {
                                return pid02;
                            }

                            /**
                             * Sets the value of the pid02 property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setPID02(String value) {
                                this.pid02 = value;
                            }

                            /**
                             * Gets the value of the pid03 property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getPID03() {
                                return pid03;
                            }

                            /**
                             * Sets the value of the pid03 property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setPID03(String value) {
                                this.pid03 = value;
                            }

                            /**
                             * Gets the value of the pid04 property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getPID04() {
                                return pid04;
                            }

                            /**
                             * Sets the value of the pid04 property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setPID04(String value) {
                                this.pid04 = value;
                            }

                            /**
                             * Gets the value of the pid05 property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getPID05() {
                                return pid05;
                            }

                            /**
                             * Sets the value of the pid05 property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setPID05(String value) {
                                this.pid05 = value;
                            }

                            /**
                             * Gets the value of the type property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getType() {
                                return type;
                            }

                            /**
                             * Sets the value of the type property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setType(String value) {
                                this.type = value;
                            }

                        }

                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="PO101">
                     *           &lt;simpleType>
                     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
                     *               &lt;enumeration value="1"/>
                     *               &lt;enumeration value="2"/>
                     *               &lt;enumeration value="3"/>
                     *               &lt;enumeration value="4"/>
                     *               &lt;enumeration value="5"/>
                     *               &lt;enumeration value="6"/>
                     *             &lt;/restriction>
                     *           &lt;/simpleType>
                     *         &lt;/element>
                     *         &lt;element name="PO102">
                     *           &lt;simpleType>
                     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}short">
                     *               &lt;enumeration value="120"/>
                     *               &lt;enumeration value="220"/>
                     *               &lt;enumeration value="126"/>
                     *               &lt;enumeration value="76"/>
                     *               &lt;enumeration value="72"/>
                     *               &lt;enumeration value="696"/>
                     *             &lt;/restriction>
                     *           &lt;/simpleType>
                     *         &lt;/element>
                     *         &lt;element name="PO103" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="PO104">
                     *           &lt;simpleType>
                     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}float">
                     *               &lt;enumeration value="9.25"/>
                     *               &lt;enumeration value="13.79"/>
                     *               &lt;enumeration value="10.99"/>
                     *               &lt;enumeration value="4.35"/>
                     *               &lt;enumeration value="7.5"/>
                     *               &lt;enumeration value="9.55"/>
                     *             &lt;/restriction>
                     *           &lt;/simpleType>
                     *         &lt;/element>
                     *         &lt;element name="PO105" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="PO106" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="PO107">
                     *           &lt;simpleType>
                     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                     *               &lt;enumeration value="065322-117"/>
                     *               &lt;enumeration value="066850-116"/>
                     *               &lt;enumeration value="060733-110"/>
                     *               &lt;enumeration value="065308-116"/>
                     *               &lt;enumeration value="065374-118"/>
                     *               &lt;enumeration value="067504-118"/>
                     *             &lt;/restriction>
                     *           &lt;/simpleType>
                     *         &lt;/element>
                     *         &lt;element name="PO108" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="PO109" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="PO110" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="PO111">
                     *           &lt;simpleType>
                     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                     *               &lt;enumeration value="AB3542"/>
                     *               &lt;enumeration value="RD5322"/>
                     *               &lt;enumeration value="XY5266"/>
                     *               &lt;enumeration value="VX2332"/>
                     *               &lt;enumeration value="RV0524"/>
                     *               &lt;enumeration value="DX1875"/>
                     *             &lt;/restriction>
                     *           &lt;/simpleType>
                     *         &lt;/element>
                     *       &lt;/sequence>
                     *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "po101",
                        "po102",
                        "po103",
                        "po104",
                        "po105",
                        "po106",
                        "po107",
                        "po108",
                        "po109",
                        "po110",
                        "po111"
                    })
                    public static class PO1 {

                        @Override
						public String toString() {
							return "PO1 [po101=" + po101 + ", po102=" + po102 + ", po103=" + po103 + ", po104=" + po104
									+ ", po105=" + po105 + ", po106=" + po106 + ", po107=" + po107 + ", po108=" + po108
									+ ", po109=" + po109 + ", po110=" + po110 + ", po111=" + po111 + ", type=" + type
									+ "]";
						}

						@XmlElement(name = "PO101")
                        protected byte po101;
                        @XmlElement(name = "PO102")
                        protected short po102;
                        @XmlElement(name = "PO103", required = true)
                        protected String po103;
                        @XmlElement(name = "PO104")
                        protected float po104;
                        @XmlElement(name = "PO105", required = true)
                        protected String po105;
                        @XmlElement(name = "PO106", required = true)
                        protected String po106;
                        @XmlElement(name = "PO107", required = true)
                        protected String po107;
                        @XmlElement(name = "PO108", required = true)
                        protected String po108;
                        @XmlElement(name = "PO109", required = true)
                        protected String po109;
                        @XmlElement(name = "PO110", required = true)
                        protected String po110;
                        @XmlElement(name = "PO111", required = true)
                        protected String po111;
                        @XmlAttribute(name = "type")
                        protected String type;

                        /**
                         * Gets the value of the po101 property.
                         * 
                         */
                        public byte getPO101() {
                            return po101;
                        }

                        /**
                         * Sets the value of the po101 property.
                         * 
                         */
                        public void setPO101(byte value) {
                            this.po101 = value;
                        }

                        /**
                         * Gets the value of the po102 property.
                         * 
                         */
                        public short getPO102() {
                            return po102;
                        }

                        /**
                         * Sets the value of the po102 property.
                         * 
                         */
                        public void setPO102(short value) {
                            this.po102 = value;
                        }

                        /**
                         * Gets the value of the po103 property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getPO103() {
                            return po103;
                        }

                        /**
                         * Sets the value of the po103 property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setPO103(String value) {
                            this.po103 = value;
                        }

                        /**
                         * Gets the value of the po104 property.
                         * 
                         */
                        public float getPO104() {
                            return po104;
                        }

                        /**
                         * Sets the value of the po104 property.
                         * 
                         */
                        public void setPO104(float value) {
                            this.po104 = value;
                        }

                        /**
                         * Gets the value of the po105 property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getPO105() {
                            return po105;
                        }

                        /**
                         * Sets the value of the po105 property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setPO105(String value) {
                            this.po105 = value;
                        }

                        /**
                         * Gets the value of the po106 property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getPO106() {
                            return po106;
                        }

                        /**
                         * Sets the value of the po106 property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setPO106(String value) {
                            this.po106 = value;
                        }

                        /**
                         * Gets the value of the po107 property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getPO107() {
                            return po107;
                        }

                        /**
                         * Sets the value of the po107 property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setPO107(String value) {
                            this.po107 = value;
                        }

                        /**
                         * Gets the value of the po108 property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getPO108() {
                            return po108;
                        }

                        /**
                         * Sets the value of the po108 property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setPO108(String value) {
                            this.po108 = value;
                        }

                        /**
                         * Gets the value of the po109 property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getPO109() {
                            return po109;
                        }

                        /**
                         * Sets the value of the po109 property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setPO109(String value) {
                            this.po109 = value;
                        }

                        /**
                         * Gets the value of the po110 property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getPO110() {
                            return po110;
                        }

                        /**
                         * Sets the value of the po110 property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setPO110(String value) {
                            this.po110 = value;
                        }

                        /**
                         * Gets the value of the po111 property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getPO111() {
                            return po111;
                        }

                        /**
                         * Sets the value of the po111 property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setPO111(String value) {
                            this.po111 = value;
                        }

                        /**
                         * Gets the value of the type property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getType() {
                            return type;
                        }

                        /**
                         * Sets the value of the type property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setType(String value) {
                            this.type = value;
                        }

                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="PO401">
                     *           &lt;simpleType>
                     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
                     *               &lt;enumeration value="4"/>
                     *               &lt;enumeration value="2"/>
                     *               &lt;enumeration value="6"/>
                     *             &lt;/restriction>
                     *           &lt;/simpleType>
                     *         &lt;/element>
                     *         &lt;element name="PO402">
                     *           &lt;simpleType>
                     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
                     *               &lt;enumeration value="4"/>
                     *               &lt;enumeration value="2"/>
                     *               &lt;enumeration value="1"/>
                     *               &lt;enumeration value="6"/>
                     *             &lt;/restriction>
                     *           &lt;/simpleType>
                     *         &lt;/element>
                     *         &lt;element name="PO403" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="PO404" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                     *         &lt;element name="PO405" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                     *         &lt;element name="PO406" minOccurs="0">
                     *           &lt;simpleType>
                     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
                     *               &lt;enumeration value="3"/>
                     *               &lt;enumeration value="6"/>
                     *             &lt;/restriction>
                     *           &lt;/simpleType>
                     *         &lt;/element>
                     *         &lt;element name="PO407" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                     *         &lt;element name="PO408" minOccurs="0">
                     *           &lt;simpleType>
                     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
                     *               &lt;enumeration value="15"/>
                     *               &lt;enumeration value="12"/>
                     *               &lt;enumeration value="19"/>
                     *               &lt;enumeration value="10"/>
                     *             &lt;/restriction>
                     *           &lt;/simpleType>
                     *         &lt;/element>
                     *         &lt;element name="PO409" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                     *       &lt;/sequence>
                     *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "po401",
                        "po402",
                        "po403",
                        "po404",
                        "po405",
                        "po406",
                        "po407",
                        "po408",
                        "po409"
                    })
                    public static class PO4 {

                        @Override
						public String toString() {
							return "PO4 [po401=" + po401 + ", po402=" + po402 + ", po403=" + po403 + ", po404=" + po404
									+ ", po405=" + po405 + ", po406=" + po406 + ", po407=" + po407 + ", po408=" + po408
									+ ", po409=" + po409 + ", type=" + type + "]";
						}

						@XmlElement(name = "PO401")
                        protected byte po401;
                        @XmlElement(name = "PO402")
                        protected byte po402;
                        @XmlElement(name = "PO403", required = true)
                        protected String po403;
                        @XmlElement(name = "PO404")
                        protected String po404;
                        @XmlElement(name = "PO405")
                        protected String po405;
                        @XmlElement(name = "PO406")
                        protected Byte po406;
                        @XmlElement(name = "PO407")
                        protected String po407;
                        @XmlElement(name = "PO408")
                        protected Byte po408;
                        @XmlElement(name = "PO409")
                        protected String po409;
                        @XmlAttribute(name = "type")
                        protected String type;

                        /**
                         * Gets the value of the po401 property.
                         * 
                         */
                        public byte getPO401() {
                            return po401;
                        }

                        /**
                         * Sets the value of the po401 property.
                         * 
                         */
                        public void setPO401(byte value) {
                            this.po401 = value;
                        }

                        /**
                         * Gets the value of the po402 property.
                         * 
                         */
                        public byte getPO402() {
                            return po402;
                        }

                        /**
                         * Sets the value of the po402 property.
                         * 
                         */
                        public void setPO402(byte value) {
                            this.po402 = value;
                        }

                        /**
                         * Gets the value of the po403 property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getPO403() {
                            return po403;
                        }

                        /**
                         * Sets the value of the po403 property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setPO403(String value) {
                            this.po403 = value;
                        }

                        /**
                         * Gets the value of the po404 property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getPO404() {
                            return po404;
                        }

                        /**
                         * Sets the value of the po404 property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setPO404(String value) {
                            this.po404 = value;
                        }

                        /**
                         * Gets the value of the po405 property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getPO405() {
                            return po405;
                        }

                        /**
                         * Sets the value of the po405 property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setPO405(String value) {
                            this.po405 = value;
                        }

                        /**
                         * Gets the value of the po406 property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Byte }
                         *     
                         */
                        public Byte getPO406() {
                            return po406;
                        }

                        /**
                         * Sets the value of the po406 property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Byte }
                         *     
                         */
                        public void setPO406(Byte value) {
                            this.po406 = value;
                        }

                        /**
                         * Gets the value of the po407 property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getPO407() {
                            return po407;
                        }

                        /**
                         * Sets the value of the po407 property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setPO407(String value) {
                            this.po407 = value;
                        }

                        /**
                         * Gets the value of the po408 property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Byte }
                         *     
                         */
                        public Byte getPO408() {
                            return po408;
                        }

                        /**
                         * Sets the value of the po408 property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Byte }
                         *     
                         */
                        public void setPO408(Byte value) {
                            this.po408 = value;
                        }

                        /**
                         * Gets the value of the po409 property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getPO409() {
                            return po409;
                        }

                        /**
                         * Sets the value of the po409 property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setPO409(String value) {
                            this.po409 = value;
                        }

                        /**
                         * Gets the value of the type property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getType() {
                            return type;
                        }

                        /**
                         * Sets the value of the type property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setType(String value) {
                            this.type = value;
                        }

                    }

                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="REF01">
                 *           &lt;simpleType>
                 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *               &lt;enumeration value="DP"/>
                 *               &lt;enumeration value="PS"/>
                 *             &lt;/restriction>
                 *           &lt;/simpleType>
                 *         &lt;/element>
                 *         &lt;element name="REF02">
                 *           &lt;simpleType>
                 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
                 *               &lt;enumeration value="038"/>
                 *               &lt;enumeration value="R"/>
                 *             &lt;/restriction>
                 *           &lt;/simpleType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "ref01",
                    "ref02"
                })
                public static class REF {

                    @Override
					public String toString() {
						return "REF [ref01=" + ref01 + ", ref02=" + ref02 + ", type=" + type + "]";
					}

					@XmlElement(name = "REF01", required = true)
                    protected String ref01;
                    @XmlElement(name = "REF02", required = true)
                    protected String ref02;
                    @XmlAttribute(name = "type")
                    protected String type;

                    /**
                     * Gets the value of the ref01 property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getREF01() {
                        return ref01;
                    }

                    /**
                     * Sets the value of the ref01 property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setREF01(String value) {
                        this.ref01 = value;
                    }

                    /**
                     * Gets the value of the ref02 property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getREF02() {
                        return ref02;
                    }

                    /**
                     * Sets the value of the ref02 property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setREF02(String value) {
                        this.ref02 = value;
                    }

                    /**
                     * Gets the value of the type property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getType() {
                        return type;
                    }

                    /**
                     * Sets the value of the type property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setType(String value) {
                        this.type = value;
                    }

                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="TD501" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="TD502" type="{http://www.w3.org/2001/XMLSchema}byte"/>
                 *         &lt;element name="TD503" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="TD504" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="TD505" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *       &lt;/sequence>
                 *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "td501",
                    "td502",
                    "td503",
                    "td504",
                    "td505"
                })
                public static class TD5 {

                    @Override
					public String toString() {
						return "TD5 [td501=" + td501 + ", td502=" + td502 + ", td503=" + td503 + ", td504=" + td504
								+ ", td505=" + td505 + ", type=" + type + "]";
					}

					@XmlElement(name = "TD501", required = true)
                    protected String td501;
                    @XmlElement(name = "TD502")
                    protected byte td502;
                    @XmlElement(name = "TD503", required = true)
                    protected String td503;
                    @XmlElement(name = "TD504", required = true)
                    protected String td504;
                    @XmlElement(name = "TD505", required = true)
                    protected String td505;
                    @XmlAttribute(name = "type")
                    protected String type;

                    /**
                     * Gets the value of the td501 property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getTD501() {
                        return td501;
                    }

                    /**
                     * Sets the value of the td501 property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setTD501(String value) {
                        this.td501 = value;
                    }

                    /**
                     * Gets the value of the td502 property.
                     * 
                     */
                    public byte getTD502() {
                        return td502;
                    }

                    /**
                     * Sets the value of the td502 property.
                     * 
                     */
                    public void setTD502(byte value) {
                        this.td502 = value;
                    }

                    /**
                     * Gets the value of the td503 property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getTD503() {
                        return td503;
                    }

                    /**
                     * Sets the value of the td503 property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setTD503(String value) {
                        this.td503 = value;
                    }

                    /**
                     * Gets the value of the td504 property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getTD504() {
                        return td504;
                    }

                    /**
                     * Sets the value of the td504 property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setTD504(String value) {
                        this.td504 = value;
                    }

                    /**
                     * Gets the value of the td505 property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getTD505() {
                        return td505;
                    }

                    /**
                     * Sets the value of the td505 property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setTD505(String value) {
                        this.td505 = value;
                    }

                    /**
                     * Gets the value of the type property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getType() {
                        return type;
                    }

                    /**
                     * Sets the value of the type property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setType(String value) {
                        this.type = value;
                    }

                }

            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ISA01" type="{http://www.w3.org/2001/XMLSchema}byte"/>
     *         &lt;element name="ISA02" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ISA03" type="{http://www.w3.org/2001/XMLSchema}byte"/>
     *         &lt;element name="ISA04" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ISA05" type="{http://www.w3.org/2001/XMLSchema}byte"/>
     *         &lt;element name="ISA06" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ISA07" type="{http://www.w3.org/2001/XMLSchema}byte"/>
     *         &lt;element name="ISA08" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ISA09" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="ISA10" type="{http://www.w3.org/2001/XMLSchema}short"/>
     *         &lt;element name="ISA11" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ISA12" type="{http://www.w3.org/2001/XMLSchema}short"/>
     *         &lt;element name="ISA13" type="{http://www.w3.org/2001/XMLSchema}short"/>
     *         &lt;element name="ISA14" type="{http://www.w3.org/2001/XMLSchema}byte"/>
     *         &lt;element name="ISA15" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ISA16" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "isa01",
        "isa02",
        "isa03",
        "isa04",
        "isa05",
        "isa06",
        "isa07",
        "isa08",
        "isa09",
        "isa10",
        "isa11",
        "isa12",
        "isa13",
        "isa14",
        "isa15",
        "isa16"
    })
    public static class Meta {

        @Override
		public String toString() {
			return "Meta [isa01=" + isa01 + ", isa02=" + isa02 + ", isa03=" + isa03 + ", isa04=" + isa04 + ", isa05="
					+ isa05 + ", isa06=" + isa06 + ", isa07=" + isa07 + ", isa08=" + isa08 + ", isa09=" + isa09
					+ ", isa10=" + isa10 + ", isa11=" + isa11 + ", isa12=" + isa12 + ", isa13=" + isa13 + ", isa14="
					+ isa14 + ", isa15=" + isa15 + ", isa16=" + isa16 + "]";
		}

		@XmlElement(name = "ISA01")
        protected byte isa01;
        @XmlElement(name = "ISA02", required = true)
        protected String isa02;
        @XmlElement(name = "ISA03")
        protected byte isa03;
        @XmlElement(name = "ISA04", required = true)
        protected String isa04;
        @XmlElement(name = "ISA05")
        protected byte isa05;
        @XmlElement(name = "ISA06", required = true)
        protected String isa06;
        @XmlElement(name = "ISA07")
        protected byte isa07;
        @XmlElement(name = "ISA08", required = true)
        protected String isa08;
        @XmlElement(name = "ISA09")
        protected int isa09;
        @XmlElement(name = "ISA10")
        protected short isa10;
        @XmlElement(name = "ISA11", required = true)
        protected String isa11;
        @XmlElement(name = "ISA12")
        protected short isa12;
        @XmlElement(name = "ISA13")
        protected short isa13;
        @XmlElement(name = "ISA14")
        protected byte isa14;
        @XmlElement(name = "ISA15", required = true)
        protected String isa15;
        @XmlElement(name = "ISA16", required = true)
        protected String isa16;

        /**
         * Gets the value of the isa01 property.
         * 
         */
        public byte getISA01() {
            return isa01;
        }

        /**
         * Sets the value of the isa01 property.
         * 
         */
        public void setISA01(byte value) {
            this.isa01 = value;
        }

        /**
         * Gets the value of the isa02 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getISA02() {
            return isa02;
        }

        /**
         * Sets the value of the isa02 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setISA02(String value) {
            this.isa02 = value;
        }

        /**
         * Gets the value of the isa03 property.
         * 
         */
        public byte getISA03() {
            return isa03;
        }

        /**
         * Sets the value of the isa03 property.
         * 
         */
        public void setISA03(byte value) {
            this.isa03 = value;
        }

        /**
         * Gets the value of the isa04 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getISA04() {
            return isa04;
        }

        /**
         * Sets the value of the isa04 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setISA04(String value) {
            this.isa04 = value;
        }

        /**
         * Gets the value of the isa05 property.
         * 
         */
        public byte getISA05() {
            return isa05;
        }

        /**
         * Sets the value of the isa05 property.
         * 
         */
        public void setISA05(byte value) {
            this.isa05 = value;
        }

        /**
         * Gets the value of the isa06 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getISA06() {
            return isa06;
        }

        /**
         * Sets the value of the isa06 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setISA06(String value) {
            this.isa06 = value;
        }

        /**
         * Gets the value of the isa07 property.
         * 
         */
        public byte getISA07() {
            return isa07;
        }

        /**
         * Sets the value of the isa07 property.
         * 
         */
        public void setISA07(byte value) {
            this.isa07 = value;
        }

        /**
         * Gets the value of the isa08 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getISA08() {
            return isa08;
        }

        /**
         * Sets the value of the isa08 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setISA08(String value) {
            this.isa08 = value;
        }

        /**
         * Gets the value of the isa09 property.
         * 
         */
        public int getISA09() {
            return isa09;
        }

        /**
         * Sets the value of the isa09 property.
         * 
         */
        public void setISA09(int value) {
            this.isa09 = value;
        }

        /**
         * Gets the value of the isa10 property.
         * 
         */
        public short getISA10() {
            return isa10;
        }

        /**
         * Sets the value of the isa10 property.
         * 
         */
        public void setISA10(short value) {
            this.isa10 = value;
        }

        /**
         * Gets the value of the isa11 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getISA11() {
            return isa11;
        }

        /**
         * Sets the value of the isa11 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setISA11(String value) {
            this.isa11 = value;
        }

        /**
         * Gets the value of the isa12 property.
         * 
         */
        public short getISA12() {
            return isa12;
        }

        /**
         * Sets the value of the isa12 property.
         * 
         */
        public void setISA12(short value) {
            this.isa12 = value;
        }

        /**
         * Gets the value of the isa13 property.
         * 
         */
        public short getISA13() {
            return isa13;
        }

        /**
         * Sets the value of the isa13 property.
         * 
         */
        public void setISA13(short value) {
            this.isa13 = value;
        }

        /**
         * Gets the value of the isa14 property.
         * 
         */
        public byte getISA14() {
            return isa14;
        }

        /**
         * Sets the value of the isa14 property.
         * 
         */
        public void setISA14(byte value) {
            this.isa14 = value;
        }

        /**
         * Gets the value of the isa15 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getISA15() {
            return isa15;
        }

        /**
         * Sets the value of the isa15 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setISA15(String value) {
            this.isa15 = value;
        }

        /**
         * Gets the value of the isa16 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getISA16() {
            return isa16;
        }

        /**
         * Sets the value of the isa16 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setISA16(String value) {
            this.isa16 = value;
        }

    }

}
