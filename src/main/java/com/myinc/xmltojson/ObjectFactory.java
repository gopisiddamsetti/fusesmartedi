
package com.myinc.xmltojson;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.rssbus package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.rssbus
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Interchange }
     * 
     */
    public Interchange createInterchange() {
        return new Interchange();
    }

    /**
     * Create an instance of {@link Interchange.FunctionalGroup }
     * 
     */
    public Interchange.FunctionalGroup createInterchangeFunctionalGroup() {
        return new Interchange.FunctionalGroup();
    }

    /**
     * Create an instance of {@link Interchange.FunctionalGroup.TransactionSet }
     * 
     */
    public Interchange.FunctionalGroup.TransactionSet createInterchangeFunctionalGroupTransactionSet() {
        return new Interchange.FunctionalGroup.TransactionSet();
    }

    /**
     * Create an instance of {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 }
     * 
     */
    public Interchange.FunctionalGroup.TransactionSet.TX00401850 createInterchangeFunctionalGroupTransactionSetTX00401850() {
        return new Interchange.FunctionalGroup.TransactionSet.TX00401850();
    }

    /**
     * Create an instance of {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .CTTLoop1 }
     * 
     */
    public Interchange.FunctionalGroup.TransactionSet.TX00401850 .CTTLoop1 createInterchangeFunctionalGroupTransactionSetTX00401850CTTLoop1() {
        return new Interchange.FunctionalGroup.TransactionSet.TX00401850 .CTTLoop1();
    }

    /**
     * Create an instance of {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1 }
     * 
     */
    public Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1 createInterchangeFunctionalGroupTransactionSetTX00401850PO1Loop1() {
        return new Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1();
    }

    /**
     * Create an instance of {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1 .PIDLoop1 }
     * 
     */
    public Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1 .PIDLoop1 createInterchangeFunctionalGroupTransactionSetTX00401850PO1Loop1PIDLoop1() {
        return new Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1 .PIDLoop1();
    }

    /**
     * Create an instance of {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .N1Loop1 }
     * 
     */
    public Interchange.FunctionalGroup.TransactionSet.TX00401850 .N1Loop1 createInterchangeFunctionalGroupTransactionSetTX00401850N1Loop1() {
        return new Interchange.FunctionalGroup.TransactionSet.TX00401850 .N1Loop1();
    }

    /**
     * Create an instance of {@link Interchange.Meta }
     * 
     */
    public Interchange.Meta createInterchangeMeta() {
        return new Interchange.Meta();
    }

    /**
     * Create an instance of {@link Interchange.FunctionalGroup.Meta }
     * 
     */
    public Interchange.FunctionalGroup.Meta createInterchangeFunctionalGroupMeta() {
        return new Interchange.FunctionalGroup.Meta();
    }

    /**
     * Create an instance of {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .Meta }
     * 
     */
    public Interchange.FunctionalGroup.TransactionSet.TX00401850 .Meta createInterchangeFunctionalGroupTransactionSetTX00401850Meta() {
        return new Interchange.FunctionalGroup.TransactionSet.TX00401850 .Meta();
    }

    /**
     * Create an instance of {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .BEG }
     * 
     */
    public Interchange.FunctionalGroup.TransactionSet.TX00401850 .BEG createInterchangeFunctionalGroupTransactionSetTX00401850BEG() {
        return new Interchange.FunctionalGroup.TransactionSet.TX00401850 .BEG();
    }

    /**
     * Create an instance of {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .REF }
     * 
     */
    public Interchange.FunctionalGroup.TransactionSet.TX00401850 .REF createInterchangeFunctionalGroupTransactionSetTX00401850REF() {
        return new Interchange.FunctionalGroup.TransactionSet.TX00401850 .REF();
    }

    /**
     * Create an instance of {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .ITD }
     * 
     */
    public Interchange.FunctionalGroup.TransactionSet.TX00401850 .ITD createInterchangeFunctionalGroupTransactionSetTX00401850ITD() {
        return new Interchange.FunctionalGroup.TransactionSet.TX00401850 .ITD();
    }

    /**
     * Create an instance of {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .DTM }
     * 
     */
    public Interchange.FunctionalGroup.TransactionSet.TX00401850 .DTM createInterchangeFunctionalGroupTransactionSetTX00401850DTM() {
        return new Interchange.FunctionalGroup.TransactionSet.TX00401850 .DTM();
    }

    /**
     * Create an instance of {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .PKG }
     * 
     */
    public Interchange.FunctionalGroup.TransactionSet.TX00401850 .PKG createInterchangeFunctionalGroupTransactionSetTX00401850PKG() {
        return new Interchange.FunctionalGroup.TransactionSet.TX00401850 .PKG();
    }

    /**
     * Create an instance of {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .TD5 }
     * 
     */
    public Interchange.FunctionalGroup.TransactionSet.TX00401850 .TD5 createInterchangeFunctionalGroupTransactionSetTX00401850TD5() {
        return new Interchange.FunctionalGroup.TransactionSet.TX00401850 .TD5();
    }

    /**
     * Create an instance of {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .CTTLoop1 .CTT }
     * 
     */
    public Interchange.FunctionalGroup.TransactionSet.TX00401850 .CTTLoop1 .CTT createInterchangeFunctionalGroupTransactionSetTX00401850CTTLoop1CTT() {
        return new Interchange.FunctionalGroup.TransactionSet.TX00401850 .CTTLoop1 .CTT();
    }

    /**
     * Create an instance of {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .CTTLoop1 .AMT }
     * 
     */
    public Interchange.FunctionalGroup.TransactionSet.TX00401850 .CTTLoop1 .AMT createInterchangeFunctionalGroupTransactionSetTX00401850CTTLoop1AMT() {
        return new Interchange.FunctionalGroup.TransactionSet.TX00401850 .CTTLoop1 .AMT();
    }

    /**
     * Create an instance of {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1 .PO1 }
     * 
     */
    public Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1 .PO1 createInterchangeFunctionalGroupTransactionSetTX00401850PO1Loop1PO1() {
        return new Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1 .PO1();
    }

    /**
     * Create an instance of {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1 .PO4 }
     * 
     */
    public Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1 .PO4 createInterchangeFunctionalGroupTransactionSetTX00401850PO1Loop1PO4() {
        return new Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1 .PO4();
    }

    /**
     * Create an instance of {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1 .PIDLoop1 .PID }
     * 
     */
    public Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1 .PIDLoop1 .PID createInterchangeFunctionalGroupTransactionSetTX00401850PO1Loop1PIDLoop1PID() {
        return new Interchange.FunctionalGroup.TransactionSet.TX00401850 .PO1Loop1 .PIDLoop1 .PID();
    }

    /**
     * Create an instance of {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .N1Loop1 .N1 }
     * 
     */
    public Interchange.FunctionalGroup.TransactionSet.TX00401850 .N1Loop1 .N1 createInterchangeFunctionalGroupTransactionSetTX00401850N1Loop1N1() {
        return new Interchange.FunctionalGroup.TransactionSet.TX00401850 .N1Loop1 .N1();
    }

    /**
     * Create an instance of {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .N1Loop1 .N3 }
     * 
     */
    public Interchange.FunctionalGroup.TransactionSet.TX00401850 .N1Loop1 .N3 createInterchangeFunctionalGroupTransactionSetTX00401850N1Loop1N3() {
        return new Interchange.FunctionalGroup.TransactionSet.TX00401850 .N1Loop1 .N3();
    }

    /**
     * Create an instance of {@link Interchange.FunctionalGroup.TransactionSet.TX00401850 .N1Loop1 .N4 }
     * 
     */
    public Interchange.FunctionalGroup.TransactionSet.TX00401850 .N1Loop1 .N4 createInterchangeFunctionalGroupTransactionSetTX00401850N1Loop1N4() {
        return new Interchange.FunctionalGroup.TransactionSet.TX00401850 .N1Loop1 .N4();
    }

}
