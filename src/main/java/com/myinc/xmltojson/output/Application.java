
package com.myinc.xmltojson.output;

import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "TransactionID",
    "ProjectID",
    "TransactionDateTimeStamp",
    "CorrelationID",
    "DocumentType",
    "TransactionType",
    "TransactionStatus",
    "System",
    "EnvironmentType",
    "Organisation",
    "LogType"
})
public class Application {

    @JsonProperty("TransactionID")
    private String TransactionID;
    @JsonProperty("ProjectID")
    private String ProjectID;
    @JsonProperty("TransactionDateTimeStamp")
    private String TransactionDateTimeStamp;
    @JsonProperty("CorrelationID")
    private String CorrelationID;
    @JsonProperty("DocumentType")
    private String DocumentType;
    @JsonProperty("TransactionType")
    private String TransactionType;
    @JsonProperty("TransactionStatus")
    private String TransactionStatus;
    @JsonProperty("System")
    private String System;
    @JsonProperty("EnvironmentType")
    private String EnvironmentType;
    @JsonProperty("Organisation")
    private String Organisation;
    @JsonProperty("LogType")
    private String LogType;

    /**
     * 
     * @return
     *     The TransactionID
     */
    @JsonProperty("TransactionID")
    public String getTransactionID() {
        return TransactionID;
    }

    /**
     * 
     * @param TransactionID
     *     The TransactionID
     */
    @JsonProperty("TransactionID")
    public void setTransactionID(String TransactionID) {
        this.TransactionID = TransactionID;
    }

    /**
     * 
     * @return
     *     The ProjectID
     */
    @JsonProperty("ProjectID")
    public String getProjectID() {
        return ProjectID;
    }

    /**
     * 
     * @param ProjectID
     *     The ProjectID
     */
    @JsonProperty("ProjectID")
    public void setProjectID(String ProjectID) {
        this.ProjectID = ProjectID;
    }

    /**
     * 
     * @return
     *     The TransactionDateTimeStamp
     */
    @JsonProperty("TransactionDateTimeStamp")
    public String getTransactionDateTimeStamp() {
        return TransactionDateTimeStamp;
    }

    /**
     * 
     * @param TransactionDateTimeStamp
     *     The TransactionDateTimeStamp
     */
    @JsonProperty("TransactionDateTimeStamp")
    public void setTransactionDateTimeStamp(String TransactionDateTimeStamp) {
        this.TransactionDateTimeStamp = TransactionDateTimeStamp;
    }

    /**
     * 
     * @return
     *     The CorrelationID
     */
    @JsonProperty("CorrelationID")
    public String getCorrelationID() {
        return CorrelationID;
    }

    /**
     * 
     * @param CorrelationID
     *     The CorrelationID
     */
    @JsonProperty("CorrelationID")
    public void setCorrelationID(String CorrelationID) {
        this.CorrelationID = CorrelationID;
    }

    /**
     * 
     * @return
     *     The DocumentType
     */
    @JsonProperty("DocumentType")
    public String getDocumentType() {
        return DocumentType;
    }

    /**
     * 
     * @param DocumentType
     *     The DocumentType
     */
    @JsonProperty("DocumentType")
    public void setDocumentType(String DocumentType) {
        this.DocumentType = DocumentType;
    }

    /**
     * 
     * @return
     *     The TransactionType
     */
    @JsonProperty("TransactionType")
    public String getTransactionType() {
        return TransactionType;
    }

    /**
     * 
     * @param TransactionType
     *     The TransactionType
     */
    @JsonProperty("TransactionType")
    public void setTransactionType(String TransactionType) {
        this.TransactionType = TransactionType;
    }

    /**
     * 
     * @return
     *     The TransactionStatus
     */
    @JsonProperty("TransactionStatus")
    public String getTransactionStatus() {
        return TransactionStatus;
    }

    /**
     * 
     * @param TransactionStatus
     *     The TransactionStatus
     */
    @JsonProperty("TransactionStatus")
    public void setTransactionStatus(String TransactionStatus) {
        this.TransactionStatus = TransactionStatus;
    }

    /**
     * 
     * @return
     *     The System
     */
    @JsonProperty("System")
    public String getSystem() {
        return System;
    }

    /**
     * 
     * @param System
     *     The System
     */
    @JsonProperty("System")
    public void setSystem(String System) {
        this.System = System;
    }

    /**
     * 
     * @return
     *     The EnvironmentType
     */
    @JsonProperty("EnvironmentType")
    public String getEnvironmentType() {
        return EnvironmentType;
    }

    /**
     * 
     * @param EnvironmentType
     *     The EnvironmentType
     */
    @JsonProperty("EnvironmentType")
    public void setEnvironmentType(String EnvironmentType) {
        this.EnvironmentType = EnvironmentType;
    }

    /**
     * 
     * @return
     *     The Organisation
     */
    @JsonProperty("Organisation")
    public String getOrganisation() {
        return Organisation;
    }

    /**
     * 
     * @param Organisation
     *     The Organisation
     */
    @JsonProperty("Organisation")
    public void setOrganisation(String Organisation) {
        this.Organisation = Organisation;
    }

    /**
     * 
     * @return
     *     The LogType
     */
    @JsonProperty("LogType")
    public String getLogType() {
        return LogType;
    }

    /**
     * 
     * @param LogType
     *     The LogType
     */
    @JsonProperty("LogType")
    public void setLogType(String LogType) {
        this.LogType = LogType;
    }

}
