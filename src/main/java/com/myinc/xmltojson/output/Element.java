
package com.myinc.xmltojson.output;

import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "ErronNotification",
    "ErrorComment",
    "ErrorDump",
    "ErrorID",
    "ErrorLocation",
    "ErrorMessage",
    "ErrorStatus",
    "ErrorType"
})
public class Element {

    @JsonProperty("ErronNotification")
    private String ErronNotification;
    @JsonProperty("ErrorComment")
    private String ErrorComment;
    @JsonProperty("ErrorDump")
    private String ErrorDump;
    @JsonProperty("ErrorID")
    private String ErrorID;
    @JsonProperty("ErrorLocation")
    private String ErrorLocation;
    @JsonProperty("ErrorMessage")
    private String ErrorMessage;
    @JsonProperty("ErrorStatus")
    private String ErrorStatus;
    @JsonProperty("ErrorType")
    private String ErrorType;

    /**
     * 
     * @return
     *     The ErronNotification
     */
    @JsonProperty("ErronNotification")
    public String getErronNotification() {
        return ErronNotification;
    }

    /**
     * 
     * @param ErronNotification
     *     The ErronNotification
     */
    @JsonProperty("ErronNotification")
    public void setErronNotification(String ErronNotification) {
        this.ErronNotification = ErronNotification;
    }

    /**
     * 
     * @return
     *     The ErrorComment
     */
    @JsonProperty("ErrorComment")
    public String getErrorComment() {
        return ErrorComment;
    }

    /**
     * 
     * @param ErrorComment
     *     The ErrorComment
     */
    @JsonProperty("ErrorComment")
    public void setErrorComment(String ErrorComment) {
        this.ErrorComment = ErrorComment;
    }

    /**
     * 
     * @return
     *     The ErrorDump
     */
    @JsonProperty("ErrorDump")
    public String getErrorDump() {
        return ErrorDump;
    }

    /**
     * 
     * @param ErrorDump
     *     The ErrorDump
     */
    @JsonProperty("ErrorDump")
    public void setErrorDump(String ErrorDump) {
        this.ErrorDump = ErrorDump;
    }

    /**
     * 
     * @return
     *     The ErrorID
     */
    @JsonProperty("ErrorID")
    public String getErrorID() {
        return ErrorID;
    }

    /**
     * 
     * @param ErrorID
     *     The ErrorID
     */
    @JsonProperty("ErrorID")
    public void setErrorID(String ErrorID) {
        this.ErrorID = ErrorID;
    }

    /**
     * 
     * @return
     *     The ErrorLocation
     */
    @JsonProperty("ErrorLocation")
    public String getErrorLocation() {
        return ErrorLocation;
    }

    /**
     * 
     * @param ErrorLocation
     *     The ErrorLocation
     */
    @JsonProperty("ErrorLocation")
    public void setErrorLocation(String ErrorLocation) {
        this.ErrorLocation = ErrorLocation;
    }

    /**
     * 
     * @return
     *     The ErrorMessage
     */
    @JsonProperty("ErrorMessage")
    public String getErrorMessage() {
        return ErrorMessage;
    }

    /**
     * 
     * @param ErrorMessage
     *     The ErrorMessage
     */
    @JsonProperty("ErrorMessage")
    public void setErrorMessage(String ErrorMessage) {
        this.ErrorMessage = ErrorMessage;
    }

    /**
     * 
     * @return
     *     The ErrorStatus
     */
    @JsonProperty("ErrorStatus")
    public String getErrorStatus() {
        return ErrorStatus;
    }

    /**
     * 
     * @param ErrorStatus
     *     The ErrorStatus
     */
    @JsonProperty("ErrorStatus")
    public void setErrorStatus(String ErrorStatus) {
        this.ErrorStatus = ErrorStatus;
    }

    /**
     * 
     * @return
     *     The ErrorType
     */
    @JsonProperty("ErrorType")
    public String getErrorType() {
        return ErrorType;
    }

    /**
     * 
     * @param ErrorType
     *     The ErrorType
     */
    @JsonProperty("ErrorType")
    public void setErrorType(String ErrorType) {
        this.ErrorType = ErrorType;
    }

}
