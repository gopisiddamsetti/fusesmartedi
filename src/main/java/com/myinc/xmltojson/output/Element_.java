
package com.myinc.xmltojson.output;

import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "KeyPerformanceIndicator1",
    "KeyPerformanceIndicator2",
    "KeyPerformanceIndicator3",
    "KeyPerformanceIndicator4",
    "KeyPerformanceIndicator5",
    "KeyPerformanceKeyName1",
    "KeyPerformanceKeyName2",
    "KeyPerformanceKeyName3",
    "KeyPerformanceKeyName4",
    "KeyPerformanceKeyName5"
})
public class Element_ {

    @JsonProperty("KeyPerformanceIndicator1")
    private String KeyPerformanceIndicator1;
    @JsonProperty("KeyPerformanceIndicator2")
    private String KeyPerformanceIndicator2;
    @JsonProperty("KeyPerformanceIndicator3")
    private String KeyPerformanceIndicator3;
    @JsonProperty("KeyPerformanceIndicator4")
    private String KeyPerformanceIndicator4;
    @JsonProperty("KeyPerformanceIndicator5")
    private String KeyPerformanceIndicator5;
    @JsonProperty("KeyPerformanceKeyName1")
    private String KeyPerformanceKeyName1;
    @JsonProperty("KeyPerformanceKeyName2")
    private String KeyPerformanceKeyName2;
    @JsonProperty("KeyPerformanceKeyName3")
    private String KeyPerformanceKeyName3;
    @JsonProperty("KeyPerformanceKeyName4")
    private String KeyPerformanceKeyName4;
    @JsonProperty("KeyPerformanceKeyName5")
    private String KeyPerformanceKeyName5;

    /**
     * 
     * @return
     *     The KeyPerformanceIndicator1
     */
    @JsonProperty("KeyPerformanceIndicator1")
    public String getKeyPerformanceIndicator1() {
        return KeyPerformanceIndicator1;
    }

    /**
     * 
     * @param KeyPerformanceIndicator1
     *     The KeyPerformanceIndicator1
     */
    @JsonProperty("KeyPerformanceIndicator1")
    public void setKeyPerformanceIndicator1(String KeyPerformanceIndicator1) {
        this.KeyPerformanceIndicator1 = KeyPerformanceIndicator1;
    }

    /**
     * 
     * @return
     *     The KeyPerformanceIndicator2
     */
    @JsonProperty("KeyPerformanceIndicator2")
    public String getKeyPerformanceIndicator2() {
        return KeyPerformanceIndicator2;
    }

    /**
     * 
     * @param KeyPerformanceIndicator2
     *     The KeyPerformanceIndicator2
     */
    @JsonProperty("KeyPerformanceIndicator2")
    public void setKeyPerformanceIndicator2(String KeyPerformanceIndicator2) {
        this.KeyPerformanceIndicator2 = KeyPerformanceIndicator2;
    }

    /**
     * 
     * @return
     *     The KeyPerformanceIndicator3
     */
    @JsonProperty("KeyPerformanceIndicator3")
    public String getKeyPerformanceIndicator3() {
        return KeyPerformanceIndicator3;
    }

    /**
     * 
     * @param KeyPerformanceIndicator3
     *     The KeyPerformanceIndicator3
     */
    @JsonProperty("KeyPerformanceIndicator3")
    public void setKeyPerformanceIndicator3(String KeyPerformanceIndicator3) {
        this.KeyPerformanceIndicator3 = KeyPerformanceIndicator3;
    }

    /**
     * 
     * @return
     *     The KeyPerformanceIndicator4
     */
    @JsonProperty("KeyPerformanceIndicator4")
    public String getKeyPerformanceIndicator4() {
        return KeyPerformanceIndicator4;
    }

    /**
     * 
     * @param KeyPerformanceIndicator4
     *     The KeyPerformanceIndicator4
     */
    @JsonProperty("KeyPerformanceIndicator4")
    public void setKeyPerformanceIndicator4(String KeyPerformanceIndicator4) {
        this.KeyPerformanceIndicator4 = KeyPerformanceIndicator4;
    }

    /**
     * 
     * @return
     *     The KeyPerformanceIndicator5
     */
    @JsonProperty("KeyPerformanceIndicator5")
    public String getKeyPerformanceIndicator5() {
        return KeyPerformanceIndicator5;
    }

    /**
     * 
     * @param KeyPerformanceIndicator5
     *     The KeyPerformanceIndicator5
     */
    @JsonProperty("KeyPerformanceIndicator5")
    public void setKeyPerformanceIndicator5(String KeyPerformanceIndicator5) {
        this.KeyPerformanceIndicator5 = KeyPerformanceIndicator5;
    }

    /**
     * 
     * @return
     *     The KeyPerformanceKeyName1
     */
    @JsonProperty("KeyPerformanceKeyName1")
    public String getKeyPerformanceKeyName1() {
        return KeyPerformanceKeyName1;
    }

    /**
     * 
     * @param KeyPerformanceKeyName1
     *     The KeyPerformanceKeyName1
     */
    @JsonProperty("KeyPerformanceKeyName1")
    public void setKeyPerformanceKeyName1(String KeyPerformanceKeyName1) {
        this.KeyPerformanceKeyName1 = KeyPerformanceKeyName1;
    }

    /**
     * 
     * @return
     *     The KeyPerformanceKeyName2
     */
    @JsonProperty("KeyPerformanceKeyName2")
    public String getKeyPerformanceKeyName2() {
        return KeyPerformanceKeyName2;
    }

    /**
     * 
     * @param KeyPerformanceKeyName2
     *     The KeyPerformanceKeyName2
     */
    @JsonProperty("KeyPerformanceKeyName2")
    public void setKeyPerformanceKeyName2(String KeyPerformanceKeyName2) {
        this.KeyPerformanceKeyName2 = KeyPerformanceKeyName2;
    }

    /**
     * 
     * @return
     *     The KeyPerformanceKeyName3
     */
    @JsonProperty("KeyPerformanceKeyName3")
    public String getKeyPerformanceKeyName3() {
        return KeyPerformanceKeyName3;
    }

    /**
     * 
     * @param KeyPerformanceKeyName3
     *     The KeyPerformanceKeyName3
     */
    @JsonProperty("KeyPerformanceKeyName3")
    public void setKeyPerformanceKeyName3(String KeyPerformanceKeyName3) {
        this.KeyPerformanceKeyName3 = KeyPerformanceKeyName3;
    }

    /**
     * 
     * @return
     *     The KeyPerformanceKeyName4
     */
    @JsonProperty("KeyPerformanceKeyName4")
    public String getKeyPerformanceKeyName4() {
        return KeyPerformanceKeyName4;
    }

    /**
     * 
     * @param KeyPerformanceKeyName4
     *     The KeyPerformanceKeyName4
     */
    @JsonProperty("KeyPerformanceKeyName4")
    public void setKeyPerformanceKeyName4(String KeyPerformanceKeyName4) {
        this.KeyPerformanceKeyName4 = KeyPerformanceKeyName4;
    }

    /**
     * 
     * @return
     *     The KeyPerformanceKeyName5
     */
    @JsonProperty("KeyPerformanceKeyName5")
    public String getKeyPerformanceKeyName5() {
        return KeyPerformanceKeyName5;
    }

    /**
     * 
     * @param KeyPerformanceKeyName5
     *     The KeyPerformanceKeyName5
     */
    @JsonProperty("KeyPerformanceKeyName5")
    public void setKeyPerformanceKeyName5(String KeyPerformanceKeyName5) {
        this.KeyPerformanceKeyName5 = KeyPerformanceKeyName5;
    }

}
