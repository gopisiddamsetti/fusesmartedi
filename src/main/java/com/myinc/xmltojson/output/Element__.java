
package com.myinc.xmltojson.output;

import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "TransactionKeyName",
    "TransactionKeyReference",
    "TransactionKeyValue"
})
public class Element__ {

    @JsonProperty("TransactionKeyName")
    private String TransactionKeyName;
    @JsonProperty("TransactionKeyReference")
    private String TransactionKeyReference;
    @JsonProperty("TransactionKeyValue")
    private String TransactionKeyValue;

    /**
     * 
     * @return
     *     The TransactionKeyName
     */
    @JsonProperty("TransactionKeyName")
    public String getTransactionKeyName() {
        return TransactionKeyName;
    }

    /**
     * 
     * @param TransactionKeyName
     *     The TransactionKeyName
     */
    @JsonProperty("TransactionKeyName")
    public void setTransactionKeyName(String TransactionKeyName) {
        this.TransactionKeyName = TransactionKeyName;
    }

    /**
     * 
     * @return
     *     The TransactionKeyReference
     */
    @JsonProperty("TransactionKeyReference")
    public String getTransactionKeyReference() {
        return TransactionKeyReference;
    }

    /**
     * 
     * @param TransactionKeyReference
     *     The TransactionKeyReference
     */
    @JsonProperty("TransactionKeyReference")
    public void setTransactionKeyReference(String TransactionKeyReference) {
        this.TransactionKeyReference = TransactionKeyReference;
    }

    /**
     * 
     * @return
     *     The TransactionKeyValue
     */
    @JsonProperty("TransactionKeyValue")
    public String getTransactionKeyValue() {
        return TransactionKeyValue;
    }

    /**
     * 
     * @param TransactionKeyValue
     *     The TransactionKeyValue
     */
    @JsonProperty("TransactionKeyValue")
    public void setTransactionKeyValue(String TransactionKeyValue) {
        this.TransactionKeyValue = TransactionKeyValue;
    }

}
