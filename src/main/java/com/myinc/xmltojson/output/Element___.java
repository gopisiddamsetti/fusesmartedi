
package com.myinc.xmltojson.output;

import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "Payload",
    "RelationTransactionID"
})
public class Element___ {

    @JsonProperty("Payload")
    private com.myinc.xmltojson.output.Payload Payload;
    @JsonProperty("RelationTransactionID")
    private com.myinc.xmltojson.output.RelationTransactionID RelationTransactionID;

    /**
     * 
     * @return
     *     The Payload
     */
    @JsonProperty("Payload")
    public com.myinc.xmltojson.output.Payload getPayload() {
        return Payload;
    }

    /**
     * 
     * @param Payload
     *     The Payload
     */
    @JsonProperty("Payload")
    public void setPayload(com.myinc.xmltojson.output.Payload Payload) {
        this.Payload = Payload;
    }

    /**
     * 
     * @return
     *     The RelationTransactionID
     */
    @JsonProperty("RelationTransactionID")
    public com.myinc.xmltojson.output.RelationTransactionID getRelationTransactionID() {
        return RelationTransactionID;
    }

    /**
     * 
     * @param RelationTransactionID
     *     The RelationTransactionID
     */
    @JsonProperty("RelationTransactionID")
    public void setRelationTransactionID(com.myinc.xmltojson.output.RelationTransactionID RelationTransactionID) {
        this.RelationTransactionID = RelationTransactionID;
    }

}
