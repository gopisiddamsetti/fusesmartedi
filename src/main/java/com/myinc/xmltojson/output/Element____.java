
package com.myinc.xmltojson.output;

import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "ChildID",
    "ParentTransactionID"
})
public class Element____ {

    @JsonProperty("ChildID")
    private String ChildID;
    @JsonProperty("ParentTransactionID")
    private String ParentTransactionID;

    /**
     * 
     * @return
     *     The ChildID
     */
    @JsonProperty("ChildID")
    public String getChildID() {
        return ChildID;
    }

    /**
     * 
     * @param ChildID
     *     The ChildID
     */
    @JsonProperty("ChildID")
    public void setChildID(String ChildID) {
        this.ChildID = ChildID;
    }

    /**
     * 
     * @return
     *     The ParentTransactionID
     */
    @JsonProperty("ParentTransactionID")
    public String getParentTransactionID() {
        return ParentTransactionID;
    }

    /**
     * 
     * @param ParentTransactionID
     *     The ParentTransactionID
     */
    @JsonProperty("ParentTransactionID")
    public void setParentTransactionID(String ParentTransactionID) {
        this.ParentTransactionID = ParentTransactionID;
    }

}
