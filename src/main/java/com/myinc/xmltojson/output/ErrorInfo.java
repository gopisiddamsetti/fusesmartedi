
package com.myinc.xmltojson.output;

import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "ErrorDetails"
})
public class ErrorInfo {

    @JsonProperty("ErrorDetails")
    private com.myinc.xmltojson.output.ErrorDetails ErrorDetails;

    /**
     * 
     * @return
     *     The ErrorDetails
     */
    @JsonProperty("ErrorDetails")
    public com.myinc.xmltojson.output.ErrorDetails getErrorDetails() {
        return ErrorDetails;
    }

    /**
     * 
     * @param ErrorDetails
     *     The ErrorDetails
     */
    @JsonProperty("ErrorDetails")
    public void setErrorDetails(com.myinc.xmltojson.output.ErrorDetails ErrorDetails) {
        this.ErrorDetails = ErrorDetails;
    }

}
