
package com.myinc.xmltojson.output;

import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "Application",
    "ErrorInfo",
    "InterfaceDetails",
    "TransactionInfo"
})
public class InterchangeOutPut {

    @JsonProperty("Application")
    private com.myinc.xmltojson.output.Application Application;
    @JsonProperty("ErrorInfo")
    private com.myinc.xmltojson.output.ErrorInfo ErrorInfo;
    @JsonProperty("InterfaceDetails")
    private com.myinc.xmltojson.output.InterfaceDetails InterfaceDetails;
    @JsonProperty("TransactionInfo")
    private com.myinc.xmltojson.output.TransactionInfo TransactionInfo;

    /**
     * 
     * @return
     *     The Application
     */
    @JsonProperty("Application")
    public com.myinc.xmltojson.output.Application getApplication() {
        return Application;
    }

    /**
     * 
     * @param Application
     *     The Application
     */
    @JsonProperty("Application")
    public void setApplication(com.myinc.xmltojson.output.Application Application) {
        this.Application = Application;
    }

    /**
     * 
     * @return
     *     The ErrorInfo
     */
    @JsonProperty("ErrorInfo")
    public com.myinc.xmltojson.output.ErrorInfo getErrorInfo() {
        return ErrorInfo;
    }

    /**
     * 
     * @param ErrorInfo
     *     The ErrorInfo
     */
    @JsonProperty("ErrorInfo")
    public void setErrorInfo(com.myinc.xmltojson.output.ErrorInfo ErrorInfo) {
        this.ErrorInfo = ErrorInfo;
    }

    /**
     * 
     * @return
     *     The InterfaceDetails
     */
    @JsonProperty("InterfaceDetails")
    public com.myinc.xmltojson.output.InterfaceDetails getInterfaceDetails() {
        return InterfaceDetails;
    }

    /**
     * 
     * @param InterfaceDetails
     *     The InterfaceDetails
     */
    @JsonProperty("InterfaceDetails")
    public void setInterfaceDetails(com.myinc.xmltojson.output.InterfaceDetails InterfaceDetails) {
        this.InterfaceDetails = InterfaceDetails;
    }

    /**
     * 
     * @return
     *     The TransactionInfo
     */
    @JsonProperty("TransactionInfo")
    public com.myinc.xmltojson.output.TransactionInfo getTransactionInfo() {
        return TransactionInfo;
    }

    /**
     * 
     * @param TransactionInfo
     *     The TransactionInfo
     */
    @JsonProperty("TransactionInfo")
    public void setTransactionInfo(com.myinc.xmltojson.output.TransactionInfo TransactionInfo) {
        this.TransactionInfo = TransactionInfo;
    }

}
