
package com.myinc.xmltojson.output;

import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "BusinessKeyPerformanceValues",
    "InterfaceID",
    "InterfaceModule",
    "InterfaceType",
    "Region",
    "SourceSystemID",
    "SourceSystemTranID",
    "TargetSystemID",
    "TechnicalKeyValues"
})
public class InterfaceDetails {

    @JsonProperty("BusinessKeyPerformanceValues")
    private com.myinc.xmltojson.output.BusinessKeyPerformanceValues BusinessKeyPerformanceValues;
    @JsonProperty("InterfaceID")
    private String InterfaceID;
    @JsonProperty("InterfaceModule")
    private String InterfaceModule;
    @JsonProperty("InterfaceType")
    private String InterfaceType;
    @JsonProperty("Region")
    private String Region;
    @JsonProperty("SourceSystemID")
    private String SourceSystemID;
    @JsonProperty("SourceSystemTranID")
    private String SourceSystemTranID;
    @JsonProperty("TargetSystemID")
    private String TargetSystemID;
    @JsonProperty("TechnicalKeyValues")
    private com.myinc.xmltojson.output.TechnicalKeyValues TechnicalKeyValues;

    /**
     * 
     * @return
     *     The BusinessKeyPerformanceValues
     */
    @JsonProperty("BusinessKeyPerformanceValues")
    public com.myinc.xmltojson.output.BusinessKeyPerformanceValues getBusinessKeyPerformanceValues() {
        return BusinessKeyPerformanceValues;
    }

    /**
     * 
     * @param BusinessKeyPerformanceValues
     *     The BusinessKeyPerformanceValues
     */
    @JsonProperty("BusinessKeyPerformanceValues")
    public void setBusinessKeyPerformanceValues(com.myinc.xmltojson.output.BusinessKeyPerformanceValues BusinessKeyPerformanceValues) {
        this.BusinessKeyPerformanceValues = BusinessKeyPerformanceValues;
    }

    /**
     * 
     * @return
     *     The InterfaceID
     */
    @JsonProperty("InterfaceID")
    public String getInterfaceID() {
        return InterfaceID;
    }

    /**
     * 
     * @param InterfaceID
     *     The InterfaceID
     */
    @JsonProperty("InterfaceID")
    public void setInterfaceID(String InterfaceID) {
        this.InterfaceID = InterfaceID;
    }

    /**
     * 
     * @return
     *     The InterfaceModule
     */
    @JsonProperty("InterfaceModule")
    public String getInterfaceModule() {
        return InterfaceModule;
    }

    /**
     * 
     * @param InterfaceModule
     *     The InterfaceModule
     */
    @JsonProperty("InterfaceModule")
    public void setInterfaceModule(String InterfaceModule) {
        this.InterfaceModule = InterfaceModule;
    }

    /**
     * 
     * @return
     *     The InterfaceType
     */
    @JsonProperty("InterfaceType")
    public String getInterfaceType() {
        return InterfaceType;
    }

    /**
     * 
     * @param InterfaceType
     *     The InterfaceType
     */
    @JsonProperty("InterfaceType")
    public void setInterfaceType(String InterfaceType) {
        this.InterfaceType = InterfaceType;
    }

    /**
     * 
     * @return
     *     The Region
     */
    @JsonProperty("Region")
    public String getRegion() {
        return Region;
    }

    /**
     * 
     * @param Region
     *     The Region
     */
    @JsonProperty("Region")
    public void setRegion(String Region) {
        this.Region = Region;
    }

    /**
     * 
     * @return
     *     The SourceSystemID
     */
    @JsonProperty("SourceSystemID")
    public String getSourceSystemID() {
        return SourceSystemID;
    }

    /**
     * 
     * @param SourceSystemID
     *     The SourceSystemID
     */
    @JsonProperty("SourceSystemID")
    public void setSourceSystemID(String SourceSystemID) {
        this.SourceSystemID = SourceSystemID;
    }

    /**
     * 
     * @return
     *     The SourceSystemTranID
     */
    @JsonProperty("SourceSystemTranID")
    public String getSourceSystemTranID() {
        return SourceSystemTranID;
    }

    /**
     * 
     * @param SourceSystemTranID
     *     The SourceSystemTranID
     */
    @JsonProperty("SourceSystemTranID")
    public void setSourceSystemTranID(String SourceSystemTranID) {
        this.SourceSystemTranID = SourceSystemTranID;
    }

    /**
     * 
     * @return
     *     The TargetSystemID
     */
    @JsonProperty("TargetSystemID")
    public String getTargetSystemID() {
        return TargetSystemID;
    }

    /**
     * 
     * @param TargetSystemID
     *     The TargetSystemID
     */
    @JsonProperty("TargetSystemID")
    public void setTargetSystemID(String TargetSystemID) {
        this.TargetSystemID = TargetSystemID;
    }

    /**
     * 
     * @return
     *     The TechnicalKeyValues
     */
    @JsonProperty("TechnicalKeyValues")
    public com.myinc.xmltojson.output.TechnicalKeyValues getTechnicalKeyValues() {
        return TechnicalKeyValues;
    }

    /**
     * 
     * @param TechnicalKeyValues
     *     The TechnicalKeyValues
     */
    @JsonProperty("TechnicalKeyValues")
    public void setTechnicalKeyValues(com.myinc.xmltojson.output.TechnicalKeyValues TechnicalKeyValues) {
        this.TechnicalKeyValues = TechnicalKeyValues;
    }

}
