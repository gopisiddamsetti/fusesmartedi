
package com.myinc.xmltojson.output;

import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "InterchangeOutPut"
})
public class OutPut {

    @JsonProperty("InterchangeOutPut")
    private com.myinc.xmltojson.output.InterchangeOutPut InterchangeOutPut;

    /**
     * 
     * @return
     *     The InterchangeOutPut
     */
    @JsonProperty("InterchangeOutPut")
    public com.myinc.xmltojson.output.InterchangeOutPut getInterchangeOutPut() {
        return InterchangeOutPut;
    }

    /**
     * 
     * @param InterchangeOutPut
     *     The InterchangeOutPut
     */
    @JsonProperty("InterchangeOutPut")
    public void setInterchangeOutPut(com.myinc.xmltojson.output.InterchangeOutPut InterchangeOutPut) {
        this.InterchangeOutPut = InterchangeOutPut;
    }

}
