
package com.myinc.xmltojson.output;

import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "Payload",
    "SourceType",
    "SourceTypeID"
})
public class Payload {

    @JsonProperty("Payload")
    private String Payload;
    @JsonProperty("SourceType")
    private String SourceType;
    @JsonProperty("SourceTypeID")
    private String SourceTypeID;

    /**
     * 
     * @return
     *     The Payload
     */
    @JsonProperty("Payload")
    public String getPayload() {
        return Payload;
    }

    /**
     * 
     * @param Payload
     *     The Payload
     */
    @JsonProperty("Payload")
    public void setPayload(String Payload) {
        this.Payload = Payload;
    }

    /**
     * 
     * @return
     *     The SourceType
     */
    @JsonProperty("SourceType")
    public String getSourceType() {
        return SourceType;
    }

    /**
     * 
     * @param SourceType
     *     The SourceType
     */
    @JsonProperty("SourceType")
    public void setSourceType(String SourceType) {
        this.SourceType = SourceType;
    }

    /**
     * 
     * @return
     *     The SourceTypeID
     */
    @JsonProperty("SourceTypeID")
    public String getSourceTypeID() {
        return SourceTypeID;
    }

    /**
     * 
     * @param SourceTypeID
     *     The SourceTypeID
     */
    @JsonProperty("SourceTypeID")
    public void setSourceTypeID(String SourceTypeID) {
        this.SourceTypeID = SourceTypeID;
    }

}
