
package com.myinc.xmltojson.output;

import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "TransationDetails"
})
public class TransactionInfo {

    @JsonProperty("TransationDetails")
    private com.myinc.xmltojson.output.TransationDetails TransationDetails;

    /**
     * 
     * @return
     *     The TransationDetails
     */
    @JsonProperty("TransationDetails")
    public com.myinc.xmltojson.output.TransationDetails getTransationDetails() {
        return TransationDetails;
    }

    /**
     * 
     * @param TransationDetails
     *     The TransationDetails
     */
    @JsonProperty("TransationDetails")
    public void setTransationDetails(com.myinc.xmltojson.output.TransationDetails TransationDetails) {
        this.TransationDetails = TransationDetails;
    }

}
