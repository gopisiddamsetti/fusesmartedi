
package com.myinc.xmltojson.output;

import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "element"
})
public class TransationDetails_ {

    @JsonProperty("element")
    private Element___ element;

    /**
     * 
     * @return
     *     The element
     */
    @JsonProperty("element")
    public Element___ getElement() {
        return element;
    }

    /**
     * 
     * @param element
     *     The element
     */
    @JsonProperty("element")
    public void setElement(Element___ element) {
        this.element = element;
    }

}
