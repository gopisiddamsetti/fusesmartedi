
package generated_1603090170802;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TransactionSetHeader">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="TransactionSetIdCode" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *                   &lt;element name="TransactionSetControlNumber" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="BeginningSegmentForInvoice">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;choice maxOccurs="unbounded" minOccurs="0">
 *                   &lt;element name="Date" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="InvoiceNumber">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;enumeration value="INV-1234567890"/>
 *                         &lt;enumeration value="20090901"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="PurchaseOrderNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ReleaseNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ChangeOrderSequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="TransactionTypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="TransactionSetPurposeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ActionCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/choice>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ReferenceId" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ReferenceIdQual">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;enumeration value="ZA"/>
 *                         &lt;enumeration value="PO"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="ReferenceId">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;enumeration value="AMZ"/>
 *                         &lt;enumeration value="4100ABC12300"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Name">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="NameIn">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;choice maxOccurs="unbounded" minOccurs="0">
 *                             &lt;element name="EntityIdCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IdCodeQual" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IdCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="EntityRelationshipCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/choice>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="AddrInfo">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="AddrInfo" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="100 Westwood"/>
 *                                   &lt;enumeration value=""/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="GeographicLocation">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="CityName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="StateOrProvinceCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                             &lt;element name="CountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LocationQual" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LocationId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="DateTimeReference" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="DateTimeQual">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}short">
 *                         &lt;enumeration value="205"/>
 *                         &lt;enumeration value="012"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="Date">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *                         &lt;enumeration value="20090721"/>
 *                         &lt;enumeration value="20090901"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="Time" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="TimeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="DateTimePeriodFormatQual" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="DateTimePeriod" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="BaselineItemData" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="BaselineItemDataIn">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;choice maxOccurs="unbounded" minOccurs="0">
 *                             &lt;element name="AssignedId">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
 *                                   &lt;enumeration value="1"/>
 *                                   &lt;enumeration value="2"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="QuantityInvoiced">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="2"/>
 *                                   &lt;enumeration value=""/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="UnitOrBasisForMeasurementCode">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="EA"/>
 *                                   &lt;enumeration value=""/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="UnitPrice">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="50.00"/>
 *                                   &lt;enumeration value=""/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="BasisOfUnitPriceCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ProductServiceIdQual" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ProductServiceId">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="1"/>
 *                                   &lt;enumeration value=""/>
 *                                   &lt;enumeration value="2"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                           &lt;/choice>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="TaxInfo">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="TaxTypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="MonetaryAmount">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}float">
 *                                   &lt;enumeration value="100.00"/>
 *                                   &lt;enumeration value="10.00"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="Percent">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="9.75"/>
 *                                   &lt;enumeration value=""/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="TaxJurisdictionCodeQual" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TaxJurisdictionCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TaxExemptCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="RelationshipCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DollarBasisForPercent" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TaxIdNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AssignedId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="TotalMonetaryValueSummary">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Amount" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;enumeration value="99.75"/>
 *                         &lt;enumeration value=""/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="TaxInfo">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="TaxTypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="MonetaryAmount" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *                   &lt;element name="Percent" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="TaxJurisdictionCodeQual" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="TaxJurisdictionCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="TaxExemptCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="RelationshipCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="DollarBasisForPercent" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="TaxIdNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="AssignedId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="MonetaryAmount">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="AmountQualCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="MonetaryAmount" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *                   &lt;element name="CreditDebitFlagCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "transactionSetHeader",
    "beginningSegmentForInvoice",
    "referenceId",
    "name",
    "dateTimeReference",
    "baselineItemData",
    "totalMonetaryValueSummary",
    "taxInfo",
    "monetaryAmount"
})
@XmlRootElement(name = "Invoice")
public class Invoice {

    @Override
	public String toString() {
		return "Invoice [transactionSetHeader=" + transactionSetHeader + ", beginningSegmentForInvoice="
				+ beginningSegmentForInvoice + ", referenceId=" + referenceId + ", name=" + name
				+ ", dateTimeReference=" + dateTimeReference + ", baselineItemData=" + baselineItemData
				+ ", totalMonetaryValueSummary=" + totalMonetaryValueSummary + ", taxInfo=" + taxInfo
				+ ", monetaryAmount=" + monetaryAmount + "]";
	}


	@XmlElement(name = "TransactionSetHeader", required = true)
    protected Invoice.TransactionSetHeader transactionSetHeader;
    @XmlElement(name = "BeginningSegmentForInvoice", required = true)
    protected Invoice.BeginningSegmentForInvoice beginningSegmentForInvoice;
    @XmlElement(name = "ReferenceId")
    protected List<Invoice.ReferenceId> referenceId;
    @XmlElement(name = "Name", required = true)
    protected Invoice.Name name;
    @XmlElement(name = "DateTimeReference")
    protected List<Invoice.DateTimeReference> dateTimeReference;
    @XmlElement(name = "BaselineItemData")
    protected List<Invoice.BaselineItemData> baselineItemData;
    @XmlElement(name = "TotalMonetaryValueSummary", required = true)
    protected Invoice.TotalMonetaryValueSummary totalMonetaryValueSummary;
    @XmlElement(name = "TaxInfo", required = true)
    protected Invoice.TaxInfo taxInfo;
    @XmlElement(name = "MonetaryAmount", required = true)
    protected Invoice.MonetaryAmount monetaryAmount;

    /**
     * Gets the value of the transactionSetHeader property.
     * 
     * @return
     *     possible object is
     *     {@link Invoice.TransactionSetHeader }
     *     
     */
    public Invoice.TransactionSetHeader getTransactionSetHeader() {
        return transactionSetHeader;
    }

    /**
     * Sets the value of the transactionSetHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link Invoice.TransactionSetHeader }
     *     
     */
    public void setTransactionSetHeader(Invoice.TransactionSetHeader value) {
        this.transactionSetHeader = value;
    }

    /**
     * Gets the value of the beginningSegmentForInvoice property.
     * 
     * @return
     *     possible object is
     *     {@link Invoice.BeginningSegmentForInvoice }
     *     
     */
    public Invoice.BeginningSegmentForInvoice getBeginningSegmentForInvoice() {
        return beginningSegmentForInvoice;
    }

    /**
     * Sets the value of the beginningSegmentForInvoice property.
     * 
     * @param value
     *     allowed object is
     *     {@link Invoice.BeginningSegmentForInvoice }
     *     
     */
    public void setBeginningSegmentForInvoice(Invoice.BeginningSegmentForInvoice value) {
        this.beginningSegmentForInvoice = value;
    }

    /**
     * Gets the value of the referenceId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the referenceId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReferenceId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Invoice.ReferenceId }
     * 
     * 
     */
    public List<Invoice.ReferenceId> getReferenceId() {
        if (referenceId == null) {
            referenceId = new ArrayList<Invoice.ReferenceId>();
        }
        return this.referenceId;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link Invoice.Name }
     *     
     */
    public Invoice.Name getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link Invoice.Name }
     *     
     */
    public void setName(Invoice.Name value) {
        this.name = value;
    }

    /**
     * Gets the value of the dateTimeReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dateTimeReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDateTimeReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Invoice.DateTimeReference }
     * 
     * 
     */
    public List<Invoice.DateTimeReference> getDateTimeReference() {
        if (dateTimeReference == null) {
            dateTimeReference = new ArrayList<Invoice.DateTimeReference>();
        }
        return this.dateTimeReference;
    }

    /**
     * Gets the value of the baselineItemData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the baselineItemData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBaselineItemData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Invoice.BaselineItemData }
     * 
     * 
     */
    public List<Invoice.BaselineItemData> getBaselineItemData() {
        if (baselineItemData == null) {
            baselineItemData = new ArrayList<Invoice.BaselineItemData>();
        }
        return this.baselineItemData;
    }

    /**
     * Gets the value of the totalMonetaryValueSummary property.
     * 
     * @return
     *     possible object is
     *     {@link Invoice.TotalMonetaryValueSummary }
     *     
     */
    public Invoice.TotalMonetaryValueSummary getTotalMonetaryValueSummary() {
        return totalMonetaryValueSummary;
    }

    /**
     * Sets the value of the totalMonetaryValueSummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link Invoice.TotalMonetaryValueSummary }
     *     
     */
    public void setTotalMonetaryValueSummary(Invoice.TotalMonetaryValueSummary value) {
        this.totalMonetaryValueSummary = value;
    }

    /**
     * Gets the value of the taxInfo property.
     * 
     * @return
     *     possible object is
     *     {@link Invoice.TaxInfo }
     *     
     */
    public Invoice.TaxInfo getTaxInfo() {
        return taxInfo;
    }

    /**
     * Sets the value of the taxInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Invoice.TaxInfo }
     *     
     */
    public void setTaxInfo(Invoice.TaxInfo value) {
        this.taxInfo = value;
    }

    /**
     * Gets the value of the monetaryAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Invoice.MonetaryAmount }
     *     
     */
    public Invoice.MonetaryAmount getMonetaryAmount() {
        return monetaryAmount;
    }

    /**
     * Sets the value of the monetaryAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Invoice.MonetaryAmount }
     *     
     */
    public void setMonetaryAmount(Invoice.MonetaryAmount value) {
        this.monetaryAmount = value;
    }

    public void setDateTimeReference(List<Invoice.DateTimeReference> dateTimeReference) {
        this.dateTimeReference = dateTimeReference;
    }

    public void setReferenceId(List<Invoice.ReferenceId> referenceId) {
        this.referenceId = referenceId;
    }

    public void setBaselineItemData(List<Invoice.BaselineItemData> baselineItemData) {
        this.baselineItemData = baselineItemData;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="BaselineItemDataIn">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;choice maxOccurs="unbounded" minOccurs="0">
     *                   &lt;element name="AssignedId">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
     *                         &lt;enumeration value="1"/>
     *                         &lt;enumeration value="2"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="QuantityInvoiced">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="2"/>
     *                         &lt;enumeration value=""/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="UnitOrBasisForMeasurementCode">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="EA"/>
     *                         &lt;enumeration value=""/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="UnitPrice">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="50.00"/>
     *                         &lt;enumeration value=""/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="BasisOfUnitPriceCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ProductServiceIdQual" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ProductServiceId">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="1"/>
     *                         &lt;enumeration value=""/>
     *                         &lt;enumeration value="2"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                 &lt;/choice>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="TaxInfo">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="TaxTypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="MonetaryAmount">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}float">
     *                         &lt;enumeration value="100.00"/>
     *                         &lt;enumeration value="10.00"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="Percent">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="9.75"/>
     *                         &lt;enumeration value=""/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="TaxJurisdictionCodeQual" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TaxJurisdictionCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TaxExemptCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="RelationshipCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DollarBasisForPercent" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TaxIdNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AssignedId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "baselineItemDataIn",
        "taxInfo"
    })
    public static class BaselineItemData {

        @Override
		public String toString() {
			return "BaselineItemData [baselineItemDataIn=" + baselineItemDataIn + ", taxInfo=" + taxInfo + "]";
		}


		@XmlElement(name = "BaselineItemDataIn", required = true)
        protected Invoice.BaselineItemData.BaselineItemDataIn baselineItemDataIn;
        @XmlElement(name = "TaxInfo", required = true)
        protected Invoice.BaselineItemData.TaxInfo taxInfo;

        /**
         * Gets the value of the baselineItemDataIn property.
         * 
         * @return
         *     possible object is
         *     {@link Invoice.BaselineItemData.BaselineItemDataIn }
         *     
         */
        public Invoice.BaselineItemData.BaselineItemDataIn getBaselineItemDataIn() {
            return baselineItemDataIn;
        }

        /**
         * Sets the value of the baselineItemDataIn property.
         * 
         * @param value
         *     allowed object is
         *     {@link Invoice.BaselineItemData.BaselineItemDataIn }
         *     
         */
        public void setBaselineItemDataIn(Invoice.BaselineItemData.BaselineItemDataIn value) {
            this.baselineItemDataIn = value;
        }

        /**
         * Gets the value of the taxInfo property.
         * 
         * @return
         *     possible object is
         *     {@link Invoice.BaselineItemData.TaxInfo }
         *     
         */
        public Invoice.BaselineItemData.TaxInfo getTaxInfo() {
            return taxInfo;
        }

        /**
         * Sets the value of the taxInfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link Invoice.BaselineItemData.TaxInfo }
         *     
         */
        public void setTaxInfo(Invoice.BaselineItemData.TaxInfo value) {
            this.taxInfo = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;choice maxOccurs="unbounded" minOccurs="0">
         *         &lt;element name="AssignedId">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}byte">
         *               &lt;enumeration value="1"/>
         *               &lt;enumeration value="2"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="QuantityInvoiced">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="2"/>
         *               &lt;enumeration value=""/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="UnitOrBasisForMeasurementCode">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="EA"/>
         *               &lt;enumeration value=""/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="UnitPrice">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="50.00"/>
         *               &lt;enumeration value=""/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="BasisOfUnitPriceCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ProductServiceIdQual" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ProductServiceId">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="1"/>
         *               &lt;enumeration value=""/>
         *               &lt;enumeration value="2"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *       &lt;/choice>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "assignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode"
        })
        public static class BaselineItemDataIn {

            @Override
			public String toString() {
				return "BaselineItemDataIn [assignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode="
						+ assignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode + "]";
			}

			@XmlElementRefs({
                @XmlElementRef(name = "UnitOrBasisForMeasurementCode", type = JAXBElement.class, required = false),
                @XmlElementRef(name = "UnitPrice", type = JAXBElement.class, required = false),
                @XmlElementRef(name = "ProductServiceId", type = JAXBElement.class, required = false),
                @XmlElementRef(name = "AssignedId", type = JAXBElement.class, required = false),
                @XmlElementRef(name = "QuantityInvoiced", type = JAXBElement.class, required = false),
                @XmlElementRef(name = "ProductServiceIdQual", type = JAXBElement.class, required = false),
                @XmlElementRef(name = "BasisOfUnitPriceCode", type = JAXBElement.class, required = false)
            })
            protected List<JAXBElement<? extends Serializable>> assignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode;

            /**
             * Gets the value of the assignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the assignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getAssignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link JAXBElement }{@code <}{@link String }{@code >}
             * {@link JAXBElement }{@code <}{@link String }{@code >}
             * {@link JAXBElement }{@code <}{@link String }{@code >}
             * {@link JAXBElement }{@code <}{@link Byte }{@code >}
             * {@link JAXBElement }{@code <}{@link String }{@code >}
             * {@link JAXBElement }{@code <}{@link String }{@code >}
             * {@link JAXBElement }{@code <}{@link String }{@code >}
             * 
             * 
             */
            public List<JAXBElement<? extends Serializable>> getAssignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode() {
                if (assignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode == null) {
                    assignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode = new ArrayList<JAXBElement<? extends Serializable>>();
                }
                return this.assignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode;
            }

            public void setAssignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode(List<JAXBElement<? extends Serializable>> assignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode) {
                this.assignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode = assignedIdOrQuantityInvoicedOrUnitOrBasisForMeasurementCode;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="TaxTypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="MonetaryAmount">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}float">
         *               &lt;enumeration value="100.00"/>
         *               &lt;enumeration value="10.00"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="Percent">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="9.75"/>
         *               &lt;enumeration value=""/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="TaxJurisdictionCodeQual" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TaxJurisdictionCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TaxExemptCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="RelationshipCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DollarBasisForPercent" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TaxIdNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AssignedId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "taxTypeCode",
            "monetaryAmount",
            "percent",
            "taxJurisdictionCodeQual",
            "taxJurisdictionCode",
            "taxExemptCode",
            "relationshipCode",
            "dollarBasisForPercent",
            "taxIdNumber",
            "assignedId"
        })
        public static class TaxInfo {

            @Override
			public String toString() {
				return "TaxInfo [taxTypeCode=" + taxTypeCode + ", monetaryAmount=" + monetaryAmount + ", percent="
						+ percent + ", taxJurisdictionCodeQual=" + taxJurisdictionCodeQual + ", taxJurisdictionCode="
						+ taxJurisdictionCode + ", taxExemptCode=" + taxExemptCode + ", relationshipCode="
						+ relationshipCode + ", dollarBasisForPercent=" + dollarBasisForPercent + ", taxIdNumber="
						+ taxIdNumber + ", assignedId=" + assignedId + "]";
			}

			@XmlElement(name = "TaxTypeCode", required = true)
            protected String taxTypeCode;
            @XmlElement(name = "MonetaryAmount")
            protected float monetaryAmount;
            @XmlElement(name = "Percent", required = true)
            protected String percent;
            @XmlElement(name = "TaxJurisdictionCodeQual", required = true)
            protected String taxJurisdictionCodeQual;
            @XmlElement(name = "TaxJurisdictionCode", required = true)
            protected String taxJurisdictionCode;
            @XmlElement(name = "TaxExemptCode", required = true)
            protected String taxExemptCode;
            @XmlElement(name = "RelationshipCode", required = true)
            protected String relationshipCode;
            @XmlElement(name = "DollarBasisForPercent", required = true)
            protected String dollarBasisForPercent;
            @XmlElement(name = "TaxIdNumber", required = true)
            protected String taxIdNumber;
            @XmlElement(name = "AssignedId", required = true)
            protected String assignedId;

            /**
             * Gets the value of the taxTypeCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTaxTypeCode() {
                return taxTypeCode;
            }

            /**
             * Sets the value of the taxTypeCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTaxTypeCode(String value) {
                this.taxTypeCode = value;
            }

            /**
             * Gets the value of the monetaryAmount property.
             * 
             */
            public float getMonetaryAmount() {
                return monetaryAmount;
            }

            /**
             * Sets the value of the monetaryAmount property.
             * 
             */
            public void setMonetaryAmount(float value) {
                this.monetaryAmount = value;
            }

            /**
             * Gets the value of the percent property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPercent() {
                return percent;
            }

            /**
             * Sets the value of the percent property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPercent(String value) {
                this.percent = value;
            }

            /**
             * Gets the value of the taxJurisdictionCodeQual property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTaxJurisdictionCodeQual() {
                return taxJurisdictionCodeQual;
            }

            /**
             * Sets the value of the taxJurisdictionCodeQual property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTaxJurisdictionCodeQual(String value) {
                this.taxJurisdictionCodeQual = value;
            }

            /**
             * Gets the value of the taxJurisdictionCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTaxJurisdictionCode() {
                return taxJurisdictionCode;
            }

            /**
             * Sets the value of the taxJurisdictionCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTaxJurisdictionCode(String value) {
                this.taxJurisdictionCode = value;
            }

            /**
             * Gets the value of the taxExemptCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTaxExemptCode() {
                return taxExemptCode;
            }

            /**
             * Sets the value of the taxExemptCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTaxExemptCode(String value) {
                this.taxExemptCode = value;
            }

            /**
             * Gets the value of the relationshipCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRelationshipCode() {
                return relationshipCode;
            }

            /**
             * Sets the value of the relationshipCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRelationshipCode(String value) {
                this.relationshipCode = value;
            }

            /**
             * Gets the value of the dollarBasisForPercent property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDollarBasisForPercent() {
                return dollarBasisForPercent;
            }

            /**
             * Sets the value of the dollarBasisForPercent property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDollarBasisForPercent(String value) {
                this.dollarBasisForPercent = value;
            }

            /**
             * Gets the value of the taxIdNumber property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTaxIdNumber() {
                return taxIdNumber;
            }

            /**
             * Sets the value of the taxIdNumber property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTaxIdNumber(String value) {
                this.taxIdNumber = value;
            }

            /**
             * Gets the value of the assignedId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAssignedId() {
                return assignedId;
            }

            /**
             * Sets the value of the assignedId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAssignedId(String value) {
                this.assignedId = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;choice maxOccurs="unbounded" minOccurs="0">
     *         &lt;element name="Date" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="InvoiceNumber">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;enumeration value="INV-1234567890"/>
     *               &lt;enumeration value="20090901"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="PurchaseOrderNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ReleaseNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ChangeOrderSequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="TransactionTypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="TransactionSetPurposeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ActionCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/choice>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "dateOrInvoiceNumberOrPurchaseOrderNumber"
    })
    public static class BeginningSegmentForInvoice {

        @Override
		public String toString() {
			return "BeginningSegmentForInvoice [dateOrInvoiceNumberOrPurchaseOrderNumber="
					+ dateOrInvoiceNumberOrPurchaseOrderNumber + "]";
		}

		@XmlElementRefs({
            @XmlElementRef(name = "ActionCode", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "TransactionTypeCode", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "PurchaseOrderNumber", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "ReleaseNumber", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "Date", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "InvoiceNumber", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "TransactionSetPurposeCode", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "ChangeOrderSequenceNumber", type = JAXBElement.class, required = false)
        })
        protected List<JAXBElement<? extends Serializable>> dateOrInvoiceNumberOrPurchaseOrderNumber;

        /**
         * Gets the value of the dateOrInvoiceNumberOrPurchaseOrderNumber property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the dateOrInvoiceNumberOrPurchaseOrderNumber property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDateOrInvoiceNumberOrPurchaseOrderNumber().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link JAXBElement }{@code <}{@link String }{@code >}
         * {@link JAXBElement }{@code <}{@link String }{@code >}
         * {@link JAXBElement }{@code <}{@link String }{@code >}
         * {@link JAXBElement }{@code <}{@link String }{@code >}
         * {@link JAXBElement }{@code <}{@link Integer }{@code >}
         * {@link JAXBElement }{@code <}{@link String }{@code >}
         * {@link JAXBElement }{@code <}{@link String }{@code >}
         * {@link JAXBElement }{@code <}{@link String }{@code >}
         * 
         * 
         */
        public List<JAXBElement<? extends Serializable>> getDateOrInvoiceNumberOrPurchaseOrderNumber() {
            if (dateOrInvoiceNumberOrPurchaseOrderNumber == null) {
                dateOrInvoiceNumberOrPurchaseOrderNumber = new ArrayList<JAXBElement<? extends Serializable>>();
            }
            return this.dateOrInvoiceNumberOrPurchaseOrderNumber;
        }

        public void setDateOrInvoiceNumberOrPurchaseOrderNumber(List<JAXBElement<? extends Serializable>> dateOrInvoiceNumberOrPurchaseOrderNumber) {
            this.dateOrInvoiceNumberOrPurchaseOrderNumber = dateOrInvoiceNumberOrPurchaseOrderNumber;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="DateTimeQual">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}short">
     *               &lt;enumeration value="205"/>
     *               &lt;enumeration value="012"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="Date">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
     *               &lt;enumeration value="20090721"/>
     *               &lt;enumeration value="20090901"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="Time" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="TimeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="DateTimePeriodFormatQual" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="DateTimePeriod" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "dateTimeQual",
        "date",
        "time",
        "timeCode",
        "dateTimePeriodFormatQual",
        "dateTimePeriod"
    })
    public static class DateTimeReference {

        @Override
		public String toString() {
			return "DateTimeReference [dateTimeQual=" + dateTimeQual + ", date=" + date + ", time=" + time
					+ ", timeCode=" + timeCode + ", dateTimePeriodFormatQual=" + dateTimePeriodFormatQual
					+ ", dateTimePeriod=" + dateTimePeriod + "]";
		}

		@XmlElement(name = "DateTimeQual")
        protected short dateTimeQual;
        @XmlElement(name = "Date")
        protected int date;
        @XmlElement(name = "Time", required = true)
        protected String time;
        @XmlElement(name = "TimeCode", required = true)
        protected String timeCode;
        @XmlElement(name = "DateTimePeriodFormatQual", required = true)
        protected String dateTimePeriodFormatQual;
        @XmlElement(name = "DateTimePeriod", required = true)
        protected String dateTimePeriod;

        /**
         * Gets the value of the dateTimeQual property.
         * 
         */
        public short getDateTimeQual() {
            return dateTimeQual;
        }

        /**
         * Sets the value of the dateTimeQual property.
         * 
         */
        public void setDateTimeQual(short value) {
            this.dateTimeQual = value;
        }

        /**
         * Gets the value of the date property.
         * 
         */
        public int getDate() {
            return date;
        }

        /**
         * Sets the value of the date property.
         * 
         */
        public void setDate(int value) {
            this.date = value;
        }

        /**
         * Gets the value of the time property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTime() {
            return time;
        }

        /**
         * Sets the value of the time property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTime(String value) {
            this.time = value;
        }

        /**
         * Gets the value of the timeCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTimeCode() {
            return timeCode;
        }

        /**
         * Sets the value of the timeCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTimeCode(String value) {
            this.timeCode = value;
        }

        /**
         * Gets the value of the dateTimePeriodFormatQual property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDateTimePeriodFormatQual() {
            return dateTimePeriodFormatQual;
        }

        /**
         * Sets the value of the dateTimePeriodFormatQual property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDateTimePeriodFormatQual(String value) {
            this.dateTimePeriodFormatQual = value;
        }

        /**
         * Gets the value of the dateTimePeriod property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDateTimePeriod() {
            return dateTimePeriod;
        }

        /**
         * Sets the value of the dateTimePeriod property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDateTimePeriod(String value) {
            this.dateTimePeriod = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="AmountQualCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="MonetaryAmount" type="{http://www.w3.org/2001/XMLSchema}float"/>
     *         &lt;element name="CreditDebitFlagCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "amountQualCode",
        "monetaryAmount",
        "creditDebitFlagCode"
    })
    public static class MonetaryAmount {

        @Override
		public String toString() {
			return "MonetaryAmount [amountQualCode=" + amountQualCode + ", monetaryAmount=" + monetaryAmount
					+ ", creditDebitFlagCode=" + creditDebitFlagCode + "]";
		}

		@XmlElement(name = "AmountQualCode", required = true)
        protected String amountQualCode;
        @XmlElement(name = "MonetaryAmount")
        protected float monetaryAmount;
        @XmlElement(name = "CreditDebitFlagCode", required = true)
        protected String creditDebitFlagCode;

        /**
         * Gets the value of the amountQualCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAmountQualCode() {
            return amountQualCode;
        }

        /**
         * Sets the value of the amountQualCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAmountQualCode(String value) {
            this.amountQualCode = value;
        }

        /**
         * Gets the value of the monetaryAmount property.
         * 
         */
        public float getMonetaryAmount() {
            return monetaryAmount;
        }

        /**
         * Sets the value of the monetaryAmount property.
         * 
         */
        public void setMonetaryAmount(float value) {
            this.monetaryAmount = value;
        }

        /**
         * Gets the value of the creditDebitFlagCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCreditDebitFlagCode() {
            return creditDebitFlagCode;
        }

        /**
         * Sets the value of the creditDebitFlagCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCreditDebitFlagCode(String value) {
            this.creditDebitFlagCode = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="NameIn">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;choice maxOccurs="unbounded" minOccurs="0">
     *                   &lt;element name="EntityIdCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IdCodeQual" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IdCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="EntityRelationshipCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/choice>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="AddrInfo">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="AddrInfo" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="100 Westwood"/>
     *                         &lt;enumeration value=""/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="GeographicLocation">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="CityName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="StateOrProvinceCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="CountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LocationQual" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LocationId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "nameIn",
        "addrInfo",
        "geographicLocation"
    })
    public static class Name {

        @Override
		public String toString() {
			return "Name [nameIn=" + nameIn + ", addrInfo=" + addrInfo + ", geographicLocation=" + geographicLocation
					+ "]";
		}


		@XmlElement(name = "NameIn", required = true)
        protected Invoice.Name.NameIn nameIn;
        @XmlElement(name = "AddrInfo", required = true)
        protected Invoice.Name.AddrInfo addrInfo;
        @XmlElement(name = "GeographicLocation", required = true)
        protected Invoice.Name.GeographicLocation geographicLocation;

        /**
         * Gets the value of the nameIn property.
         * 
         * @return
         *     possible object is
         *     {@link Invoice.Name.NameIn }
         *     
         */
        public Invoice.Name.NameIn getNameIn() {
            return nameIn;
        }

        /**
         * Sets the value of the nameIn property.
         * 
         * @param value
         *     allowed object is
         *     {@link Invoice.Name.NameIn }
         *     
         */
        public void setNameIn(Invoice.Name.NameIn value) {
            this.nameIn = value;
        }

        /**
         * Gets the value of the addrInfo property.
         * 
         * @return
         *     possible object is
         *     {@link Invoice.Name.AddrInfo }
         *     
         */
        public Invoice.Name.AddrInfo getAddrInfo() {
            return addrInfo;
        }

        /**
         * Sets the value of the addrInfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link Invoice.Name.AddrInfo }
         *     
         */
        public void setAddrInfo(Invoice.Name.AddrInfo value) {
            this.addrInfo = value;
        }

        /**
         * Gets the value of the geographicLocation property.
         * 
         * @return
         *     possible object is
         *     {@link Invoice.Name.GeographicLocation }
         *     
         */
        public Invoice.Name.GeographicLocation getGeographicLocation() {
            return geographicLocation;
        }

        /**
         * Sets the value of the geographicLocation property.
         * 
         * @param value
         *     allowed object is
         *     {@link Invoice.Name.GeographicLocation }
         *     
         */
        public void setGeographicLocation(Invoice.Name.GeographicLocation value) {
            this.geographicLocation = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="AddrInfo" maxOccurs="unbounded" minOccurs="0">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="100 Westwood"/>
         *               &lt;enumeration value=""/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "addrInfo"
        })
        public static class AddrInfo {

            @Override
			public String toString() {
				return "AddrInfo [addrInfo=" + addrInfo + "]";
			}

			@XmlElement(name = "AddrInfo")
            protected List<String> addrInfo;

            /**
             * Gets the value of the addrInfo property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the addrInfo property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getAddrInfo().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getAddrInfo() {
                if (addrInfo == null) {
                    addrInfo = new ArrayList<String>();
                }
                return this.addrInfo;
            }

            public void setAddrInfo(List<String> addrInfo) {
                this.addrInfo = addrInfo;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="CityName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="StateOrProvinceCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="CountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LocationQual" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LocationId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "cityName",
            "stateOrProvinceCode",
            "postalCode",
            "countryCode",
            "locationQual",
            "locationId"
        })
        public static class GeographicLocation {

            @Override
			public String toString() {
				return "GeographicLocation [cityName=" + cityName + ", stateOrProvinceCode=" + stateOrProvinceCode
						+ ", postalCode=" + postalCode + ", countryCode=" + countryCode + ", locationQual="
						+ locationQual + ", locationId=" + locationId + "]";
			}

			@XmlElement(name = "CityName", required = true)
            protected String cityName;
            @XmlElement(name = "StateOrProvinceCode", required = true)
            protected String stateOrProvinceCode;
            @XmlElement(name = "PostalCode")
            protected int postalCode;
            @XmlElement(name = "CountryCode", required = true)
            protected String countryCode;
            @XmlElement(name = "LocationQual", required = true)
            protected String locationQual;
            @XmlElement(name = "LocationId", required = true)
            protected String locationId;

            /**
             * Gets the value of the cityName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCityName() {
                return cityName;
            }

            /**
             * Sets the value of the cityName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCityName(String value) {
                this.cityName = value;
            }

            /**
             * Gets the value of the stateOrProvinceCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStateOrProvinceCode() {
                return stateOrProvinceCode;
            }

            /**
             * Sets the value of the stateOrProvinceCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStateOrProvinceCode(String value) {
                this.stateOrProvinceCode = value;
            }

            /**
             * Gets the value of the postalCode property.
             * 
             */
            public int getPostalCode() {
                return postalCode;
            }

            /**
             * Sets the value of the postalCode property.
             * 
             */
            public void setPostalCode(int value) {
                this.postalCode = value;
            }

            /**
             * Gets the value of the countryCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCountryCode() {
                return countryCode;
            }

            /**
             * Sets the value of the countryCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCountryCode(String value) {
                this.countryCode = value;
            }

            /**
             * Gets the value of the locationQual property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLocationQual() {
                return locationQual;
            }

            /**
             * Sets the value of the locationQual property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLocationQual(String value) {
                this.locationQual = value;
            }

            /**
             * Gets the value of the locationId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLocationId() {
                return locationId;
            }

            /**
             * Sets the value of the locationId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLocationId(String value) {
                this.locationId = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;choice maxOccurs="unbounded" minOccurs="0">
         *         &lt;element name="EntityIdCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IdCodeQual" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IdCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="EntityRelationshipCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/choice>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "entityIdCodeOrNameOrIdCodeQual"
        })
        public static class NameIn {

            @Override
			public String toString() {
				return "NameIn [entityIdCodeOrNameOrIdCodeQual=" + entityIdCodeOrNameOrIdCodeQual + "]";
			}

			@XmlElementRefs({
                @XmlElementRef(name = "IdCode", type = JAXBElement.class, required = false),
                @XmlElementRef(name = "Name", type = JAXBElement.class, required = false),
                @XmlElementRef(name = "EntityIdCode", type = JAXBElement.class, required = false),
                @XmlElementRef(name = "IdCodeQual", type = JAXBElement.class, required = false),
                @XmlElementRef(name = "EntityRelationshipCode", type = JAXBElement.class, required = false)
            })
            protected List<JAXBElement<String>> entityIdCodeOrNameOrIdCodeQual;

            /**
             * Gets the value of the entityIdCodeOrNameOrIdCodeQual property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the entityIdCodeOrNameOrIdCodeQual property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getEntityIdCodeOrNameOrIdCodeQual().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link JAXBElement }{@code <}{@link String }{@code >}
             * {@link JAXBElement }{@code <}{@link String }{@code >}
             * {@link JAXBElement }{@code <}{@link String }{@code >}
             * {@link JAXBElement }{@code <}{@link String }{@code >}
             * {@link JAXBElement }{@code <}{@link String }{@code >}
             * 
             * 
             */
            public List<JAXBElement<String>> getEntityIdCodeOrNameOrIdCodeQual() {
                if (entityIdCodeOrNameOrIdCodeQual == null) {
                    entityIdCodeOrNameOrIdCodeQual = new ArrayList<JAXBElement<String>>();
                }
                return this.entityIdCodeOrNameOrIdCodeQual;
            }

            public void setEntityIdCodeOrNameOrIdCodeQual(List<JAXBElement<String>> entityIdCodeOrNameOrIdCodeQual) {
                this.entityIdCodeOrNameOrIdCodeQual = entityIdCodeOrNameOrIdCodeQual;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ReferenceIdQual">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;enumeration value="ZA"/>
     *               &lt;enumeration value="PO"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="ReferenceId">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;enumeration value="AMZ"/>
     *               &lt;enumeration value="4100ABC12300"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "referenceIdQual",
        "referenceId",
        "description"
    })
    public static class ReferenceId {

        @Override
		public String toString() {
			return "ReferenceId [referenceIdQual=" + referenceIdQual + ", referenceId=" + referenceId + ", description="
					+ description + "]";
		}

		@XmlElement(name = "ReferenceIdQual", required = true)
        protected String referenceIdQual;
        @XmlElement(name = "ReferenceId", required = true)
        protected String referenceId;
        @XmlElement(name = "Description", required = true)
        protected String description;

        /**
         * Gets the value of the referenceIdQual property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReferenceIdQual() {
            return referenceIdQual;
        }

        /**
         * Sets the value of the referenceIdQual property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReferenceIdQual(String value) {
            this.referenceIdQual = value;
        }

        /**
         * Gets the value of the referenceId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReferenceId() {
            return referenceId;
        }

        /**
         * Sets the value of the referenceId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReferenceId(String value) {
            this.referenceId = value;
        }

        /**
         * Gets the value of the description property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDescription() {
            return description;
        }

        /**
         * Sets the value of the description property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDescription(String value) {
            this.description = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="TaxTypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="MonetaryAmount" type="{http://www.w3.org/2001/XMLSchema}float"/>
     *         &lt;element name="Percent" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="TaxJurisdictionCodeQual" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="TaxJurisdictionCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="TaxExemptCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="RelationshipCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="DollarBasisForPercent" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="TaxIdNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="AssignedId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "taxTypeCode",
        "monetaryAmount",
        "percent",
        "taxJurisdictionCodeQual",
        "taxJurisdictionCode",
        "taxExemptCode",
        "relationshipCode",
        "dollarBasisForPercent",
        "taxIdNumber",
        "assignedId"
    })
    public static class TaxInfo {

        @Override
		public String toString() {
			return "TaxInfo [taxTypeCode=" + taxTypeCode + ", monetaryAmount=" + monetaryAmount + ", percent=" + percent
					+ ", taxJurisdictionCodeQual=" + taxJurisdictionCodeQual + ", taxJurisdictionCode="
					+ taxJurisdictionCode + ", taxExemptCode=" + taxExemptCode + ", relationshipCode="
					+ relationshipCode + ", dollarBasisForPercent=" + dollarBasisForPercent + ", taxIdNumber="
					+ taxIdNumber + ", assignedId=" + assignedId + "]";
		}

		@XmlElement(name = "TaxTypeCode", required = true)
        protected String taxTypeCode;
        @XmlElement(name = "MonetaryAmount")
        protected float monetaryAmount;
        @XmlElement(name = "Percent", required = true)
        protected String percent;
        @XmlElement(name = "TaxJurisdictionCodeQual", required = true)
        protected String taxJurisdictionCodeQual;
        @XmlElement(name = "TaxJurisdictionCode", required = true)
        protected String taxJurisdictionCode;
        @XmlElement(name = "TaxExemptCode", required = true)
        protected String taxExemptCode;
        @XmlElement(name = "RelationshipCode", required = true)
        protected String relationshipCode;
        @XmlElement(name = "DollarBasisForPercent", required = true)
        protected String dollarBasisForPercent;
        @XmlElement(name = "TaxIdNumber", required = true)
        protected String taxIdNumber;
        @XmlElement(name = "AssignedId", required = true)
        protected String assignedId;

        /**
         * Gets the value of the taxTypeCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTaxTypeCode() {
            return taxTypeCode;
        }

        /**
         * Sets the value of the taxTypeCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTaxTypeCode(String value) {
            this.taxTypeCode = value;
        }

        /**
         * Gets the value of the monetaryAmount property.
         * 
         */
        public float getMonetaryAmount() {
            return monetaryAmount;
        }

        /**
         * Sets the value of the monetaryAmount property.
         * 
         */
        public void setMonetaryAmount(float value) {
            this.monetaryAmount = value;
        }

        /**
         * Gets the value of the percent property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPercent() {
            return percent;
        }

        /**
         * Sets the value of the percent property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPercent(String value) {
            this.percent = value;
        }

        /**
         * Gets the value of the taxJurisdictionCodeQual property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTaxJurisdictionCodeQual() {
            return taxJurisdictionCodeQual;
        }

        /**
         * Sets the value of the taxJurisdictionCodeQual property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTaxJurisdictionCodeQual(String value) {
            this.taxJurisdictionCodeQual = value;
        }

        /**
         * Gets the value of the taxJurisdictionCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTaxJurisdictionCode() {
            return taxJurisdictionCode;
        }

        /**
         * Sets the value of the taxJurisdictionCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTaxJurisdictionCode(String value) {
            this.taxJurisdictionCode = value;
        }

        /**
         * Gets the value of the taxExemptCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTaxExemptCode() {
            return taxExemptCode;
        }

        /**
         * Sets the value of the taxExemptCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTaxExemptCode(String value) {
            this.taxExemptCode = value;
        }

        /**
         * Gets the value of the relationshipCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRelationshipCode() {
            return relationshipCode;
        }

        /**
         * Sets the value of the relationshipCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRelationshipCode(String value) {
            this.relationshipCode = value;
        }

        /**
         * Gets the value of the dollarBasisForPercent property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDollarBasisForPercent() {
            return dollarBasisForPercent;
        }

        /**
         * Sets the value of the dollarBasisForPercent property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDollarBasisForPercent(String value) {
            this.dollarBasisForPercent = value;
        }

        /**
         * Gets the value of the taxIdNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTaxIdNumber() {
            return taxIdNumber;
        }

        /**
         * Sets the value of the taxIdNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTaxIdNumber(String value) {
            this.taxIdNumber = value;
        }

        /**
         * Gets the value of the assignedId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAssignedId() {
            return assignedId;
        }

        /**
         * Sets the value of the assignedId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAssignedId(String value) {
            this.assignedId = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Amount" maxOccurs="unbounded" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;enumeration value="99.75"/>
     *               &lt;enumeration value=""/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "amount"
    })
    public static class TotalMonetaryValueSummary {

        @Override
		public String toString() {
			return "TotalMonetaryValueSummary [amount=" + amount + "]";
		}

		@XmlElement(name = "Amount")
        protected List<String> amount;

        /**
         * Gets the value of the amount property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the amount property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAmount().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getAmount() {
            if (amount == null) {
                amount = new ArrayList<String>();
            }
            return this.amount;
        }

        public void setAmount(List<String> amount) {
            this.amount = amount;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="TransactionSetIdCode" type="{http://www.w3.org/2001/XMLSchema}short"/>
     *         &lt;element name="TransactionSetControlNumber" type="{http://www.w3.org/2001/XMLSchema}byte"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "transactionSetIdCode",
        "transactionSetControlNumber"
    })
    public static class TransactionSetHeader {

        @Override
		public String toString() {
			return "TransactionSetHeader [transactionSetIdCode=" + transactionSetIdCode
					+ ", transactionSetControlNumber=" + transactionSetControlNumber + "]";
		}

		@XmlElement(name = "TransactionSetIdCode")
        protected short transactionSetIdCode;
        @XmlElement(name = "TransactionSetControlNumber")
        protected byte transactionSetControlNumber;

        /**
         * Gets the value of the transactionSetIdCode property.
         * 
         */
        public short getTransactionSetIdCode() {
            return transactionSetIdCode;
        }

        /**
         * Sets the value of the transactionSetIdCode property.
         * 
         */
        public void setTransactionSetIdCode(short value) {
            this.transactionSetIdCode = value;
        }

        /**
         * Gets the value of the transactionSetControlNumber property.
         * 
         */
        public byte getTransactionSetControlNumber() {
            return transactionSetControlNumber;
        }

        /**
         * Sets the value of the transactionSetControlNumber property.
         * 
         */
        public void setTransactionSetControlNumber(byte value) {
            this.transactionSetControlNumber = value;
        }

    }

}
