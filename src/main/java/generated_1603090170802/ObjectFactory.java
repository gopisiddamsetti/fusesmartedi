
package generated_1603090170802;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the generated package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _InvoiceBaselineItemDataBaselineItemDataInAssignedId_QNAME = new QName("", "AssignedId");
    private final static QName _InvoiceBaselineItemDataBaselineItemDataInUnitPrice_QNAME = new QName("", "UnitPrice");
    private final static QName _InvoiceBaselineItemDataBaselineItemDataInBasisOfUnitPriceCode_QNAME = new QName("", "BasisOfUnitPriceCode");
    private final static QName _InvoiceBaselineItemDataBaselineItemDataInProductServiceIdQual_QNAME = new QName("", "ProductServiceIdQual");
    private final static QName _InvoiceBaselineItemDataBaselineItemDataInProductServiceId_QNAME = new QName("", "ProductServiceId");
    private final static QName _InvoiceBaselineItemDataBaselineItemDataInUnitOrBasisForMeasurementCode_QNAME = new QName("", "UnitOrBasisForMeasurementCode");
    private final static QName _InvoiceBaselineItemDataBaselineItemDataInQuantityInvoiced_QNAME = new QName("", "QuantityInvoiced");
    private final static QName _InvoiceBeginningSegmentForInvoicePurchaseOrderNumber_QNAME = new QName("", "PurchaseOrderNumber");
    private final static QName _InvoiceBeginningSegmentForInvoiceTransactionSetPurposeCode_QNAME = new QName("", "TransactionSetPurposeCode");
    private final static QName _InvoiceBeginningSegmentForInvoiceTransactionTypeCode_QNAME = new QName("", "TransactionTypeCode");
    private final static QName _InvoiceBeginningSegmentForInvoiceActionCode_QNAME = new QName("", "ActionCode");
    private final static QName _InvoiceBeginningSegmentForInvoiceInvoiceNumber_QNAME = new QName("", "InvoiceNumber");
    private final static QName _InvoiceBeginningSegmentForInvoiceReleaseNumber_QNAME = new QName("", "ReleaseNumber");
    private final static QName _InvoiceBeginningSegmentForInvoiceChangeOrderSequenceNumber_QNAME = new QName("", "ChangeOrderSequenceNumber");
    private final static QName _InvoiceBeginningSegmentForInvoiceDate_QNAME = new QName("", "Date");
    private final static QName _InvoiceNameNameInEntityIdCode_QNAME = new QName("", "EntityIdCode");
    private final static QName _InvoiceNameNameInIdCode_QNAME = new QName("", "IdCode");
    private final static QName _InvoiceNameNameInEntityRelationshipCode_QNAME = new QName("", "EntityRelationshipCode");
    private final static QName _InvoiceNameNameInIdCodeQual_QNAME = new QName("", "IdCodeQual");
    private final static QName _InvoiceNameNameInName_QNAME = new QName("", "Name");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Invoice }
     * 
     */
    public Invoice createInvoice() {
        return new Invoice();
    }

    /**
     * Create an instance of {@link Invoice.BaselineItemData }
     * 
     */
    public Invoice.BaselineItemData createInvoiceBaselineItemData() {
        return new Invoice.BaselineItemData();
    }

    /**
     * Create an instance of {@link Invoice.Name }
     * 
     */
    public Invoice.Name createInvoiceName() {
        return new Invoice.Name();
    }

    /**
     * Create an instance of {@link Invoice.TransactionSetHeader }
     * 
     */
    public Invoice.TransactionSetHeader createInvoiceTransactionSetHeader() {
        return new Invoice.TransactionSetHeader();
    }

    /**
     * Create an instance of {@link Invoice.BeginningSegmentForInvoice }
     * 
     */
    public Invoice.BeginningSegmentForInvoice createInvoiceBeginningSegmentForInvoice() {
        return new Invoice.BeginningSegmentForInvoice();
    }

    /**
     * Create an instance of {@link Invoice.ReferenceId }
     * 
     */
    public Invoice.ReferenceId createInvoiceReferenceId() {
        return new Invoice.ReferenceId();
    }

    /**
     * Create an instance of {@link Invoice.DateTimeReference }
     * 
     */
    public Invoice.DateTimeReference createInvoiceDateTimeReference() {
        return new Invoice.DateTimeReference();
    }

    /**
     * Create an instance of {@link Invoice.TotalMonetaryValueSummary }
     * 
     */
    public Invoice.TotalMonetaryValueSummary createInvoiceTotalMonetaryValueSummary() {
        return new Invoice.TotalMonetaryValueSummary();
    }

    /**
     * Create an instance of {@link Invoice.TaxInfo }
     * 
     */
    public Invoice.TaxInfo createInvoiceTaxInfo() {
        return new Invoice.TaxInfo();
    }

    /**
     * Create an instance of {@link Invoice.MonetaryAmount }
     * 
     */
    public Invoice.MonetaryAmount createInvoiceMonetaryAmount() {
        return new Invoice.MonetaryAmount();
    }

    /**
     * Create an instance of {@link Invoice.BaselineItemData.BaselineItemDataIn }
     * 
     */
    public Invoice.BaselineItemData.BaselineItemDataIn createInvoiceBaselineItemDataBaselineItemDataIn() {
        return new Invoice.BaselineItemData.BaselineItemDataIn();
    }

    /**
     * Create an instance of {@link Invoice.BaselineItemData.TaxInfo }
     * 
     */
    public Invoice.BaselineItemData.TaxInfo createInvoiceBaselineItemDataTaxInfo() {
        return new Invoice.BaselineItemData.TaxInfo();
    }

    /**
     * Create an instance of {@link Invoice.Name.NameIn }
     * 
     */
    public Invoice.Name.NameIn createInvoiceNameNameIn() {
        return new Invoice.Name.NameIn();
    }

    /**
     * Create an instance of {@link Invoice.Name.AddrInfo }
     * 
     */
    public Invoice.Name.AddrInfo createInvoiceNameAddrInfo() {
        return new Invoice.Name.AddrInfo();
    }

    /**
     * Create an instance of {@link Invoice.Name.GeographicLocation }
     * 
     */
    public Invoice.Name.GeographicLocation createInvoiceNameGeographicLocation() {
        return new Invoice.Name.GeographicLocation();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Byte }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "AssignedId", scope = Invoice.BaselineItemData.BaselineItemDataIn.class)
    public JAXBElement<Byte> createInvoiceBaselineItemDataBaselineItemDataInAssignedId(Byte value) {
        return new JAXBElement<Byte>(_InvoiceBaselineItemDataBaselineItemDataInAssignedId_QNAME, Byte.class, Invoice.BaselineItemData.BaselineItemDataIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "UnitPrice", scope = Invoice.BaselineItemData.BaselineItemDataIn.class)
    public JAXBElement<String> createInvoiceBaselineItemDataBaselineItemDataInUnitPrice(String value) {
        return new JAXBElement<String>(_InvoiceBaselineItemDataBaselineItemDataInUnitPrice_QNAME, String.class, Invoice.BaselineItemData.BaselineItemDataIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BasisOfUnitPriceCode", scope = Invoice.BaselineItemData.BaselineItemDataIn.class)
    public JAXBElement<String> createInvoiceBaselineItemDataBaselineItemDataInBasisOfUnitPriceCode(String value) {
        return new JAXBElement<String>(_InvoiceBaselineItemDataBaselineItemDataInBasisOfUnitPriceCode_QNAME, String.class, Invoice.BaselineItemData.BaselineItemDataIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ProductServiceIdQual", scope = Invoice.BaselineItemData.BaselineItemDataIn.class)
    public JAXBElement<String> createInvoiceBaselineItemDataBaselineItemDataInProductServiceIdQual(String value) {
        return new JAXBElement<String>(_InvoiceBaselineItemDataBaselineItemDataInProductServiceIdQual_QNAME, String.class, Invoice.BaselineItemData.BaselineItemDataIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ProductServiceId", scope = Invoice.BaselineItemData.BaselineItemDataIn.class)
    public JAXBElement<String> createInvoiceBaselineItemDataBaselineItemDataInProductServiceId(String value) {
        return new JAXBElement<String>(_InvoiceBaselineItemDataBaselineItemDataInProductServiceId_QNAME, String.class, Invoice.BaselineItemData.BaselineItemDataIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "UnitOrBasisForMeasurementCode", scope = Invoice.BaselineItemData.BaselineItemDataIn.class)
    public JAXBElement<String> createInvoiceBaselineItemDataBaselineItemDataInUnitOrBasisForMeasurementCode(String value) {
        return new JAXBElement<String>(_InvoiceBaselineItemDataBaselineItemDataInUnitOrBasisForMeasurementCode_QNAME, String.class, Invoice.BaselineItemData.BaselineItemDataIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "QuantityInvoiced", scope = Invoice.BaselineItemData.BaselineItemDataIn.class)
    public JAXBElement<String> createInvoiceBaselineItemDataBaselineItemDataInQuantityInvoiced(String value) {
        return new JAXBElement<String>(_InvoiceBaselineItemDataBaselineItemDataInQuantityInvoiced_QNAME, String.class, Invoice.BaselineItemData.BaselineItemDataIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PurchaseOrderNumber", scope = Invoice.BeginningSegmentForInvoice.class)
    public JAXBElement<String> createInvoiceBeginningSegmentForInvoicePurchaseOrderNumber(String value) {
        return new JAXBElement<String>(_InvoiceBeginningSegmentForInvoicePurchaseOrderNumber_QNAME, String.class, Invoice.BeginningSegmentForInvoice.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "TransactionSetPurposeCode", scope = Invoice.BeginningSegmentForInvoice.class)
    public JAXBElement<String> createInvoiceBeginningSegmentForInvoiceTransactionSetPurposeCode(String value) {
        return new JAXBElement<String>(_InvoiceBeginningSegmentForInvoiceTransactionSetPurposeCode_QNAME, String.class, Invoice.BeginningSegmentForInvoice.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "TransactionTypeCode", scope = Invoice.BeginningSegmentForInvoice.class)
    public JAXBElement<String> createInvoiceBeginningSegmentForInvoiceTransactionTypeCode(String value) {
        return new JAXBElement<String>(_InvoiceBeginningSegmentForInvoiceTransactionTypeCode_QNAME, String.class, Invoice.BeginningSegmentForInvoice.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ActionCode", scope = Invoice.BeginningSegmentForInvoice.class)
    public JAXBElement<String> createInvoiceBeginningSegmentForInvoiceActionCode(String value) {
        return new JAXBElement<String>(_InvoiceBeginningSegmentForInvoiceActionCode_QNAME, String.class, Invoice.BeginningSegmentForInvoice.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "InvoiceNumber", scope = Invoice.BeginningSegmentForInvoice.class)
    public JAXBElement<String> createInvoiceBeginningSegmentForInvoiceInvoiceNumber(String value) {
        return new JAXBElement<String>(_InvoiceBeginningSegmentForInvoiceInvoiceNumber_QNAME, String.class, Invoice.BeginningSegmentForInvoice.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ReleaseNumber", scope = Invoice.BeginningSegmentForInvoice.class)
    public JAXBElement<String> createInvoiceBeginningSegmentForInvoiceReleaseNumber(String value) {
        return new JAXBElement<String>(_InvoiceBeginningSegmentForInvoiceReleaseNumber_QNAME, String.class, Invoice.BeginningSegmentForInvoice.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ChangeOrderSequenceNumber", scope = Invoice.BeginningSegmentForInvoice.class)
    public JAXBElement<String> createInvoiceBeginningSegmentForInvoiceChangeOrderSequenceNumber(String value) {
        return new JAXBElement<String>(_InvoiceBeginningSegmentForInvoiceChangeOrderSequenceNumber_QNAME, String.class, Invoice.BeginningSegmentForInvoice.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Date", scope = Invoice.BeginningSegmentForInvoice.class)
    public JAXBElement<Integer> createInvoiceBeginningSegmentForInvoiceDate(Integer value) {
        return new JAXBElement<Integer>(_InvoiceBeginningSegmentForInvoiceDate_QNAME, Integer.class, Invoice.BeginningSegmentForInvoice.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "EntityIdCode", scope = Invoice.Name.NameIn.class)
    public JAXBElement<String> createInvoiceNameNameInEntityIdCode(String value) {
        return new JAXBElement<String>(_InvoiceNameNameInEntityIdCode_QNAME, String.class, Invoice.Name.NameIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IdCode", scope = Invoice.Name.NameIn.class)
    public JAXBElement<String> createInvoiceNameNameInIdCode(String value) {
        return new JAXBElement<String>(_InvoiceNameNameInIdCode_QNAME, String.class, Invoice.Name.NameIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "EntityRelationshipCode", scope = Invoice.Name.NameIn.class)
    public JAXBElement<String> createInvoiceNameNameInEntityRelationshipCode(String value) {
        return new JAXBElement<String>(_InvoiceNameNameInEntityRelationshipCode_QNAME, String.class, Invoice.Name.NameIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IdCodeQual", scope = Invoice.Name.NameIn.class)
    public JAXBElement<String> createInvoiceNameNameInIdCodeQual(String value) {
        return new JAXBElement<String>(_InvoiceNameNameInIdCodeQual_QNAME, String.class, Invoice.Name.NameIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Name", scope = Invoice.Name.NameIn.class)
    public JAXBElement<String> createInvoiceNameNameInName(String value) {
        return new JAXBElement<String>(_InvoiceNameNameInName_QNAME, String.class, Invoice.Name.NameIn.class, value);
    }

}
